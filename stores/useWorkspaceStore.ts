import type { Interactor } from "@/stores/useNodeStore";
import type { Ref } from "vue";

import { useConnectionStore } from "@/stores/useConnectionStore";
import { defineStore } from "pinia";

interface User {
  id: string | null;
  username: string | null;
  firstName: string | null;
  lastName: string | null;
  email: string | null;
}

interface Role {
  role: string | null;
  description: string | null;
}

interface Workspace {
  id: string | null;
  slug: string | null;
  name: string | null;
  description: string | null;
  createdTime: string | null;
  createdBy: Interactor | null;
  lastEditedTime: string | null;
  lastEditedBy: Interactor | null;
}

interface WorkspaceMember {
  id: string | null;
  user: User | null;
  role: Role | null;
  createdTime: string | null;
  createdBy: Interactor | null;
  lastEditedTime: string | null;
  lastEditedBy: Interactor | null;
}

export const useWorkspaceStore = defineStore("workspace", () => {
  //////////////////////////////////////////////////////////////////////////////////////
  // States
  //////////////////////////////////////////////////////////////////////////////////////
  const connectionStore = useConnectionStore();
  // ------------------------------------------------------------------------------------
  // Workspace
  // ------------------------------------------------------------------------------------
  const workspaceMap = ref(new Map<string, Workspace>());

  const currentWSlug: Ref<null | string> = ref(null);
  // const currentWid: Ref<null | string> = ref(null);
  const loaded = ref(false);

  // ------------------------------------------------------------------------------------
  // Workspace Members
  // ------------------------------------------------------------------------------------
  const currentWMid: Ref<null | string> = ref(null);
  const workspaceMemberMap = ref(new Map<string, Ref<WorkspaceMember>>());
  const workspaceRoles = ref(new Map<string, Role>());

  //////////////////////////////////////////////////////////////////////////////////////
  // Getters
  //////////////////////////////////////////////////////////////////////////////////////

  // ------------------------------------------------------------------------------------
  // Workspace
  // ------------------------------------------------------------------------------------

  const workspacesCount = computed(() => workspaceMap.value.size);
  const currentWid = computed(() => {
    if (currentWSlug.value !== null && workspaceMap.value.has(currentWSlug.value)) {
      return workspaceMap.value.get(currentWSlug.value)?.id;
    }
    return undefined;
  });

  const currentWorkspace = computed(() => {
    if (currentWid.value !== null && workspaceMap.value.has(currentWSlug.value)) {
      return workspaceMap.value.get(currentWSlug.value);
    }

    return {
      id: null,
      slug: null,
      name: null,
      description: null,
      createdTime: null,
      createdBy: null,
      lastEditedTime: null,
      lastEditedBy: null,
    } as Workspace;
  });

  // ------------------------------------------------------------------------------------
  // WorkspaceMembers
  // ------------------------------------------------------------------------------------

  const currentWorkspaceMember = computed(() => {
    if (currentWMid.value !== null && workspaceMemberMap.value.has(currentWMid.value)) {
      return workspaceMemberMap.value.get(currentWMid.value);
    }

    return ref({
      id: null,
      user: {
        id: null,
        username: null,
        firstName: null,
        lastName: null,
        email: null,
      },
      role: {
        role: null,
        description: null,
      },
      createdTime: null,
      createdBy: null,
      lastEditedTime: null,
      lastEditedBy: null,
    });
  });

  //////////////////////////////////////////////////////////////////////////////////////
  // WATCH
  //////////////////////////////////////////////////////////////////////////////////////

  watch(
    currentWid,
    async (newValue) => {
      if (newValue !== undefined && currentWid !== undefined) {
        connectionStore.fetchData();
      }
    },
  );

  //////////////////////////////////////////////////////////////////////////////////////
  // Actions
  //////////////////////////////////////////////////////////////////////////////////////

  // ------------------------------------------------------------------------------------
  // Workspace
  // ------------------------------------------------------------------------------------

  /**
   * The function 'updateWorkspace' update a given workspace from an API.
   * @param workspaceSlug - The `workspaceSlug` parameter is the Slug of the workspace
   * @param name - The `name` parameter is the name of the workspace
   * @param description - The `description` parameter is the description of the workspace
   */
  async function updateWorkspace(workspaceSlug: string, name: string, description: string) {
    const workspaceID = workspaceMap.value.get(workspaceSlug)?.id;
    const requestBody = {
      name: name,
      description: description,
    };

    const { data: workspace } = await useAPIFetch(`/api/workspaces/${workspaceID}`, {
      method: "PUT",
      body: requestBody,
    });
    return workspace.value;
  }

  /**
   * The function fetches a given workspace from an API .
   * @param workspaceSlug - The `workspaceSlug` parameter is the Slug of the workspace
   */
  async function fetchWorkspace(workspaceSlug) {
    const workspaceID = workspaceMap.value.get(workspaceSlug)?.id;
    try {
      const { data: workspace } = await useAPIFetch(`/api/workspaces/${workspaceID}`);
      return workspace.value;
    } catch (error) {
      console.error("Error fetching workspace:", error);
    }
  }

  /**
   * The function fetchWorkspaces fetches workspace from an API and updates the local workspace map
   */
  async function fetchWorkspaces() {
    const currentWSlugs = [] as string[];
    try {
      const allWorkspaces = await recFetch("/api/workspaces");
      for (const workspace of allWorkspaces) {
        // Map each workspace to the Workspace interface and add/update in WorkspaceMap
        const workspaceObj: Workspace = {
          id: workspace.id,
          slug: workspace.slug,
          name: workspace.name,
          description: workspace.description,
          createdTime: workspace.created_time,
          createdBy: workspace.created_by,
          lastEditedTime: workspace.list_edited_time,
          lastEditedBy: workspace.list_edited_by,
        };
        workspaceMap.value.set(workspaceObj.slug, workspaceObj);
        currentWSlugs.push(workspace.slug);
      }

      Array.from(workspaceMap.value.keys()).forEach((slug) => {
        if (slug !== null && !currentWSlugs.includes(slug)) {
          workspaceMap.value.delete(slug);
        }
      });
    } catch (error) {
      console.error("Error fetching workspaces:", error);
    }
  }

  /**
   * The function `createWorkspace` is an asynchronous function that creates a new workspace with the
   * specified parameters and returns the created workspace's value.
   * @param {string} [name] - The name parameter is a string that represents the name of
   * the workspace. It is optional and has a default value of an empty string ("").
   * @param {string} [description] - The `description` parameter is a string that
   * represents the description of the workspace. It provides additional information or details
   * about the workspace.
   * @returns the value of the "workspace" property from the response data.
   */
  async function createWorkspace(name: string = "", description: string = "") {
    try {
      const requestBody = {
        name: name,
        description: description,
      };

      const { data: workspace } = await useAPIFetch("/api/workspaces", {
        method: "POST",
        body: requestBody,
      });
      return workspace.value;
    } catch (error) {
      console.error("Error creating workspace:", error);
    }
  }

  /**
   * The function `deleteWorkspace` is an asynchronous function that deletes a workspace by making a
   * DELETE request to the specified API endpoint.
   * @param workspaceSlug - The `workspaceSlug` parameter is the slug of the
   * workspace that you want to delete.
   * This slug will be used to get the unique identifier of the workspace,
   * later used to construct the URL for the API endpoint that will handle
   * the deletion of the workspace.
   */
  async function deleteWorkspace(workspaceSlug) {
    const workspaceID = workspaceMap.value.get(workspaceSlug)?.id;

    if (workspaceID) {
      workspaceMap.value.delete(workspaceID);
    }
    try {
      await useAPIFetch(`/api/workspaces/${workspaceID}`, {
        method: "DELETE",
      });
    } catch (error) {
      console.error("Error deleting workspace:", error);
    }
  }

  // ------------------------------------------------------------------------------------
  // Workspace Members
  // ------------------------------------------------------------------------------------

  /**
   * The function `setWorkspaceMember` sets a workspace member in a map with the given workpace member's properties.
   * @param workspaceMember - The `workspaceMember` parameter is an object that represents a member of a workspace.
   * It has the following properties:
   */
  function setWorkspaceMember(workspaceMember) {
    workspaceMemberMap.value.set(
      workspaceMember.id,
      ref({
        id: workspaceMember.id,
        user: {
          id: workspaceMember.user.id,
          username: workspaceMember.user.username,
          firstName: workspaceMember.user.first_name,
          lastName: workspaceMember.user.last_name,
          email: workspaceMember.user.email,
        } as User,
        role: {
          role: workspaceMember.role,
          description: workspaceRoles.value.get(workspaceMember.role)?.description,
        } as Role,
        createdTime: workspaceMember.created_time,
        createdBy: workspaceMember.created_by,
        lastEditedTime: workspaceMember.last_edited_time,
        lastEditedBy: workspaceMember.last_edited_by,
      } as WorkspaceMember),
    );
  }

  /**
   * The function `fetchWorkspaceMembers` fetches workspace members from an API and stores them in a map.
   */
  async function fetchWorkspaceMembers() {
    if (currentWid.value !== null) {
      try {
        workspaceMemberMap.value.clear();
        const allWorkspaceMembers = await recFetch(
          `/api/workspaces/${currentWid.value}/members`,
        );

        const { data: session } = useAuth();
        const user = session.value?.user;

        allWorkspaceMembers.forEach((workspaceMember) => {
          setWorkspaceMember(workspaceMember);
          if (workspaceMember.user.username === user?.username) {
            currentWMid.value = workspaceMember.id;
          }
        });
      } catch (error) {
        console.error("Error fetching members:", error);
      }
    }
  }

  /**
   * The function `searchUser` search users from an API and return the result as a list.
   */
  async function searchUser(searchTerm: string) {
    return new Promise((resolve) => {
      recFetch(`/api/users?search=${searchTerm}`)
        .then((searchUsers) => {
          const { data: session } = useAuth();
          const currentUser = session.value?.user;

          searchUsers.forEach((user, index, object) => {
            const filterWorkspaceMemberUser = [...workspaceMemberMap.value.values()].find(
              entry => entry.value.user?.id === user.id,
            );

            if (user.username === currentUser?.username || filterWorkspaceMemberUser) {
              object.splice(index, 1);
            }
          });
          resolve(searchUsers);
        })
        .catch((error) => {
          console.error("Error searching members:", error);
          resolve([]);
        });
    });
  }

  /**
   * The function `updateWorkspaceMemberRole` update a workspace member's role with an API call
   * and modify the workspace member map.
   */
  async function updateWorkspaceMemberRole(workspaceMember: WorkspaceMember) {
    await useAPIFetch(`/api/workspaces/${currentWid.value}/members/${workspaceMember.id}`, {
      method: "PUT",
      body: {
        role: workspaceMember.role.role,
      },
    }).then(() => {
      fetchWorkspaceMembers();
    });
  }

  /**
   * The function `createWorkspaceMember` is an asynchronous function that creates a new workspace member with the
   * specified parameters
   * @param {User} [userID] - The ID of the user to create a workspace member for.
   * @param {string} [role] - The role of the workspace member.
   * @returns the value of the workspace member created.
   */
  async function createWorkspaceMember(userID: string, role: string) {
    await useAPIFetch(`/api/workspaces/${currentWid.value}/members`, {
      method: "POST",
      body: {
        user: userID,
        role: role,
      },
    }).then(() => {
      fetchWorkspaceMembers();
    });
  }

  /**
   * The function `deleteWorkspaceMember` delete a workspace member with an API call
   * and modify the workspace member map.
   */
  async function deleteWorkspaceMember(member: WorkspaceMember) {
    // workspaceMemberMap.value.delete(member.id);
    return new Promise((resolve) => {
      useAPIFetch(`/api/workspaces/${currentWid.value}/members/${member.id}`, {
        method: "DELETE",
      })
        .then((error: error) => {
          if (error.value === undefined) {
            fetchWorkspaceMembers();
            resolve(true);
          } else {
            resolve(false);
          }
        })
        .catch((error) => {
          console.error("Error deleting workspace member:", error);
          resolve(false);
        });
    });
  }

  /**
   * The function `fetchWorkspaceRoles` fetches workspaces' roles and their
   * descriptions from an API and stores them in a map.
   */
  async function fetchWorkspaceRoles() {
    const { data: restRoles } = await useAPIFetch("/api/roles");

    restRoles.value.workspaces.forEach((role) => {
      // for now, if the description mention "nodes", replace with "investigations"
      // the notion of node will be really introduced after the MVP
      role.description = role.description.replaceAll("node", "investigation");
      workspaceRoles.value.set(role.role, role as Role);
    });
  }

  //////////////////////////////////////////////////////////////////////////////////////
  // Returns
  //////////////////////////////////////////////////////////////////////////////////////

  return {
    ////////////////////
    // states
    // -----------------
    // workspaces
    workspaceMap,
    currentWSlug,
    loaded,
    // -----------------
    // workspacemember
    workspaceMemberMap,
    // -----------------
    // roles
    workspaceRoles,

    ////////////////////
    // getter
    // -----------------
    // workspaces
    currentWid,
    currentWorkspace,
    workspacesCount,
    // -----------------
    // members
    currentWorkspaceMember,

    ////////////////////
    // actions
    // -----------------
    // workspace
    fetchWorkspace,
    fetchWorkspaces,
    createWorkspace,
    updateWorkspace,
    deleteWorkspace,
    // -----------------
    // workspacemembers
    fetchWorkspaceMembers,
    searchUser,
    updateWorkspaceMemberRole,
    createWorkspaceMember,
    deleteWorkspaceMember,
    // -----------------
    // roles
    fetchWorkspaceRoles,
  };
});

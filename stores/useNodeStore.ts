import type { Ref } from "vue";

import { useWorkspaceStore } from "@/stores/useWorkspaceStore";
import { defineStore } from "pinia";

export interface Interactor {
  object: string;
  id: string;
}

export interface Pagination {
  countItems: number;
  countPages: number;
  next: string;
  previous: string;
}

interface NodeType {
  id: string;
  name: string;
  description: string | null;
  icon: string | null;
  parent: [] | null;
  children: object | null;
}

export interface Node {
  id: string | null;
  type: NodeType | null;
  title: string | null;
  description: string | null;
  status: "pending" | "active" | "archived" | null;
  created_time: string | null;
  created_by: Interactor | null;
  last_edited_time: string | null;
  last_edited_by: Interactor | null;
  children: Ref<Map<string, Ref<Node>>>;
  path: string[];
  opened: boolean;
  userRole: string | null;
  samples: {
    count: number;
    related: string;
  } | null;
  datalinks: {
    count: number;
    related: string;
  } | null;
}

interface User {
  id: string | null;
  username: string | null;
  firstName: string | null;
  lastName: string | null;
  email: string | null;
}

interface Role {
  role: string | null;
  description: string | null;
}

interface Member {
  id: string | null;
  user: User | null;
  role: Role | null;
  createdTime: string | null;
  createdBy: Interactor | null;
  lastEditedTime: string | null;
  lastEditedBy: Interactor | null;
}

// interface Tool {
//   id: number;
//   connector: string;
//   name: string;
//   parameters: object;
//   async: boolean;
// }

// interface DataObject {
//   id: string;
//   name: string;
//   type: string;
//   icon: string;
//   linkable: boolean;
//   hasChildren: boolean;
//   url: string;
//   children: DataObject[];
// }

export const useNodeStore = defineStore("node", () => {
  const workspaceStore = useWorkspaceStore();

  //////////////////////////////////////////////////////////////////////////////////////
  // States
  //////////////////////////////////////////////////////////////////////////////////////

  // ------------------------------------------------------------------------------------
  // Nodes
  // ------------------------------------------------------------------------------------

  const nodes = ref(new Map<string, Ref<Node>>());
  const nodeMap = ref(new Map<string, Ref<Node>>());

  const currentNid: Ref<null | string> = ref(null);
  const nodeTypes = ref(new Map<string, NodeType>());
  const currentNodepagination: Ref<null | Pagination> = ref(null);
  const updatedNodes = ref([] as string[]);
  const loaded = ref(false);

  // ------------------------------------------------------------------------------------
  // Members
  // ------------------------------------------------------------------------------------
  const currentMid: Ref<null | string> = ref(null);
  const memberMap = ref(new Map<string, Ref<Member>>());
  const nodeRoles = ref(new Map<string, Role>());

  //////////////////////////////////////////////////////////////////////////////////////
  // Getters
  //////////////////////////////////////////////////////////////////////////////////////

  // ------------------------------------------------------------------------------------
  // Nodes
  // ------------------------------------------------------------------------------------

  const nodesCount = computed(() => nodes.value.size);
  const hasPendingModification = computed(() => updatedNodes.value.length > 0);

  const currentNode = computed(() => {
    if (currentNid.value !== null && nodeMap.value.has(currentNid.value)) {
      return nodeMap.value.get(currentNid.value);
    }

    return ref({
      id: null,
      type: null,
      title: null,
      description: null,
      status: null,
      createdTime: null,
      createdBy: null,
      lastEditedTime: null,
      lastEditedBy: null,
      children: ref(new Map<string, Ref<Node>>()),
      path: [],
      opened: false,
      userRole: null,
      samples: {
        count: 0,
        related: "",
      },
      datalinks: {
        count: 0,
        related: "",
      },
    } as Node);
  });

  // ------------------------------------------------------------------------------------
  // Members
  // ------------------------------------------------------------------------------------

  const currentMember = computed(() => {
    if (currentMid.value !== null && memberMap.value.has(currentMid.value)) {
      return memberMap.value.get(currentMid.value);
    }

    return ref({
      id: null,
      user: {
        id: null,
        username: null,
        firstName: null,
        lastName: null,
        email: null,
      },
      role: {
        role: null,
        description: null,
      },
      createdTime: null,
      createdBy: null,
      lastEditedTime: null,
      lastEditedBy: null,
    });
  });

  //////////////////////////////////////////////////////////////////////////////////////
  // Actions
  //////////////////////////////////////////////////////////////////////////////////////

  // ------------------------------------------------------------------------------------
  // Nodes
  // ------------------------------------------------------------------------------------

  /**
   * The function adds a node ID to an array of updated node IDs if it is not already
   * present.
   * @param {string} nid - The `nid` parameter is a string that represents the ID of a
   * node.
   */
  function addUpdatedNode(nid: string) {
    if (!updatedNodes.value.includes(nid)) {
      updatedNodes.value.push(nid);
    }
  }

  async function updateNode(
    nodeId: string,
    title: string,
    description: string,
    status: string = "active",
    parentID: string = "",
  ) {
    let requestBody = {
      title: title,
      description: description,
      status: status,
    };

    if (parentID === "") {
      requestBody = {
        ...requestBody,
      };
    } else {
      requestBody = {
        ...requestBody,
        parent: parentID,
      };
    }
    const { data: node } = await useAPIFetch(`/api/nodes/${nodeId}`, {
      method: "PUT",
      body: requestBody,
      headers: { "X-Workspace": workspaceStore.currentWid },
    });
    return node.value;
  }

  /**
   * The function `saveAndRefresh` updates nodes by sending PUT requests to an API and
   * removes the updated nodes from the `updatedNodes` array.
   */

  async function saveAndRefresh() {
    updatedNodes.value.forEach(async (nid) => {
      if (nodeMap.value.has(nid)) {
        const tempNode = nodeMap.value.get(nid)?.value;
        updateNode(nid, tempNode.title, tempNode.description, "active");
      }

      const index = updatedNodes.value.indexOf(nid, 0);
      if (index > -1) {
        updatedNodes.value.splice(index, 1);
      }
    });
  }

  /**
   * The function `setNode` sets a node in a map with the given node's properties.
   * @param node - The `node` parameter is an object that represents a node in a tree
   * structure. It has the following properties:
   */
  function setNode(node, userRole: string | null = null) {
    nodes.value.set(
      node.id,
      ref({
        id: node.id,
        title: node.title,
        description: node.description,
        type: {
          id: node.type.id,
          name: node.type.name,
          description: node.type.description,
          icon: node.type.icon,
        } as NodeType,
        status: node.status,
        createdTime: node.created_time,
        createdBy: node.created_by,
        lastEditedTime: node.created_time,
        lastEditedBy: node.last_edited_by,
        path: node.path,
        children: ref(new Map<string, Ref<Node>>()),
        opened: false,
        userRole: userRole,
        samples: node.samples,
        datalinks: node.datalinks,
      } as Node),
    );
  }

  /**
   * The function `generateNode` creates a new node object with the provided properties and
   * returns it as a reference.
   * @param node - The `node` parameter is an object that represents a node in a tree
   * structure. It contains properties such as `id`, `title`, `description`, `type`,
   * `createdTime`, `createdBy`, `lastEditedTime`, `lastEditedBy`, `path`, and `children`.
   * @param {boolean} [opened] - The `opened` parameter is a boolean value that
   * indicates whether the node should be initially opened or closed. It is an optional
   * parameter with a default value of `false`.
   * @param {string | null } [userRole] - The userRole describe the role of the current user
   * in the Node
   * @returns The function `generateNode` returns a reference to a `Node` object.
   */
  const generateNode = (
    node,
    opened: boolean = false,
    userRole: string | null = null,
  ): Ref<Node> => ref({
    id: node.id,
    title: node.title,
    description: node.description,
    type: {
      id: node.type.id,
      name: node.type.name,
      description: node.type.description,
      icon: node.type.icon,
    } as NodeType,
    status: node.status,
    createdTime: node.created_time,
    createdBy: node.created_by,
    lastEditedTime: node.created_time,
    lastEditedBy: node.last_edited_by,
    path: node.path,
    children: ref(new Map<string, Ref<Node>>()),
    opened: opened,
    userRole: userRole,
    samples: node.samples,
    datalinks: node.datalinks,
  } as Node);

  /**
   * The function `addNodeToTree` adds a node to a tree structure and updates the active node
   * accordingly.
   * @param {Node} node - The `node` parameter is an object representing a node in a tree
   * structure. It contains properties such as `path`, which is an array representing the
   * path to the node in the tree.
   */
  const addNodeToTree = async (node: Node) => {
    const path = node.path;

    // Ensure path is not empty
    if (path.length === 0) return;

    // Ensure root node exists
    const rootNodeId = path[0];
    if (!nodes.value.has(rootNodeId)) {
      return;
    }

    const rootNode = nodes.value.get(rootNodeId);
    if (!rootNode) return; // Ensure root node is not undefined

    // Root node
    const activeNode: Node | undefined = ref(rootNode.value);
    activeNode.value.opened = true;

    // Get the current user's session data
    const { data: session } = useAuth();
    const user = session.value?.user;

    for (let i = 0; i < path.length; i++) {
      let nextCurrent = ref(null);
      // Fetch all children of the current node in the path from the API
      const allNodeChildren = await recFetch(`/api/nodes?parent=${path[i]}`, {
        headers: { "X-Workspace": workspaceStore.currentWid },
      });

      for (const child of allNodeChildren) {
        // Fetch the member data for the current child node
        let member = null;
        try {
          await useAPIFetch(`/api/nodes/${node.id}/members?search=${user.username}`, {
            headers: { "X-Workspace": workspaceStore.currentWid },
          }).then(response => (member = response.data.value.results[0]));
        } catch (error) {
          console.error("Error fetching members:", error);
        }

        // Generate a new node for the child and add it to the current node's children
        const childNode = generateNode(child, path.includes(child.id), member?.role);
        if (path.includes(child.id)) {
          nextCurrent = childNode;
        }
        activeNode.value.children.set(child.id, childNode);
        nodeMap.value.set(child.id, activeNode.value.children.get(child.id));
      }

      // Set the current node as the next current node
      activeNode.value = nextCurrent.value;

      nextCurrent = ref(null);
    }
  };

  /**
   * The function fetches the children of a given node from an API and adds them to a tree.
   * @param nodeID - The `nodeID` parameter is the ID of the parent node for which we want to
   * fetch the children.
   */
  async function fetchNodeChildren(nodeID) {
    const allChildren = await recFetch(`/api/nodes?parent=${nodeID}`, {
      headers: { "X-Workspace": workspaceStore.currentWid },
    });
    for (const node of allChildren) {
      addNodeToTree(node);
    }
  }

  /**
   * The function fetchNodes fetches nodes from an API, updates the local node map, and
   * fetches children of opened nodes.
   */
  async function fetchNodes() {
    if (workspaceStore.currentWid) {
      const currentNids = [] as string[];
      try {
        const allNodes = await recFetch("/api/nodes?parent=null", {
          headers: { "X-Workspace": workspaceStore.currentWid },
        });

        const { data: session } = useAuth();
        const user = session.value?.user;

        allNodes.forEach(async (node) => {
          if (!nodes.value.has(node.id)) {
            let member = null;
            try {
              await useAPIFetch(`/api/nodes/${node.id}/members?search=${user.username}`, {
                headers: { "X-Workspace": workspaceStore.currentWid },
              }).then(response => (member = response.data.value.results[0]));
            } catch (error) {
              console.error("Error fetching members:", error);
            }

            setNode(node, member?.role);
            nodeMap.value.set(node.id, nodes.value.get(node.id));
          }
          currentNids.push(node.id);
        });

        nodes.value.forEach(async (node: Node, key: string) => {
          if (key !== null && !currentNids.includes(key)) {
            // remove nodes deleted remotely
            nodes.value.delete(key);
          } else {
            if (node.value && node.value.opened && node.value.path.includes(currentNid)) {
              await fetchNodeChildren(key);
            }
          }
        });
      } catch (error) {
        console.error("Error fetching nodes:", error);
      }
    }
  }

  /**
   * The function fetches a node from an API and adds it to a tree.
   * @param nodeID - The `nodeID` parameter is the unique identifier of the node that we want
   * to fetch and add to the tree.
   */
  async function fetchAndAddNode(nodeID) {
    loaded.value = false;
    const { data: node, error } = await useAPIFetch(`/api/nodes/${nodeID}`, {
      headers: { "X-Workspace": workspaceStore.currentWid },
    });
    if (error.value) {
      return false;
    }
    await addNodeToTree(node.value);
    loaded.value = true;
    return true;
  }

  /**
   * The function fetches node types from an API and stores them in a map.
   */
  async function fetchNodeTypes() {
    const { data: restNodeTypes } = await useAPIFetch("/api/node_types?fields=children");

    restNodeTypes.value.results.forEach((nodeType) => {
      nodeTypes.value.set(nodeType.id, nodeType as NodeType);
    });
  }

  /**
   * The function `createPendingNode` is an asynchronous function that creates a new node
   * with a specified type and parent, and returns the created node.
   * @param {string} typeId - The `typeId` parameter is a string that represents the type of
   * the node that you want to create. It is used to specify the type of the node when making
   * a POST request to the `/api/nodes` endpoint.
   * @returns the value of the `node` property from the response data.
   */
  async function createPendingNode(typeId: string, title: string = "", root?: boolean) {
    try {
      let requestBody = {
        type: typeId,
        title: title,
        description: "",
      };

      if (root) {
        requestBody = {
          ...requestBody,
        };
      } else {
        requestBody = {
          ...requestBody,
          parent: currentNid.value,
        };
      }

      const { data: node } = await useAPIFetch("/api/nodes", {
        method: "POST",
        body: requestBody,
        headers: { "X-Workspace": workspaceStore.currentWid },
      });
      return node.value;
    } catch (error) {
      console.error("Error creating node:", error);
    }
  }

  /**
   * The function `createNode` is an asynchronous function that creates a new node with the
   * specified parameters and returns the created node's value.
   * @param {string} typeId - The `typeId` parameter is a string that represents the type
   * of the node being created. It is used to specify the category or classification of the
   * node.
   * @param {string} [title] - The title parameter is a string that represents the title of
   * the node. It is optional and has a default value of an empty string ("").
   * @param {string} [description] - The `description` parameter is a string that
   * represents the description of the node. It provides additional information or details
   * about the node.
   * @param {string} [status] - The "status" parameter is used to specify the status
   * of the node being created. It has a default value of "active", but you can provide a
   * different value if needed.
   * @param {string} [parentID] - The `parentID` parameter is used to specify the ID of the
   * parent node to which the new node should be attached. If no parent node is specified
   * (i.e., `parentID` is an empty string), the new node will be created as a top-level
   * node.
   * @returns the value of the "node" property from the response data.
   */
  async function createNode(
    typeId: string,
    title: string = "",
    description: string = "",
    status: string = "active",
    parentID: string = "",
  ) {
    try {
      let requestBody = {
        type: typeId,
        title: title,
        description: description,
        status: status,
      };

      if (parentID === "") {
        requestBody = {
          ...requestBody,
        };
      } else {
        requestBody = {
          ...requestBody,
          parent: parentID,
        };
      }

      const { data: node } = await useAPIFetch("/api/nodes", {
        method: "POST",
        body: requestBody,
        headers: { "X-Workspace": workspaceStore.currentWid },
      });
      return node.value;
    } catch (error) {
      console.error("Error creating node:", error);
    }
  }

  /**
   * The function `deleteNode` is an asynchronous function that deletes a node by making a
   * DELETE request to the specified API endpoint.
   * @param nodeId - The `nodeId` parameter is the unique identifier of the node that you
   * want to delete. It is used to construct the URL for the API endpoint that will handle
   * the deletion of the node.
   */
  async function deleteNode(nodeId) {
    const tempNode = nodeMap.value.get(nodeId).value;
    if (tempNode.path.length > 1) {
      const parentId = tempNode.path[tempNode.path.length - 2];
      nodeMap.value.get(parentId).value.children.delete(nodeId);
    }
    nodeMap.value.delete(nodeId);
    if (nodes.value.has(nodeId)) {
      nodes.value.delete(nodeId);
    }
    try {
      await useAPIFetch(`/api/nodes/${nodeId}`, {
        method: "DELETE",
        headers: { "X-Workspace": workspaceStore.currentWid },
      });
    } catch (error) {
      console.error("Error deleting node:", error);
    }
  }

  // ------------------------------------------------------------------------------------
  // Members
  // ------------------------------------------------------------------------------------

  /**
   * The function `setMember` sets a member in a map with the given member's properties.
   * @param member - The `member` parameter is an object that represents a member of a node.
   * It has the following properties:
   */
  function setMember(member) {
    memberMap.value.set(
      member.id,
      ref({
        id: member.id,
        user: {
          id: member.user.id,
          username: member.user.username,
          firstName: member.user.first_name,
          lastName: member.user.last_name,
          email: member.user.email,
        } as User,
        role: {
          role: member.role,
          description: nodeRoles.value.get(member.role)?.description,
        } as Role,
        createdTime: member.created_time,
        createdBy: member.created_by,
        lastEditedTime: member.last_edited_time,
        lastEditedBy: member.last_edited_by,
      } as Member),
    );
  }

  /**
   * The function `fetchMembers` fetches members from an API and stores them in a map.
   */
  async function fetchMembers() {
    if (workspaceStore.currentWid && currentNid.value !== null) {
      try {
        memberMap.value.clear();
        const allMembers = await recFetch(`/api/nodes/${currentNid.value}/members`, {
          headers: { "X-Workspace": workspaceStore.currentWid },
        });

        const { data: session } = useAuth();
        const user = session.value?.user;

        allMembers.forEach((member) => {
          setMember(member);
          if (member.user.username === user.username) {
            currentMid.value = member.id;
          }
        });
      } catch (error) {
        console.error("Error fetching members:", error);
      }
    }
  }

  /**
   * The function `searchMember` search members from an API and return the result as a list.
   */
  async function searchMember(searchTerm: string) {
    return new Promise((resolve) => {
      const currentNodeParent = nodeMap.value.get(currentNid.value).value.path;

      // if the node has a parent, return the members of the parent that are not members of the node
      if (currentNodeParent.length > 1) {
        const parentId = currentNodeParent[currentNodeParent.length - 2];

        recFetch(`/api/nodes/${parentId}/members?search=${searchTerm}`, {
          headers: { "X-Workspace": workspaceStore.currentWid },
        })
          .then((searchNodeMembers) => {
            const { data: session } = useAuth();
            const currentUser = session.value?.user;

            searchNodeMembers.forEach((NodeMember, index, object) => {
              if (
                NodeMember.user.username === currentUser?.username
                || memberMap.value.has(NodeMember.id)
              ) {
                object.splice(index, 1);
              } else {
                object[index] = NodeMember.user;
              }
            });
            resolve(searchNodeMembers);
          })
          .catch((error) => {
            console.error("Error searching members:", error);
            resolve([]);
          });
        // otherwise, if it is a root node, return workspace members
      } else {
        recFetch(`/api/workspaces/${workspaceStore.currentWid}/members?search=${searchTerm}`)
          .then((searchWorkspaceMember) => {
            const { data: session } = useAuth();
            const currentUser = session.value?.user;

            /* The above code is using the `reduceRight` method on the `searchWorkspaceMember` array to
            iterate over each element from right to left. The function provided as an argument to
            `reduceRight` is called for each element in the array, with the `acc` parameter
            representing the accumulator value, `item` representing the current element being
            processed, `index` representing the index of the current element, and `object`
            representing the array itself. The function is expected to return the updated
            accumulator value for the next iteration. */
            searchWorkspaceMember.reduceRight((acc, item, index, object) => {
              const filterMemberUser = [...memberMap.value.values()].find(
                /* The above code is a TypeScript arrow function that takes an `entry` parameter and
                    checks if the `id` of the user in the `entry.value` object is equal to the `id` of
                    the `WorkspaceMember`. If the condition is met, it will return `true`, otherwise it
                    will return `false`. */
                entry => entry.value.user?.id === item.user.id,
              );
              if (item.user.username === currentUser?.username || filterMemberUser) {
                object.splice(index, 1);
              } else {
                object[index] = item.user;
              }
              return acc;
            }, []);
            resolve(searchWorkspaceMember);
          })
          .catch((error) => {
            console.error("Error searching members:", error);
            resolve([]);
          });
      }
    });
  }

  /**
   * The function `updateMemberRole` update a member's role with an API call
   * and modify the member map.
   */
  async function updateMemberRole(member: Member) {
    await useAPIFetch(`/api/nodes/${currentNid.value}/members/${member.id}`, {
      method: "PUT",
      body: {
        role: member.role?.role,
      },
      headers: { "X-Workspace": workspaceStore.currentWid },
    }).then(() => {
      fetchMembers();
    });
  }

  /**
   * The function `createMember` is an asynchronous function that creates a new member with the
   * specified parameters
   * @param {User} [userID] - The ID of the user to create a member for.
   * @param {string} [role] - The role of the member.
   * @returns the value of the member created.
   */
  async function createMember(userID: string, role: string) {
    await useAPIFetch(`/api/nodes/${currentNid.value}/members`, {
      method: "POST",
      body: {
        user: userID,
        role: role,
        node: currentNid.value,
      },
      headers: { "X-Workspace": workspaceStore.currentWid },
    }).then(() => {
      fetchMembers();
    });
  }

  /**
   * The function `deleteMember` delete a member with an API call
   * and modify the member map.
   */
  async function deleteMember(member: Member) {
    return new Promise((resolve) => {
      useAPIFetch(`/api/nodes/${currentNid.value}/members/${member.id}`, {
        method: "DELETE",
        headers: { "X-Workspace": workspaceStore.currentWid },
      })
        .then((error: error) => {
          if (error.value === undefined) {
            fetchMembers();
            resolve(true);
          } else {
            resolve(false);
          }
        })
        .catch((error) => {
          console.error("Error deleting member:", error);
          resolve(false);
        });
    });
  }

  /**
   * The function `fetchNodeRoles` fetches nodes' roles and their
   * descriptions from an API and stores them in a map.
   */
  async function fetchNodeRoles() {
    const { data: restRoles } = await useAPIFetch("/api/roles");

    restRoles.value.nodes.forEach((role) => {
      nodeRoles.value.set(role.role, role as Role);
    });
  }

  /**
   * The function fetches a node using the provided node ID and workspace header.
   * @param nodeId - The `nodeId` parameter is the unique identifier of the node
   * @returns The function `fetchNode` is returning the node fetched.
   */
  async function fetchNode(nodeId) {
    if (nodeMap.value.has(nodeId)) {
      return nodeMap.value.get(nodeId);
    } else {
      const { data: node, error } = await useAPIFetch(`/api/nodes/${nodeId}`, {
        headers: { "X-Workspace": workspaceStore.currentWid },
      });
      if (error.value) {
        console.error("Error creating node:", error);
      }
      return node.value;
    }
  }

  /**
   * The function `getNodePath` retrieves the path of a node by fetching and
   * appending the titles of its parent nodes.
   * @param nodeId - The `nodeId` parameter is the unique identifier of a node in
   * a tree structure.
   * @returns The `getNodePath` function returns an object with two properties:
   * `path` and `lastParentNodeId`. The `path` property contains a string
   * representing the path of the node, which is constructed by appending the
   * titles of the parent nodes separated by slashes ("/"). The
   * `lastParentNodeId` property contains the ID of the last parent node
   * processed in the loop.
   */
  async function getNodePath(nodeId) {
    let path = "";
    let lastParentNodeId = null;
    if (nodeId && nodeMap.value.has(nodeId)) {
      const node = nodeMap.value.get(nodeId).value;
      const parentNodeIds = node.path || [];
      const parentNodeCount = parentNodeIds.length;
      // Iterate over each parentNodeId and fetch the title
      for (let i = 0; i < parentNodeCount; i++) {
        const parentNodeId = parentNodeIds[i];
        const parentNode = await fetchNode(parentNodeId);
        // Append the parent title to the path
        path += truncateTitle(parentNode.value.title);
        // Add a slash unless it's the last element
        if (i < parentNodeCount - 1) {
          path += " / ";
        }
        // Update the lastParentNodeId
        lastParentNodeId = parentNodeId;
      }
    }
    return { path, lastParentNodeId };
  }

  function sortedMap() {
    return new Map(
      [...nodes.value.entries()].sort((a, b) => a[1].value.title.localeCompare(b[1].value.title)),
    );
  }
  //////////////////////////////////////////////////////////////////////////////////////
  // Returns
  //////////////////////////////////////////////////////////////////////////////////////

  return {
    ////////////////////
    // states
    // -----------------
    // nodes
    nodeMap,
    nodes,
    currentNid,
    nodeTypes,
    currentNodepagination,
    updatedNodes,
    loaded,
    // -----------------
    // members
    memberMap,
    // -----------------
    // roles
    nodeRoles,

    ////////////////////
    // getter
    // -----------------
    // nodes
    currentNode,
    nodesCount,
    hasPendingModification,
    // -----------------
    // members
    currentMember,

    ////////////////////
    // actions
    // -----------------
    // nodes
    addNodeToTree,
    fetchNodeChildren,
    fetchAndAddNode,
    fetchNodes,
    fetchNode,
    fetchNodeTypes,
    createPendingNode,
    createNode,
    updateNode,
    deleteNode,
    saveAndRefresh,
    addUpdatedNode,
    getNodePath,
    sortedMap,
    // -----------------
    // members
    fetchMembers,
    searchMember,
    updateMemberRole,
    createMember,
    deleteMember,
    // -----------------
    // roles
    fetchNodeRoles,
  };
});

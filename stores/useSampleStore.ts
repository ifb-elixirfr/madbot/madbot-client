import type { Interactor, Pagination } from "@/stores/useNodeStore";
import type { Ref } from "vue";

import { useNodeStore } from "@/stores/useNodeStore";
import { useWorkspaceStore } from "@/stores/useWorkspaceStore";
import { defineStore } from "pinia";

interface Node {
  id: string | null;
  title: string | null;
  description: string | null;
}

interface Sample {
  id: string | null;
  title: string | null;
  alias: string | null;
  node: Node | null;
  source: string | null;
  path: string | null;
  createdTime: string | null;
  createdBy: Interactor | null;
  lastEditedTime: string | null;
  lastEditedBy: Interactor | null;
  boundData: SampleBond[];
}

interface SampleBond {
  id: string | null;
  sample: string | null;
  data: string | null;
  dataLink: string | null;
}

export const useSampleStore = defineStore("sample", () => {
  const workspaceStore = useWorkspaceStore();
  const nodeStore = useNodeStore();

  //////////////////////////////////////////////////////////////////////////////////////
  // States
  //////////////////////////////////////////////////////////////////////////////////////

  // ------------------------------------------------------------------------------------
  // Sample
  // ------------------------------------------------------------------------------------
  const samples = ref(new Map<string, Ref<Sample>>());
  const sampleMap = ref(new Map<string, Ref<Sample>>());

  const currentSid: Ref<null | string> = ref(null);
  const currentSamplePagination: Ref<null | Pagination> = ref(null);
  const updatedSamples = ref([] as string[]);
  const loaded = ref(false);

  //////////////////////////////////////////////////////////////////////////////////////
  // Getters
  //////////////////////////////////////////////////////////////////////////////////////

  // ------------------------------------------------------------------------------------
  // Sample
  // ------------------------------------------------------------------------------------

  const samplesCount = computed(() => sampleMap.value.size);
  const hasPendingModification = computed(() => updatedSamples.value.length > 0);

  const currentSample = computed(() => {
    if (currentSid.value !== null && sampleMap.value.has(currentSid.value)) {
      return sampleMap.value.get(currentSid.value);
    }

    return ref({
      id: null,
      title: null,
      alias: null,
      node: null,
      source: null,
      path: null,
      createdTime: null,
      createdBy: null,
      lastEditedTime: null,
      lastEditedBy: null,
      boundData: [],
    } as Sample);
  });

  //////////////////////////////////////////////////////////////////////////////////////
  // Actions
  //////////////////////////////////////////////////////////////////////////////////////

  // ------------------------------------------------------------------------------------
  // Samples
  // ------------------------------------------------------------------------------------

  /**
   * The function adds a sample ID to an array of updated samples IDs if it is not already
   * present.
   * @param {string} sid - The `sid` parameter is a string that represents the ID of a
   * sample.
   */
  function addUpdatedSample(sid: string) {
    if (!updatedSamples.value.includes(sid)) {
      updatedSamples.value.push(sid);
    }
  }

  /**
   * The function `saveAndRefresh` updates samples by sending PUT requests through an API and
   * removes the updated samples from the `updatedSamples` array.
   */

  async function saveAndRefresh() {
    updatedSamples.value.forEach(async (sid) => {
      if (sampleMap.value.has(sid)) {
        const tempSample = sampleMap.value.get(sid)?.value;
        if (tempSample && tempSample.title !== null && tempSample.alias !== null) {
          updateSample(sid, tempSample.title, tempSample.alias);
        }
      }

      const index = updatedSamples.value.indexOf(sid, 0);
      if (index > -1) {
        updatedSamples.value.splice(index, 1);
      }
    });
  }

  /**
   * The `updateSample` function updates a given sample through the API.
   * @param sampleId - The `sampleId` parameter is the uuid of the sample
   * @param title - The `title` parameter is the title of the sample
   * @param alias - The `alias` parameter is the alias of the sample
   */
  async function updateSample(sampleId: string, title: string, alias: string) {
    const requestBody = {
      title: title,
      alias: alias,
    };
    try {
      const { data: sample } = await useAPIFetch(
        `/api/nodes/${nodeStore.currentNid}/samples/${sampleId}`,
        {
          method: "PUT",
          body: requestBody,
          headers: { "X-Workspace": workspaceStore.currentWid },
        },
      );
      return sample.value;
    } catch (error) {
      console.error("Error updating sample:", error);
    }
  }

  /**
   * The `fetchSample` function fetches a given sample through the API.
   * @param sampleId - The `sampleId` parameter is the uuid of the sample
   */
  async function fetchSample(sampleId: string) {
    if (sampleMap.value.has(sampleId)) {
      return sampleMap.value.get(sampleId)?.value;
    } else {
      try {
        const { data: sample } = await useAPIFetch(
          `/api/nodes/${nodeStore.currentNid}/samples/${sampleId}`,
          {
            headers: { "X-Workspace": workspaceStore.currentWid },
          },
        );
        return sample.value;
      } catch (error) {
        console.error("Error fetching sample:", error);
      }
    }
  }

  /**
   * The function fetchSamples fetches sample from an API and updates the local samples map
   */
  async function fetchSamples({
    inherited = true,
    descendant = false,
  }: {
    inherited?: boolean;
    descendant?: boolean;
  } = {}) {
    sampleMap.value.clear();
    try {
      const allSamples = await recFetch(
        // "/api/nodes/" + nodeStore.currentNid + "/samples?inherited_samples=true",
        `/api/nodes/${
          nodeStore.currentNid
        }/samples?inherited_samples=${
          inherited
        }&descendant_samples=${
          descendant}`,
        {
          headers: { "X-Workspace": workspaceStore.currentWid },
        },
      );

      for (const sample of allSamples) {
        if (!sampleMap.value.has(sample.id)) {
          let path = "";
          if (sample.node && (sample.source === "inherited" || sample.source === "descendant")) {
            ({ path } = await nodeStore.getNodePath(sample.node.id));
          }
          // Iterate through nodeMembers to find the matching user
          nodeStore.memberMap.forEach((member) => {
            if (member.value.user.id === sample.created_by.id) {
              sample.created_by.firstName = member.value.user.firstName;
              sample.created_by.lastName = member.value.user.lastName;
            }
            if (member.value.user.id === sample.last_edited_by.id) {
              sample.last_edited_by.firstName = member.value.user.firstName;
              sample.last_edited_by.lastName = member.value.user.lastName;
            }
          });
          // This code doesn't work as there is no retrieve from /api/users/id
          // if (!sample.created_by.firstName && sample.created_by.id !== null) {
          //   recFetch(`/api/users/${sample.created_by.id}`)
          //     .then((sampleUser) => {
          //       sample.created_by.firstName = sampleUser.firstName;
          //       sample.created_by.lastName = sampleUser.lastName;
          //     });
          // }
          // if (!sample.last_edited_by.firstName && sample.last_edited_by.id !== null) {
          //   recFetch(`/api/users/${sample.last_edited_by.id}`)
          //     .then((sampleUser) => {
          //       sample.last_edited_by.firstName = sampleUser.firstName;
          //       sample.last_edited_by.lastName = sampleUser.lastName;
          //     });
          // }
          if (sample.created_by.id === null) {
            sample.created_by.firstName = "";
            sample.created_by.lastName = "";
          }
          if (sample.last_edited_by.id === null) {
            sample.last_edited_by.firstName = "";
            sample.last_edited_by.lastName = "";
          }
          // Map each sample to the Sample interface and add/update in SampleMap
          const sampleObj: Sample = {
            id: sample.id,
            title: sample.title,
            alias: sample.alias,
            node: {
              id: sample.node.id,
              title: sample.node.title,
              description: sample.node.description,
            } as Node,
            source: sample.source,
            path: path,
            createdTime: sample.created_time,
            createdBy: sample.created_by,
            lastEditedTime: sample.last_edited_time,
            lastEditedBy: sample.last_edited_by,
            boundData: [],
          };
          sampleMap.value.set(sampleObj.id, { value: sampleObj });
        }
      }
    } catch (error) {
      console.error("Error fetching samples:", error);
    }
  }

  /**
   * The `createSample` function creates a new sample with the
   * specified parameters through the API and returns the created sample's value.
   * @param {string} [title] - The title parameter is a string that represents the title of
   * the sample. It is mandatory.
   * @returns The created sample if the request is successful.
   */
  async function createSample(title: string, alias: string) {
    try {
      const requestBody = {
        title: title,
        alias: alias,
      };

      const { data: sample } = await useAPIFetch(
        `/api/nodes/${nodeStore.currentNid}/samples`,
        {
          method: "POST",
          body: requestBody,
          headers: { "X-Workspace": workspaceStore.currentWid },
        },
      );
      // Update sampleMap reactively
      sampleMap.value.set(sample.id, { value: sample });
      return sample.value;
    } catch (error) {
      console.error("Error creating sample:", error);
    }
  }

  /**
   * The function `deleteSample` deletes a sample through the API.
   * @param sampleId - The `sampleId` parameter is the uuid of the
   * sample that you want to delete.
   */
  async function deleteSample(sampleId: string) {
    sampleMap.value.delete(sampleId);
    try {
      await useAPIFetch(`/api/nodes/${nodeStore.currentNid}/samples/${sampleId}`, {
        method: "DELETE",
        headers: { "X-Workspace": workspaceStore.currentWid },
      });
    } catch (error) {
      console.error("Error deleting sample:", error);
    }
  }

  async function fetchDataBoundSamples(dataID: string) {
    try {
      const allBoundSamples = await recFetch(`/api/data/${dataID}/bound_samples`, {
        headers: { "X-Workspace": workspaceStore.currentWid },
      });
      allBoundSamples.forEach((sampleBond) => {
        if (sampleMap.value.has(sampleBond.sample.id)) {
          sampleMap.value.get(sampleBond.sample.id).value.boundData.push({
            id: sampleBond.id,
            sample: sampleBond.sample.id,
            data: dataID,
            dataLink: sampleBond.datalink,
          });
        }
      });
    } catch (error) {
      console.error("Error fetching bound samples: ", error);
    }
  }

  async function fetchSampleBoundData(nodeId: string, sampleId: string) {
    try {
      const allBoundData = await recFetch(`/api/nodes/${nodeId}/samples/${sampleId}/bound_data`, {
        headers: { "X-Workspace": workspaceStore.currentWid },
      });
      allBoundData.forEach((dataBond) => {
        if (sampleMap.value.has(sampleId)) {
          const sampleData = sampleMap.value.get(sampleId).value.boundData;
          const index = sampleData.findIndex(existingBond => existingBond.id === dataBond.id);
          const alreadyExists = index !== -1;
          const missingData = alreadyExists && sampleData[index]?.data?.title === undefined;

          if (!alreadyExists) {
            sampleData.push({
              id: dataBond.id,
              sample: dataBond.sample.id,
              data: dataBond.data,
              dataLink: dataBond.datalink,
            });
          }

          if (alreadyExists && missingData) {
            sampleData[index].data = dataBond.data;
          }
        }
      });
    } catch (error) {
      console.error("Error fetching bound data: ", error);
    }
  }

  async function bindSampleToData(sampleID: string, dataID: string, dataLinkID: string) {
    try {
      const requestBody = {
        sample: sampleID,
        datalink: dataLinkID,
      };

      await useAPIFetch(`/api/data/${dataID}/bound_samples`, {
        method: "POST",
        headers: { "X-Workspace": workspaceStore.currentWid },
        body: requestBody,
      }).then((res) => {
        const error = res.error.value;
        if (error) {
          console.error("Error binding sample to data: ", error);
        } else {
          sampleMap.value.get(sampleID)?.value.boundData.push({
            id: res.data.value.id,
            sample: sampleID,
            data: dataID,
            dataLink: dataLinkID,
          });
        }
      });
    } catch (error) {
      console.error("Error binding sample to data: ", error);
    }
  }

  async function unbindSampleToData(sampleID: string, dataID: string, sampleBondID: string) {
    try {
      await useAPIFetch(`/api/data/${dataID}/bound_samples/${sampleBondID}`, {
        method: "DELETE",
        headers: { "X-Workspace": workspaceStore.currentWid },
      }).then((res) => {
        const error = res.error.value;
        if (error) {
          console.error("Error unbinding sample to data: ", error);
        } else {
          const sample = sampleMap.value.get(sampleID);
          if (sample) {
            sample.value.boundData = sample.value.boundData.filter(
              bond => bond.id !== sampleBondID,
            );
          }
        }
      });
    } catch (error) {
      console.error("Error unbinding sample to data: ", error);
    }
  }

  //////////////////////////////////////////////////////////////////////////////////////
  // Returns
  //////////////////////////////////////////////////////////////////////////////////////

  return {
    ////////////////////
    // states
    // -----------------
    sampleMap,
    samples,
    currentSid,
    currentSamplePagination,
    updatedSamples,
    loaded,

    ////////////////////
    // getter
    // -----------------
    currentSample,
    samplesCount,
    hasPendingModification,
    ////////////////////
    // actions
    // -----------------
    addUpdatedSample,
    saveAndRefresh,
    updateSample,
    fetchSample,
    fetchSamples,
    createSample,
    deleteSample,
    fetchDataBoundSamples,
    fetchSampleBoundData,
    bindSampleToData,
    unbindSampleToData,
  };
});

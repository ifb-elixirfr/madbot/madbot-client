import type { Interactor } from "@/stores/useNodeStore";

import { useNodeStore } from "@/stores/useNodeStore";
import { useWorkspaceStore } from "@/stores/useWorkspaceStore";
import { defineStore } from "pinia";

export interface Connection {
  id: string;
  name: string;
  connector: Connector;
  root: Root | null;
  sharedParams: [SharedParams];
  credential: Credential;
  createdBy: Interactor;
  lastEditedTime: string;
  lastEditedBy: Interactor;
  status: "connected" | "not_connected" | "failed" | null;
  loading: boolean;
}

export interface Connector {
  id: string;
  name: string;
  description: string;
  documentation: string;
  icon: string;
  types: [string];
  fields: [];
  settings: [];
  metadataObjectTypes: [];
  supportedFiles: [];
}

interface Datalink {
  id: string;
  nodeID: string;
  connection: Connection;
  connectorName: Connector;
  data: Data | null;
}

interface Data {
  id: string;
  name: string;
  externalID: string;
  size: number;
  icon: string;
  status: string;
}
interface Credential {
  id: string;
  lastEditedTime: string;
  status: "connected" | "not_connected" | "failed" | null;
}

interface SharedParams {
  key: string;
  value: string;
}

interface Root {
  id: string;
  name: string;
}

export const useConnectionStore = defineStore("connection", () => {
  const workspaceStore = useWorkspaceStore();
  const nodeStore = useNodeStore();

  //////////////////////////////////////////////////////////////////////////////////////
  // States
  //////////////////////////////////////////////////////////////////////////////////////

  // ------------------------------------------------------------------------------------
  // Connectors
  // ------------------------------------------------------------------------------------

  const connectorMap = new Map<string, Connector>();

  // ------------------------------------------------------------------------------------
  // Connections
  // ------------------------------------------------------------------------------------

  const connectionMap = ref(new Map<string, Connection>());

  // ------------------------------------------------------------------------------------
  // Datalinks
  // ------------------------------------------------------------------------------------

  const datalinkMap = ref(new Map<string, Datalink>());
  const currentDlid: Ref<null | string> = ref(null);

  // ------------------------------------------------------------------------------------
  // Data
  // ------------------------------------------------------------------------------------

  const dataMap = ref(new Map<string, Data>());
  const currentDid: Ref<null | string> = ref(null);

  //////////////////////////////////////////////////////////////////////////////////////
  // Getters
  //////////////////////////////////////////////////////////////////////////////////////

  /**
   * Returns the status of a connection along with a color code.
   *
   * @param {string} connectionID - The ID of the connection to get the status for.
   * @param {string | null} [status] - The connection status. If provided the previous argument
   * will be ignored. The status can be one of the following values: "connected",
   * "not_connected", "failed".
   *
   * @returns {object} - An object containing the following properties:
   * - "status" for the status value.
   * - "color" for the color code.
   * - "text" for the status text.
   * - "hex" for the hex color code.
   *
   * The color code is the following:
   *  - "success" for "connected" status.
   *  - "warning" for "not_connected" status.
   *  - "error" for "failed" status.
   *  - "grey" for any other status.
   */
  function getStatus(connectionID: string, status?: string | null) {
    if (!status) {
      const connection = connectionMap.value.get(connectionID);
      if (connection) {
        status = connection.status;
      }
    }
    if (status) {
      switch (status) {
        case "connected":
          return { status: "connected", color: "success", text: "Connected", hex: "#4CAF50" };
        case "not_connected":
          return {
            status: "not_connected",
            color: "warning",
            text: "Not connected",
            hex: "#FB8C00",
          };
        case "failed":
          return { status: "failed", color: "error", text: "Failed", hex: "#B00020" };
      }
    }
    return { status: "unknown", color: "grey", text: "Unknown", hex: "#E3E3E3" };
  }

  function getConnection(connectionID: string) {
    return connectionMap.value.get(connectionID);
  }

  //////////////////////////////////////////////////////////////////////////////////////
  // Actions
  //////////////////////////////////////////////////////////////////////////////////////

  function asConnection(connection): Connection {
    return {
      id: connection.id,
      name: connection.name,
      connector: {
        id: connection.connector.id,
        name: connection.connector.name,
        description: connection.connector.description,
        documentation: connection.connector.documentation,
        icon: connection.connector.icon,
        types: connection.connector.types,
        fields: connection.connector.fields,
        settings: connection.connector.settings,
        metadataObjectTypes: connection.connector.metadata_object_types,
        supportedFiles: connection.connector.supported_files,
      } as Connector,
      root: connection.root,
      status: connection.status,
      sharedParams: connection.shared_params,
      credential: {
        id: connection.credential.id,
        lastEditedTime: connection.credential.last_edited_time,
        status: connection.credential.status,
      } as Credential,
      createdBy: connection.created_by,
      lastEditedTime: connection.last_edited_time,
      lastEditedBy: connection.last_edited_by,
      loading: false,
    } as Connection;
  }

  // ------------------------------------------------------------------------------------
  // Connectors
  // ------------------------------------------------------------------------------------

  async function fetchConnectors() {
    try {
      connectorMap.clear();
      const allConnectors = await recFetch("/api/connectors", {
        headers: { "X-Workspace": workspaceStore.currentWid },
      });
      for (const connector of allConnectors) {
        // Map each connector to the Connector interface and add/update in connectorMap
        const connectorObj: Connector = {
          id: connector.id,
          name: connector.name,
          description: connector.description,
          documentation: connector.documentation,
          icon: connector.icon,
          types: connector.types,
          fields: connector.fields,
          settings: connector.settings,
          supportedFiles: connector.supportedFiles,
        };

        connectorMap.set(connectorObj.id, connectorObj);
      }
    } catch (error) {
      console.error("Error fetching connectors: ", error);
    }
  }

  // ------------------------------------------------------------------------------------
  // Connections
  // ------------------------------------------------------------------------------------

  async function fetchConnections() {
    try {
      connectionMap.value.clear();
      const allConnections = await recFetch("/api/connections", {
        headers: { "X-Workspace": workspaceStore.currentWid },
      });
      for (const connection of allConnections) {
        // Map each connection to the Connection interface and add/update in connectionMap
        const connectionObj = asConnection(connection);

        connectionMap.value.set(connectionObj.id, connectionObj);
      }
    } catch (error) {
      console.error("Error fetching connections: ", error);
    }
  }

  async function fetchConnection(connectionId: string) {
    try {
      const response = await useAPIFetch(`/api/connections?search=${connectionId}`, {
        headers: { "X-Workspace": workspaceStore.currentWid },
      });
      const restConnections = response.data.value; // Access the value inside the Ref object
      // Check if restConnections is an array
      if (Array.isArray(restConnections.results)) {
        // Process array data
        restConnections.results.forEach((restConnection: any) => {
          // Map each connection to the Connection interface and add/update in connectionMap
          const connection = asConnection(restConnection);

          connectionMap.value.set(connection.id, connection);
        });
      } else {
        // Handle non-array responses
        console.error("Unexpected response format: ", restConnections);
      }
    } catch (error) {
      console.error("Error fetching connections: ", error);
    }
  }

  async function createConnection(
    connectorID: string,
    sharedParams,
    privateParams = {},
  ): Promise<Connection> {
    return new Promise((resolve, reject) => {
      try {
        /* eslint-disable camelcase */
        const body: any = {
          connector: connectorID,
          shared_params: sharedParams,
        };

        // Only add private params if it's not an empty connection
        if (Object.keys(privateParams).length > 0) {
          body.private_params = privateParams;
        }
        /* eslint-enable */

        useAPIFetch("/api/connections", {
          method: "POST",
          headers: { "X-Workspace": workspaceStore.currentWid },
          body,
        }).then(async (res) => {
          const error = res.error.value;
          if (error) {
            reject(error);
          }
          await fetchConnections();
          resolve(asConnection(res.data.value));
        });
      } catch (error) {
        console.error("Error creating connection: ", error);
        throw error;
      }
    });
  }

  async function updateConnection(connectionID: string, privateParams) {
    try {
      await useAPIFetch(`/api/connections/${connectionID}`, {
        method: "PATCH",
        headers: { "X-Workspace": workspaceStore.currentWid },
        body: {
          /* eslint-disable camelcase */
          private_params: privateParams,
          /* eslint-enable */
        },
      }).then(async (res) => {
        const error = res.error.value;
        if (error) {
          throw error;
        }
        fetchConnections();
      });
    } catch (error) {
      console.error("Error updating connection: ", error);
      throw error;
    }
  }

  /**
   * Checks the status of a connection by sending a PATCH request.
   *
   * This function updates the specified connection and refetches its data to ensure
   * that the latest information is available in the store. If the PATCH request fails,
   * it throws an error with the relevant details.
   *
   * @param {string} connectionID - The ID of the connection to check.
   * @returns {Promise<boolean>} - Returns true if the connection was successfully checked and updated.
   * @throws {Error} - Throws an error if the PATCH request fails or if there's an issue during the fetch.
   */
  async function checkConnection(connectionID: string) {
    try {
      const response = await useAPIFetch(`/api/connections/${connectionID}`, {
        method: "PATCH",
        body: {},
        headers: { "X-Workspace": workspaceStore.currentWid },
      });

      const error = response.error.value;
      if (error) {
        throw error;
      }

      // Refetch the updated connection and update in the store
      await fetchConnection(connectionID);

      return true;
    } catch (error) {
      console.error(`Error patching connection ${connectionID}:`, error);
      throw error;
    }
  }

  async function deleteConnection(connectionID: string) {
    try {
      await useAPIFetch(`/api/connections/${connectionID}`, {
        method: "DELETE",
        headers: { "X-Workspace": workspaceStore.currentWid },
      }).then(async (res) => {
        const error = res.error.value;
        if (error) {
          throw error;
        }
        fetchConnections();
      });
    } catch (error) {
      console.error("Error deleting connection: ", error);
      throw error;
    }
  }

  // ------------------------------------------------------------------------------------
  // Datalinks
  // ------------------------------------------------------------------------------------

  /**
   * Fetches datalinks from the server and updates the datalinkMap and dataMap.
   */
  async function fetchDatalinks() {
    try {
      // clear the datalinkMap
      datalinkMap.value.clear();

      // fetch all datalinks
      const allDatalinks = await recFetch(`/api/nodes/${nodeStore.currentNid}/datalinks`, {
        headers: { "X-Workspace": workspaceStore.currentWid },
      });
      for (const datalink of allDatalinks) {
        // construct the data object
        const dataObj: Data = {
          id: datalink.data.id,
          name: datalink.data.name,
          externalID: datalink.data.external_id,
          size: datalink.data.size,
          icon: datalink.data.icon,
          status: datalink.data.status,
        };
        // add/update the data in the dataMap
        dataMap.value.set(dataObj.id, dataObj);

        // Map the datalink to the Datalink interface and add/update in datalinkMap
        const datalinkObj: Datalink = {
          id: datalink.id,
          nodeID: datalink.node,
          connection: datalink.connection,
          connectorName: datalink.connector,
          data: dataObj,
        };

        datalinkMap.value.set(datalinkObj.id, datalinkObj);
      }
    } catch (error) {
      console.error("Error fetching datalink: ", error);
    }
  }

  /**
   * Creates a datalink between a node and a data ID.
   * @param {string} nodeID - The ID of the node.
   * @param {string} dataId - The ID of the data.
   * @returns {Promise<boolean>} - A promise that resolves to true if the datalink is created successfully, or false otherwise.
   */
  async function createDatalink(nodeID, dataId) {
    return new Promise((resolve, reject) => {
      try {
        useAPIFetch(`/api/nodes/${nodeID}/datalinks`, {
          method: "POST",
          body: {
            data: dataId,
          },
          headers: { "X-Workspace": workspaceStore.currentWid },
        }).then(async (res) => {
          const error = res.error.value;
          if (error) {
            throw error;
          }
          // append the data to the datalinkMap
          const datalinkObj: Datalink = {
            id: res.data.value.id,
            nodeID: res.data.value.node,
            connection: res.data.value.connection,
            connectorName: res.data.value.connector,
            data: res.data.value.data,
          };
          datalinkMap.value.set(datalinkObj.id, datalinkObj);

          // return the status of the creation
          resolve(true);
        });
      } catch (error) {
        // Handle the error gracefully or log it if needed
        console.error("Error creating datalink:", error);
        reject(error);
      }
    });
  }

  // ------------------------------------------------------------------------------------
  // Data
  // ------------------------------------------------------------------------------------

  /**
   * Fetches data from the server and updates the dataMap.
   */
  async function fetchData() {
    try {
      dataMap.value.clear();
      const allData = await recFetch(`/api/data`, {
        headers: { "X-Workspace": workspaceStore.currentWid },
      });
      for (const data of allData) {
        // Map each data to the Data interface and add/update in dataMap
        const dataObj: Data = {
          id: data.id,
          name: data.name,
          externalID: data.external_id,
          size: data.size,
          icon: data.icon,
          status: data.status,
        };

        dataMap.value.set(dataObj.id, dataObj);
      }
    } catch (error) {
      console.error("Error fetching data: ", error);
    }
  }

  /**
   * Creates data with the given connection ID and data object ID.
   * @param {string} connectionID - The ID of the connection.
   * @param {string} dataObjectID - The ID of the data object.
   * @returns {Promise<string | boolean>} - A promise that resolves to the ID of the created data, or false if an error occurred.
   */
  async function createData(connectionID, dataObjectID) {
    return new Promise((resolve, reject) => {
      try {
        useAPIFetch("/api/data", {
          method: "POST",
          body: {
            /* eslint-disable camelcase */
            connection: connectionID,
            external_id: dataObjectID,
            /* eslint-enable */
          },
          headers: { "X-Workspace": workspaceStore.currentWid },
        }).then((res) => {
          const error = res.error.value;
          if (error) {
            throw error;
          }
          // append the data to the dataMap
          const dataObj: Data = {
            id: res.data.value.id,
            name: res.data.value.name,
            externalID: res.data.value.external_id,
            size: res.data.value.size,
            icon: res.data.value.icon,
            status: res.data.value.status,
          };
          dataMap.value.set(dataObj.id, dataObj);

          // return the ID of the data
          resolve(dataObj.id);
        });
      } catch (error) {
        // Handle the error gracefully or log it if needed
        console.error("Error creating data:", error);
        reject(error);
      }
    });
  }

  return {
    ////////////////////
    // actions
    // -----------------
    // connectors
    connectorMap,
    // -----------------
    // connections
    connectionMap,
    // -----------------
    // datalinks
    datalinkMap,
    currentDlid,
    // -----------------
    // data
    dataMap,
    currentDid,

    ////////////////////
    // getters
    // -----------------
    getStatus,
    getConnection,

    ////////////////////
    // actions
    // -----------------
    // connectors
    fetchConnectors,
    // -----------------
    // connections
    asConnection,
    fetchConnection,
    fetchConnections,
    createConnection,
    updateConnection,
    deleteConnection,
    checkConnection,
    // -----------------
    // datalinks
    fetchDatalinks,
    createDatalink,
    // -----------------
    // data
    fetchData,
    createData,
  };
});

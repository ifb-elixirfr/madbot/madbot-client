import type { Ref } from "vue";

import { useWorkspaceStore } from "@/stores/useWorkspaceStore";
import { defineStore } from "pinia";

interface Notification {
  id: number;
  actorObject: {
    fullName: string;
    email: string;
  };
  verb: string;
  actionObject: {
    id: number;
    obj: string;
    extraInfo: string;
  };
  targetObject: {
    id: number;
    obj: string;
    extraInfo: string;
    name: string;
  };
  level: string;
  timestamp: string;
  description: string;
  unread: boolean;
  archived: boolean;
}

export const useNotificationStore = defineStore("notification", () => {
  const workspaceStore = useWorkspaceStore();

  ////////////////////////////////////////////////////////////////////////
  // States
  ////////////////////////////////////////////////////////////////////////

  const notifications = ref(new Map<number, Notification>());
  const countUnreadMessage: Ref<null | number> = ref(0);
  const totalPages: Ref<null | number> = ref(-1);
  const currentPage: Ref<null | number> = ref(1);

  ////////////////////////////////////////////////////////////////////////
  // Actions
  ////////////////////////////////////////////////////////////////////////

  /**
   * The function `formatTimestamp` takes a timestamp and returns it formatted as "YYYY-MM-DD
   * HH:MM:SS".
   * @param timestamp - The `formatTimestamp` function takes a timestamp as input and formats
   * it into a string in the format "YYYY-MM-DD HH:MM:SS". The timestamp should be a valid
   * timestamp that can be converted into a Date object.
   * @returns The function `formatTimestamp` takes a timestamp as input, formats it into the
   * string format "YYYY-MM-DD HH:MM:SS", and returns the formatted date and time string.
   */
  function formatTimestamp(timestamp) {
    const dateFormatted = new Date(timestamp);
    const year = dateFormatted.getFullYear();
    const month = String(dateFormatted.getMonth() + 1).padStart(2, "0");
    const day = String(dateFormatted.getDate()).padStart(2, "0");
    const hours = String(dateFormatted.getHours()).padStart(2, "0");
    const minutes = String(dateFormatted.getMinutes()).padStart(2, "0");
    const seconds = String(dateFormatted.getSeconds()).padStart(2, "0");

    // Format in the required format (e.g., YYYY-MM-DD HH:MM:SS)
    const formattedDateTime = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;

    return formattedDateTime;
  }

  /**
   * The function fetchNotifications asynchronously fetches notifications from an API and
   * updates the current page and total pages accordingly.
   * @param queryParams - The `queryParams` parameter in the `fetchNotifications` function is
   * used to pass additional query parameters to the API endpoint when making the request.
   * These parameters can be used to filter, sort, or customize the data returned by the API.
   */
  async function fetchNotifications(queryParams) {
    if (
      (workspaceStore.currentWid && totalPages.value === -1)
      || ((currentPage?.value ?? 0) < (totalPages?.value ?? 0))
    ) {
      currentPage.value
        = currentPage.value === totalPages.value || totalPages.value === -1
          ? 1
          : (currentPage?.value ?? 0) + 1;

      let url = `/api/notifications?p=${currentPage.value}`;

      if (queryParams) {
        url = `${url}&${queryParams}`;
      }

      const { data: resNotification } = await useAPIFetch(url, {
        headers: { "X-Workspace": workspaceStore.currentWid },
      });

      totalPages.value = resNotification.value.total_pages;
      countUnreadMessage.value = resNotification.value.extra_data.countAllUnread;

      resNotification.value.results.forEach((notification) => {
        notifications.value.set(notification.id, {
          id: notification.id,
          actorObject: {
            fullName: notification.actor_object.full_name,
            email: notification.actor_object.email,
          },
          verb: notification.verb,
          actionObject: {
            id: notification.action_object.id,
            obj: notification.action_object.obj,
            extraInfo: notification.action_object.extra_info,
          },
          targetObject: {
            id: notification.target_object.id,
            obj: notification.target_object.obj,
            extraInfo: notification.target_object.extra_info,
            name: notification.target_object.name,
          },
          level: notification.level,
          timestamp: formatTimestamp(notification.timestamp),
          description: notification.description,
          unread: notification.unread,
          archived: notification.archived,
        } as Notification);
      });
    }
  }

  /**
   * The function `changeReadStatus` updates the read status of a notification and updates
   * the count of unread messages.
   * @param {number} id - The `id` parameter is the unique identifier of the notification
   * that we want to change the read status for.
   */
  function changeReadStatus(id: number) {
    const notification = notifications.value.get(id);

    if (notification && !notification.archived) {
      if (notification.unread) {
        useAPIFetch(`/api/notifications/${notification.id}`, {
          method: "PUT",
          body: { unread: false },
          headers: { "X-Workspace": workspaceStore.currentWid },
        });

        notifications.value.set(id, {
          ...notification,
          unread: false,
        });

        if (countUnreadMessage.value !== null) {
          countUnreadMessage.value -= 1;
        }
      } else {
        useAPIFetch(`/api/notifications/${notification.id}`, {
          method: "PUT",
          body: { unread: true },
          headers: { "X-Workspace": workspaceStore.currentWid },
        });

        notifications.value.set(id, {
          ...notification,
          unread: true,
        });
        countUnreadMessage.value = countUnreadMessage.value ? countUnreadMessage.value + 1 : 1;
      }
    }
  }

  function markAsRead(id: number) {
    const notification = notifications.value.get(id);

    if (notification && !notification.archived) {
      if (notification.unread) {
        useAPIFetch(`/api/notifications/${notification.id}`, {
          method: "PUT",
          body: { unread: false },
          headers: { "X-Workspace": workspaceStore.currentWid },
        });

        notifications.value.set(id, {
          ...notification,
          unread: false,
        });

        if (countUnreadMessage.value !== null) {
          countUnreadMessage.value -= 1;
        }
      }
    }
  }

  /**
   * The function "markAsReadAll" updates the status of all notifications to "read" and
   * sets the count of unread messages to zero.
   */
  function markAsReadAll() {
    for (const key of notifications.value.keys()) {
      markAsRead(key);
    }
    countUnreadMessage.value = 0;
  }

  /**
   * The function changes the archived status of a notification and updates the unread
   * message count if necessary. Archiving a single unread notification should mark it as read
   * @param {number} id - The `id` parameter is a number that represents the unique
   * identifier of a notification.
   */
  function changeArchivedStatus(id: number) {
    const notification = notifications.value.get(id);
    if (notification) {
      if (notification?.archived) {
        useAPIFetch(`/api/notifications/${id}`, {
          method: "PUT",
          body: { archived: false },
          headers: { "X-Workspace": workspaceStore.currentWid },
        });
      } else {
        useAPIFetch(`/api/notifications/${id}`, {
          method: "PUT",
          body: { archived: true, unread: false },
          headers: { "X-Workspace": workspaceStore.currentWid },
        });

        if (notification.unread && countUnreadMessage.value) {
          countUnreadMessage.value -= 1;
        }
      }
      notifications.value.delete(id);
    }
  }

  /**
   * The function "archivedAll" archives all notifications, updates their status to
   * deleted, and resets the count of unread messages to zero.
   */
  function archivedAll() {
    for (const key of notifications.value.keys()) {
      useAPIFetch(`/api/notifications/${key}`, {
        method: "PUT",
        body: { archived: true },
        headers: { "X-Workspace": workspaceStore.currentWid },
      });

      notifications.value.delete(key);
    }
    countUnreadMessage.value = 0;
    clear();
  }

  /**
   * The function restores all archived notifications by making a PUT request to the API to
   * update the "deleted" property to false and then removing the notification from the
   * notifications collection.
   */
  function restoreArchivedAll() {
    for (const key of notifications.value.keys()) {
      useAPIFetch(`/api/notifications/${key}`, {
        method: "PUT",
        body: { archived: false },
        headers: { "X-Workspace": workspaceStore.currentWid },
      });
      notifications.value.delete(key);
    }
  }

  /**
   * The function `clearNotifications` deletes all notifications and sets the count of
   * unread messages to zero.
   */
  function clearNotifications() {
    useAPIFetch("/api/notifications&archived=True&unread=False", {
      method: "DELETE",
      headers: { "X-Workspace": workspaceStore.currentWid },
    });
    notifications.value.clear();
  }

  /**
   * The function `clearANotification` deletes a notification with a specific key using an
   * API call and removes it from the notifications list.
   * @param key - The `key` parameter in the `clearANotification` function is used to
   * identify the specific notification that needs to be cleared. It is typically a unique
   * identifier or key associated with the notification in the system.
   */
  function clearANotification(key) {
    useAPIFetch(`/api/notifications/${key}`, {
      method: "DELETE",
      headers: { "X-Workspace": workspaceStore.currentWid },
    });
    notifications.value.delete(key);
  }

  async function clear() {
    notifications.value.clear();
    countUnreadMessage.value = 0;
    currentPage.value = 1;
    totalPages.value = -1;
  }

  return {
    // States
    notifications,
    countUnreadMessage,
    totalPages,
    currentPage,
    // actions
    fetchNotifications,
    changeReadStatus,
    markAsRead,
    markAsReadAll,
    changeArchivedStatus,
    archivedAll,
    restoreArchivedAll,
    clearNotifications,
    clearANotification,
    clear,
  };
});

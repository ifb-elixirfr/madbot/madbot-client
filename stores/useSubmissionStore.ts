import { useWorkspaceStore } from "@/stores/useWorkspaceStore";
import { defineStore } from "pinia";

export interface Interactor {
  object: string;
  id: string;
}

export interface Submission {
  id: string | null;
  title: string | null;
  status: "draft" | "generating metadata" | "ready" | "submission in progress" | "submitted" | "failed" | "update available" | null;
  connectionID: string | null;
  settings: object;
  lastFailedAsyncTask: {
    taskName: string | null;
    taskId: string | null;
    errorMessage: string | null;
    timestamp: string | null;
  } | null;
  createdTime: string | null;
  createdBy: Interactor | null;
  lastEditedTime: string | null;
  lastEditedBy: Interactor | null;
}

interface Role {
  role: string | null;
  description: string | null;
}
interface Member {
  id: string | null;
  user: User;
  role: Role;
  createdTime: string | null;
  createdBy: Interactor | null;
  lastEditedTime: string | null;
  lastEditedBy: Interactor | null;
}

interface User {
  id: string;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
}

interface MetadataField {
  slug: string;
  name: string;
  level: string;
  schema: string;
}

interface ValueField {
  value: any;
  source: {
    object: string; // object type (e.g "Node", "Sample" , "Data", "DataAssociation")
    id: string;
    name?: string; // optional because it's not provided for data associations
    node?: { // when the source is a sample
      id: string;
      name: string;
    };
    datalink?: { // when the source is a data
      id: string;
      node: {
        id: string;
        name: string;
      };
    };
    data?: { // when the source is a data association
      object: string; // object type (e.g "Data")
      id: string;
      name: string;
      datalink: {
        id: string;
        node: {
          id: string;
          name: string;
        };
      };
    }[];
  };
}

export interface Metadata {
  id: string;
  submission: string;
  submissionMetadataObject: string;
  field: MetadataField;
  value: ValueField;
  candidateValues: ValueField[];
  isValid: boolean;
  createdBy: Interactor;
  createdTime: string;
  lastEditedBy: Interactor;
  lastEditedTime: string;
}

interface Completeness {
  mandatory: { complete: number; total: number };
  recommended: { complete: number; total: number };
  optional: { complete: number; total: number };
}

interface MetadataObject {
  id: string;
  summary: object;
  type: string;
  completeness: Completeness;
  crossValidation: object;
  metadata: {
    count: number;
    related: string;
  } | null;
}

export const useSubmissionStore = defineStore("submission", () => {
  const workspaceStore = useWorkspaceStore();

  //////////////////////////////////////////////////////////////////////////////////////
  // States
  //////////////////////////////////////////////////////////////////////////////////////

  // ------------------------------------------------------------------------------------
  // Submissions
  // ------------------------------------------------------------------------------------
  const submissionMap = ref(new Map<string, Submission>());
  const metadataObjectsMap = ref(new Map<string, MetadataObject>());
  const currentSubid: Ref<null | string> = ref(null);
  const updatedSubmissions = ref([] as string[]);

  // ------------------------------------------------------------------------------------
  // Members
  // ------------------------------------------------------------------------------------
  const currentMid: Ref<null | string> = ref(null);
  const memberMap = ref(new Map<string, Ref<Member>>());
  const submissionRoles = ref(new Map<string, Role>());

  // ------------------------------------------------------------------------------------
  // Metadata
  // ------------------------------------------------------------------------------------
  const metadataMap = ref(new Map<string, Map<string, Map<string, Metadata>>>());
  // Structure: Map<type, Map<objectId, Map<metadataId, Metadata>>>

  //////////////////////////////////////////////////////////////////////////////////////
  // Getters
  //////////////////////////////////////////////////////////////////////////////////////

  // ------------------------------------------------------------------------------------
  // Submissions
  // ------------------------------------------------------------------------------------

  const submissionCount = computed(() => submissionMap.value.size);
  const hasPendingModification = computed(() => updatedSubmissions.value.length > 0);

  const currentSubmission = computed(() => {
    if (currentSubid.value !== null && submissionMap.value.has(currentSubid.value)) {
      return submissionMap.value.get(currentSubid.value);
    }

    return {
      id: null,
      title: null,
      status: null,
      connectionID: null,
      settings: {},
      lastFailedAsyncTask: {
        taskName: null,
        taskId: null,
        errorMessage: null,
        timestamp: null,
      },
      createdTime: null,
      createdBy: null,
      lastEditedTime: null,
      lastEditedBy: null,
    } as Submission;
  });

  const isLocked = computed(() => currentSubmission.value.status === "generating metadata" || currentSubmission.value.status === "submission in progress");

  /**
   * Get the badge color and label for the submission status.
   * @param {string} status - The status of the submission (e.g., "draft", "submitted").
   * @returns {object} - An object containing `color` and `label` properties for the status badge.
   */
  function getStatusBadge(status: string) {
    switch (status) {
      case "draft":
        return { color: "grey", label: "Draft" };
      case "generating metadata":
        return { color: "orange", label: "Generating metadata" };
      case "ready":
        return { color: "blue", label: "Ready" };
      case "submitted":
        return { color: "green", label: "Submitted" };
      case "failed":
        return { color: "red", label: "Failed" };
      case "update available":
        return { color: "yellow", label: "Update available" };
      default:
        return { color: "grey", label: "Unknown" };
    }
  }

  // ------------------------------------------------------------------------------------
  // Members
  // ------------------------------------------------------------------------------------

  const currentMember = computed(() => {
    if (currentMid.value !== null && memberMap.value.has(currentMid.value)) {
      return memberMap.value.get(currentMid.value);
    }
    return undefined;
  });

  // ------------------------------------------------------------------------------------
  // Metadata Objects
  // ------------------------------------------------------------------------------------

  const getMetadataObjectsByType = (type: string) =>
    computed(() => {
      const objects = Array.from(metadataObjectsMap.value.values());
      return objects.filter(obj => obj.type === type);
    });

  /**
   * Return the metadata object status as a "météo". In French, the term "météo" (weather) is often used metaphorically
   * to describe the availability and performance of digital services. Just like a weather forecast predicts sunshine
   * or storms, a "météo" for online services indicates whether they are running smoothly or experiencing issues.
   * Here we will use weather-like symbols to describe the completion status of metadata objects in a more intuitive way.
   * @param {string} type - The type of metadata object
   * @returns {object} - The metadata object meteo
   */
  function getMetadataObjectStatus(type: string) {
    const objects = getMetadataObjectsByType(type).value;
    const statusMapping = {
      1: { symbol: "🦄", message: "Perfect! All fields are filled. Amazing work!" },
      2: { symbol: "☀️", message: "Great! All recommended fields are complete. Keep going!" },
      3: { symbol: "⛅️", message: "Good job! All mandatory fields are complete. Consider adding more details." },
      4: { symbol: "⚠️", message: "Oops! Some mandatory fields are missing. Let's complete them." },
    };

    // Handle empty object list
    if (objects.length === 0) {
      return { symbol: null, message: null };
    }

    // Check each object's completeness
    const status = objects.reduce((acc: number, obj) => {
      const completeness = obj.completeness;
      const hasValidationErrors = Object.keys(obj.crossValidation).length > 0;

      // First check if any mandatory fields are missing
      if (completeness.mandatory.complete < completeness.mandatory.total || hasValidationErrors) {
        return 4;
      }

      // If all mandatory are complete but not all recommended
      if (completeness.recommended.complete < completeness.recommended.total) {
        return Math.max(acc, 3);
      }

      // if all recommended fields are complete but not all optional
      if (completeness.optional.complete < completeness.optional.total) {
        return Math.max(acc, 2);
      }

      // If we get here, all fields are complete
      return Math.max(acc, 1);
    }, 1);

    return statusMapping[status];
  }

  // ------------------------------------------------------------------------------------
  // Metadata
  // ------------------------------------------------------------------------------------

  /**
   * Get metadata from the metadata map:
   * - If only type is provided, returns all metadata for that type
   * - If type and objectId are provided, returns all metadata for that object
   * - If type, objectId, and metadataId are provided, returns specific metadata
   * @param type - The type of metadata object
   * @param [objectId] - Optional. The ID of the metadata object
   * @param [metadataId] - Optional. The ID of the specific metadata to retrieve
   * @returns The metadata object(s)
   */
  function getMetadata(type: string, objectId?: string, metadataId?: string) {
    const typeMap = metadataMap.value.get(type);
    if (!typeMap) return metadataId ? null : new Map();

    // If no objectId provided, return all metadata for the type
    if (!objectId) {
      return typeMap;
    }

    const objectMap = typeMap.get(objectId);
    if (!objectMap) return metadataId ? null : new Map();

    return metadataId ? objectMap.get(metadataId) : objectMap;
  }

  function clearMetadata() {
    metadataMap.value = new Map<string, Map<string, Map<string, Metadata>>>();
  }

  //////////////////////////////////////////////////////////////////////////////////////
  // Actions
  //////////////////////////////////////////////////////////////////////////////////////

  // ------------------------------------------------------------------------------------
  // Submissions
  // ------------------------------------------------------------------------------------

  /**
   * The function adds a submission ID to an array of updated submission IDs if it is not already
   * present.
   * @param {string} nid - The `subid` parameter is a string that represents the ID of a
   * submission.
   */
  function addUpdatedSubmission(nid: string) {
    if (!updatedSubmissions.value.includes(nid)) {
      updatedSubmissions.value.push(nid);
    }
  }

  /**
   * The function fetchSubmissions fetches submissions from an API and updates the Submissions map
   */
  async function fetchSubmissions() {
    if (workspaceStore.currentWid) {
      try {
        const allSubmissions = await recFetch("/api/submissions", {
          headers: { "X-Workspace": workspaceStore.currentWid },
        });

        allSubmissions.forEach(async (submission) => {
          submissionMap.value.set(submission.id, {
            id: submission.id,
            title: submission.title,
            status: submission.status,
            connectionID: submission.connection.id,
            settings: submission.settings,
            lastFailedAsyncTask: {
              taskName: submission.last_failed_async_task?.task_name,
              taskId: submission.last_failed_async_task?.task_id,
              errorMessage: submission.last_failed_async_task?.error_message,
              timestamp: submission.last_failed_async_task?.timestamp,
            },
            createdTime: submission.created_time,
            createdBy: submission.created_by,
            lastEditedTime: submission.created_time,
            lastEditedBy: submission.last_edited_by,
          } as Submission);
        });
      } catch (error) {
        console.error("Error fetching submissions:", error);
      }
    }
  }

  /**
   * The function fetchSubmission fetches a single submission from an API and updates the Submissions map
   */
  async function fetchSubmission(subid: string) {
    if (workspaceStore.currentWid && subid) {
      try {
        const { data } = await useAPIFetch(`/api/submissions/${subid}`, {
          method: "GET",
          headers: { "X-Workspace": workspaceStore.currentWid },
        });
        submissionMap.value.set(subid, {
          id: data.value.id,
          title: data.value.title,
          status: data.value.status,
          connectionID: data.value.connection.id,
          settings: data.value.settings,
          lastFailedAsyncTask: {
            taskName: data.value.last_failed_async_task?.task_name,
            taskId: data.value.last_failed_async_task?.task_id,
            errorMessage: data.value.last_failed_async_task?.error_message,
            timestamp: data.value.last_failed_async_task?.timestamp,
          },
          createdTime: data.value.created_time,
          createdBy: data.value.created_by,
          lastEditedTime: data.value.last_edited_time,
          lastEditedBy: data.value.last_edited_by,
        } as Submission);
      } catch (error) {
        console.error("Error fetching submission", subid, ":", error);
      }
    }
  }

  /**
   * The function fetchSubmissionSettings fetches submission settings from an API and updates the Submissions map
   */
  async function fetchSubmissionSettings() {
    const urlSettings = currentSubmission.value.settings.related;
    if (workspaceStore.currentWid && currentSubid) {
      try {
        const allSubmissionsSettings = await recFetch(urlSettings, { headers: { "X-Workspace": workspaceStore.currentWid } });

        const transformedSettings = Object.fromEntries(
          allSubmissionsSettings.map((setting) => {
            let value = setting.value;
            if (typeof value === "string") {
              try {
                value = JSON.parse(value.replace(/'/g, "\""));
              } catch {
                // Keep value as string if parsing fails
                value = setting.value;
              }
            }
            return [setting.field, { value }];
          }),
        );

        // Map field names to their respective settings IDs
        const fieldToIdMap = Object.fromEntries(
          allSubmissionsSettings.map(setting => [setting.field, setting.id]),
        );

        // Update current submission settings
        const currentSubmission = submissionMap.value.get(currentSubid.value);
        if (currentSubmission) {
          currentSubmission.settings = transformedSettings;
          currentSubmission.fieldToIdMap = fieldToIdMap; // Save the mapping
          submissionMap.value.set(currentSubid.value, currentSubmission);
        } else {
          console.warn(`Submission with ID ${currentSubid.value} not found.`);
        }
      } catch (error) {
        console.error("Error fetching submission settings:", error);
      }
    } else {
      console.warn("Workspace ID or current submission ID is missing.");
    }
  }

  /**
   * Updates submission settings with the provided settings.
   * @param {string} submissionId - ID of the submission to update.
   * @param {string} settingsId - ID of the settings to update.
   * @param {object} payload - Payload containing the new settings.
   * @param {any} payload.value - The value to be updated for the specified setting.
   */
  async function updateSubmissionSettings(submissionId: string, settingsId: string, payload: { value: any }) {
    if (!workspaceStore.currentWid || !submissionId || !settingsId) {
      console.warn("Missing Workspace ID, Submission ID, or Settings ID.");
      return;
    }

    try {
      // Send update request to the API for a single setting
      await recFetch(`/api/submissions/${submissionId}/settings/${settingsId}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          "X-Workspace": workspaceStore.currentWid,
        },
        body: JSON.stringify(payload),
      });
    } catch (error) {
      console.error(`Error updating setting ${settingsId}:`, error);
    }
  }

  /**
   * The function `createSubmission` is an asynchronous function that creates a new submission
   * with the specified parameters and returns the created submission's value.
   * @param {string} [title] - The title parameter is a string that represents the title of
   * the submission. It is optional and has a default value of an empty string ("").
   * @param {string} [connectionID] - The `connectionID` parameter is a string that
   * represents the ID of the connection to create the submission with.
   * @param {object} [settings] - The "settings" parameter is used to specify settings for the
   * submission. It is by default empty.
   * @returns the created submission.
   */
  async function createSubmission(
    title: string = "",
    connectionID: string,
    settings: object = {},
  ) {
    return new Promise((resolve, reject) => {
      try {
        useAPIFetch("/api/submissions", {
          method: "POST",
          body: {
            title: title,
            connection: connectionID,
            settings: settings,
          },
          headers: { "X-Workspace": workspaceStore.currentWid },
        }).then(async (res) => {
          const error = res.error.value;
          if (error) {
            throw error;
          }
          resolve(res.data);
        });
      } catch (error) {
        console.error("Error creating submission:", error);
        reject(error);
      }
    });
  }

  /**
   * The function `updateSubmission` is an asynchronous function that updates a submission
   * with the specified parameters and returns the updated submission's value.
   * @param {string} submissionID - The submissionID parameter is a string that represents
   * the ID of the submission to update.
   * @param {string} [title] - The title parameter is a string that represents the title of
   * the submission.
   * @returns the updated submission.
   */
  async function updateSubmission(submissionID: string, title?: string) {
    try {
      const { data: submission } = await useAPIFetch(`/api/submissions/${submissionID}`, {
        method: "PATCH",
        body: { title: title },
        headers: { "X-Workspace": workspaceStore.currentWid },
      });
      return submission.value;
    } catch (error) {
      console.error("Error updating submission:", error);
    }
  }

  /**
   * The function `deleteSubmission` is an asynchronous function that deletes a submission by making a
   * DELETE request to the specified API endpoint.
   * @param submissionID - The `submissionID` parameter is the unique identifier of the submission that you
   * want to delete. It is used to construct the URL for the API endpoint that will handle
   * the deletion of the submission.
   */
  async function deleteSubmission(submissionID: string) {
    try {
      await useAPIFetch(`/api/submissions/${submissionID}`, {
        method: "DELETE",
        headers: { "X-Workspace": workspaceStore.currentWid },
      }).then(() => {
        submissionMap.value.delete(submissionID);
      });
    } catch (error) {
      console.error("Error deleting submission:", error);
    }
  }

  /**
   * The function `saveAndRefresh` updates submissions by sending PUT requests through an API and
   * removes the updated submissions from the `updatedSubmissions` array.
   */

  async function saveAndRefresh() {
    updatedSubmissions.value.forEach(async (subid) => {
      if (submissionMap.value.has(subid)) {
        const tempSubmission = submissionMap.value.get(subid);
        if (tempSubmission && tempSubmission.title !== null) {
          updateSubmission(subid, tempSubmission.title);
        }
      }

      const index = updatedSubmissions.value.indexOf(subid, 0);
      if (index > -1) {
        updatedSubmissions.value.splice(index, 1);
      }
    });
  }

  // ------------------------------------------------------------------------------------
  // Submission members
  // ------------------------------------------------------------------------------------

  /**
   * The function `setMember` sets a member in a map with the given member's properties.
   * @param member - The `member` parameter is an object that represents a member of a submission.
   * It has the following properties:
   */
  function setMember(member) {
    memberMap.value.set(
      member.id,
      ref({
        id: member.id,
        user: {
          id: member.user.id,
          username: member.user.username,
          firstName: member.user.first_name,
          lastName: member.user.last_name,
          email: member.user.email,
        } as User,
        role: {
          role: member.role,
          description: submissionRoles.value.get(member.role)?.description,
        } as Role,
        createdTime: member.created_time,
        createdBy: member.created_by,
        lastEditedTime: member.last_edited_time,
        lastEditedBy: member.last_edited_by,
      } as Member),
    );
  }

  /**
   * The function `updateMemberRole` update a member's role with an API call
   * and modify the member map.
   */
  async function updateMemberRole(member: Member) {
    await useAPIFetch(`/api/submissions/${currentSubid.value}/members/${member.id}`, {
      method: "PUT",
      body: {
        role: member.role.role,
      },
      headers: { "X-Workspace": workspaceStore.currentWid },
    }).then(() => {
      fetchMembers();
    });
  }

  /**
   * The function `createMember` is an asynchronous function that creates a new member with the
   * specified parameters
   * @param {User} [userID] - The ID of the user to create a member for.
   * @param {string} [role] - The role of the member.
   * @returns the value of the member created.
   */
  async function createMember(userID: string, role: string) {
    await useAPIFetch(`/api/submissions/${currentSubid.value}/members`, {
      method: "POST",
      body: {
        user: userID,
        role: role,
      },
      headers: { "X-Workspace": workspaceStore.currentWid },
    }).then(() => {
      fetchMembers();
    });
  }

  /**
   * The function `deleteMember` delete a member with an API call
   * and modify the member map.
   */
  async function deleteMember(member: Member) {
    return new Promise((resolve) => {
      useAPIFetch(`/api/submissions/${currentSubid.value}/members/${member.id}`, {
        method: "DELETE",
        headers: { "X-Workspace": workspaceStore.currentWid },
      })
        .then((error: error) => {
          if (error.value === undefined) {
            fetchMembers();
            resolve(true);
          } else {
            resolve(false);
          }
        })
        .catch((error) => {
          console.error("Error deleting member:", error);
          resolve(false);
        });
    });
  }

  /**
   * The function `fetchSubmissionRoles` fetches submissions' roles and their
   * descriptions from an API and stores them in a map.
   */
  async function fetchSubmissionRoles() {
    const { data: restRoles } = await useAPIFetch("/api/roles");

    restRoles.value.submissions.forEach((role) => {
      submissionRoles.value.set(role.role, role as Role);
    });
  }

  /**
   * The function `fetchMembers` fetches members from an API and stores them in a map.
   */
  async function fetchMembers() {
    if (workspaceStore.currentWid && currentSubid.value !== null) {
      try {
        memberMap.value.clear();
        const allMembers = await recFetch(`/api/submissions/${currentSubid.value}/members`, {
          headers: { "X-Workspace": workspaceStore.currentWid },
        });

        const { data: session } = useAuth();
        const user = session.value?.user;

        allMembers.forEach((member) => {
          setMember(member);
          if (member.user.username === user?.username) {
            currentMid.value = member.id;
          }
        });
      } catch (error) {
        console.error("Error fetching members:", error);
      }
    }
  }

  /**
   * The function `searchMember` search members from an API and return the result as a list.
   */
  async function searchMember(searchTerm: string) {
    return new Promise((resolve) => {
      recFetch(`/api/workspaces/${workspaceStore.currentWid}/members?search=${searchTerm}`)
        .then((searchWorkspaceMember) => {
          const { data: session } = useAuth();
          const currentUser = session.value?.user;

          /* The above code is using the `reduceRight` method on the `searchWorkspaceMember` array to
              iterate over each element from right to left. The function provided as an argument to
              `reduceRight` is called for each element in the array, with the `acc` parameter
              representing the accumulator value, `item` representing the current element being
              processed, `index` representing the index of the current element, and `object`
              representing the array itself. The function is expected to return the updated
              accumulator value for the next iteration. */
          searchWorkspaceMember.reduceRight((acc, item, index, object) => {
            const filterMemberUser = [...memberMap.value.values()].find(
              /* The above code is a TypeScript arrow function that takes an `entry` parameter and
                  checks if the `id` of the user in the `entry.value` object is equal to the `id` of
                  the `WorkspaceMember`. If the condition is met, it will return `true`, otherwise it
                  will return `false`. */
              entry => entry.value.user.id === item.user.id,
            );
            if (item.user.username === currentUser?.username || filterMemberUser) {
              object.splice(index, 1);
            } else {
              object[index] = item.user;
            }
            return acc;
          }, []);
          resolve(searchWorkspaceMember);
        })
        .catch((error) => {
          console.error("Error searching members:", error);
          resolve([]);
        });
    });
  }

  // ------------------------------------------------------------------------------------
  // Metadata objects
  // ------------------------------------------------------------------------------------

  async function fetchMetadataObjects() {
    if (workspaceStore.currentWid && currentSubid.value !== null) {
      try {
        metadataObjectsMap.value.clear();
        const url = `/api/submissions/${currentSubid.value}/metadata_objects`;
        const submissionObjects = await recFetch(url, {
          headers: { "X-Workspace": workspaceStore.currentWid },
        });

        submissionObjects.forEach((object: any) => {
          metadataObjectsMap.value.set(object.id, {
            id: object.id,
            summary: object.summary,
            type: object.type,
            completeness: object.completeness,
            crossValidation: object.cross_validation || [],
            metadata: object.metadata,
          } as MetadataObject);
        });
      } catch (error) {
        console.error("Error fetching metadata objects:", error);
      }
    }
  }

  // ------------------------------------------------------------------------------------
  // Metadata
  // ------------------------------------------------------------------------------------

  async function fetchMetadataForObject(objectId: string) {
    if (workspaceStore.currentWid && currentSubid.value !== null) {
      try {
        const url = `/api/submissions/${currentSubid.value}/metadata_objects/${objectId}/metadata`;
        const metadata = await recFetch(url, {
          headers: { "X-Workspace": workspaceStore.currentWid },
        });

        // Get the object type from metadataObjectsMap
        const object = metadataObjectsMap.value.get(objectId);
        if (!object) return;

        const type = object.type;

        // Initialize maps if they don't exist
        if (!metadataMap.value.has(type)) {
          metadataMap.value.set(type, new Map());
        }
        const typeMap = metadataMap.value.get(type)!;

        if (!typeMap.has(objectId)) {
          typeMap.set(objectId, new Map());
        }
        const objectMap = typeMap.get(objectId)!;

        // Store metadata
        metadata.forEach((item) => {
          objectMap.set(item.id, {
            id: item.id,
            submission: item.submission,
            submissionMetadataObject: item.submission_metadata_object,
            field: {
              slug: item.field.slug,
              name: item.field.name,
              level: item.field.level,
              schema: item.field.schema,
            } as MetadataField,
            value: {
              value: item.value?.value,
              source: item.value?.source,
            } as ValueField,
            candidateValues: item.candidate_values
              .filter(candidate =>
                candidate?.value !== null
                && candidate?.value !== undefined
                && candidate?.value !== ""
                && (Array.isArray(candidate?.value) ? candidate.value.length > 0 : true),
              )
              .map(candidate => ({
                value: candidate?.value,
                source: candidate?.source,
              })),
            isValid: item.is_valid,
            createdBy: item.created_by,
            createdTime: item.created_time,
            lastEditedBy: item.last_edited_by,
            lastEditedTime: item.last_edited_time,
          } as Metadata);
        });
      } catch (error) {
        console.error("Error fetching metadata:", error);
      }
    }
  }

  async function fetchMetadataForType(type: string) {
    const objects = getMetadataObjectsByType(type).value;
    await Promise.all(
      objects.map(async (object) => {
        await fetchMetadataForObject(object.id);
      }),
    );
  }

  //////////////////////////////////////////////////////////////////////////////////////
  // Returns
  //////////////////////////////////////////////////////////////////////////////////////

  return {
    ////////////////////
    // states
    // -----------------
    submissionMap,
    currentSubid,
    updatedSubmissions,
    // -----------------
    // members
    memberMap,
    // -----------------
    // roles
    submissionRoles,
    // -----------------
    // metadata objects
    metadataObjectsMap,
    // -----------------
    // metadata
    metadataMap,

    ////////////////////
    // getter
    // -----------------
    // submissions
    currentSubmission,
    submissionCount,
    hasPendingModification,
    isLocked,
    getStatusBadge,
    // -----------------
    // members
    currentMember,
    // -----------------
    // Metadata objects
    getMetadataObjectsByType,
    getMetadataObjectStatus,
    // -----------------
    // metadata
    getMetadata,
    clearMetadata,
    ////////////////////
    // actions
    // -----------------
    // submissions
    addUpdatedSubmission,
    fetchSubmissions,
    fetchSubmission,
    fetchSubmissionSettings,
    updateSubmissionSettings,
    createSubmission,
    updateSubmission,
    deleteSubmission,
    saveAndRefresh,
    // -----------------
    // members
    fetchMembers,
    fetchSubmissionRoles,
    searchMember,
    updateMemberRole,
    createMember,
    deleteMember,
    // -----------------
    // submission objects
    fetchMetadataObjects,
    fetchMetadataForObject,
    fetchMetadataForType,
  };
});

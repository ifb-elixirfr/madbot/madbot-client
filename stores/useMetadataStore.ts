import { type Interactor, useNodeStore } from "@/stores/useNodeStore";
import { useWorkspaceStore } from "@/stores/useWorkspaceStore";
import jsonata from "jsonata";
import { defineStore } from "pinia";
// import { useSampleStore } from "@/stores/useSampleStore";
// import { useConnectionStore } from "@/stores/useConnectionStore";

interface MetadataMapping {
  metadata_field: MetadataField | null;
  connector: string | null;
}

interface MetadataField {
  id: string | null;
  slug: string | null;
  source: string | null;
  type: string | null;
  name: string | null;
  description: string | null;
  schema: string | null;
  metadataMapping: [MetadataMapping];
  relatedTypes: [];
  source: string | null;
}

interface MetadataValue {
  id: string;
  source: {
    object: string | null;
    id: string | null;
    name: string | null;
  };
  value: string | null;
  createdTime: string | null;
  createdBy: Interactor | null;
  lastEditedTime: string | null;
  lastEditedBy: Interactor | null;
}

interface Metadata {
  id: string;
  field: MetadataField;
  values: Map<string, MetadataValue>;
  isSelected: boolean;
  selected: MetadataValue | null;
  heritage: boolean;
}

export const useMetadataStore = defineStore("metadata", () => {
  const workspaceStore = useWorkspaceStore();
  const nodeStore = useNodeStore();
  // const sampleStore = useSampleStore();
  // const connectionStore = useConnectionStore();
  const unsavedMetadata = {};
  const metadataNodeSize = ref(0);

  //////////////////////////////////////////////////////////////////////////////////////
  // States
  //////////////////////////////////////////////////////////////////////////////////////

  const metadataMap = ref(new Map<string, Metadata>());
  const schemaMap = ref(new Map<string, string | null>());

  //////////////////////////////////////////////////////////////////////////////////////
  // Getters
  //////////////////////////////////////////////////////////////////////////////////////

  function getMetadataByID(metadataID: string) {
    for (const value of metadataMap.value.values()) {
      if (value.values.has(metadataID)) {
        return value;
      }
    }
  }

  function getMetadataByFieldID(metadataFieldID: string) {
    return metadataMap.value.get(metadataFieldID);
  }

  //////////////////////////////////////////////////////////////////////////////////////
  // Actions
  //////////////////////////////////////////////////////////////////////////////////////

  async function getMetadataSelected(item) {
    return await jsonata("values[selected=true]").evaluate(item);
  }

  /**
   * Retrieves the associated object based on the provided ID and, optionally, the type.
   * If the type is provided, it looks for the object in the corresponding store.
   * If the type is not provided, it tries to get the object from each store.
   * @param associatedObjectID - The ID of the associated object.
   * @param associatedObjectType - The type of the associated object.
   * @returns The associated object if found, otherwise undefined.
   */
  // async function getAssociatedObject(associatedObjectID: string, associatedObjectType?: string) {
  //   // if the type is provided, work from here to get the object
  //   if (associatedObjectType) {
  //     switch (associatedObjectType) {
  //       case "node":
  //         return nodeStore.fetchNode(associatedObjectID);
  //       case "sample":
  //         return sampleStore.fetchSample(associatedObjectID);
  //       case "connection":
  //         return connectionStore.fetchData(associatedObjectID);
  //     }
  //   } else {
  //     // if the type is not provided, try to get the object from each store
  //     let obj = nodeStore.fetchNode(associatedObjectID);
  //     if (obj) {
  //       return obj;
  //     }
  //     obj = sampleStore.fetchSample(associatedObjectID);
  //     if (obj) {
  //       return obj;
  //     }
  //     obj = connectionStore.fetchData(associatedObjectID);
  //     if (obj) {
  //       return obj;
  //     }
  //   }
  // }

  /**
   * Retrieves the type of the associated object based on the provided ID.
   * If will infer the type based on which store hte object will be found.
   * @param associatedObjectID - The ID of the associated object.
   * @returns The type of the associated object if found, otherwise undefined.
   */
  // async function getAssociatedObjectType(associatedObjectID: string) {
  //   let obj = nodeStore.fetchNode(associatedObjectID);
  //   if (obj) {
  //     return "node";
  //   }
  //   obj = sampleStore.fetchSample(associatedObjectID);
  //   if (obj) {
  //     return "sample";
  //   }
  //   obj = connectionStore.fetchData(associatedObjectID);
  //   if (obj) {
  //     return "data";
  //   }
  // }

  /**
   * Helper function to fetch schema data from a URL.
   * @param {string} schemaUrl - The URL of the schema to fetch.
   * @returns The schema data.
   */
  async function fetchSchemaData(schemaUrl: string) {
    try {
      const response = await fetch(schemaUrl, {
        headers: { "X-Workspace": workspaceStore.currentWid },
      });
      if (!response.ok) {
        throw new Error(`Error fetching schema data from ${schemaUrl}: ${response.statusText}`);
      }
      return await response.json();
    } catch (error) {
      console.error(`Error fetching schema data from ${schemaUrl}: `, error);
      return null;
    }
  }

  function convertJson(str) {
    try {
      const temp = JSON.parse(JSON.stringify(str));
      return temp;
    } catch {
      return str;
    }
  }

  /**
   * Fetches all metadata from the server and updates the metadata map.
   * @param {string} associatedObjectID - The ID of the associated object.
   * @param {string} associatedObjectType - The type of the associated object.
   * @param {string} [associatedDatalinkID] - The ID of the datalink of the associated object.
   * @param {string} [associatedDataAssociationID] - The ID of the data related to the association.
   * @param {string} [associatedNodeID] - The ID of the node of the associated object.
   * It is optional, especially since this information is only relevant when the associated
   * object is of type data.
   */
  async function fetchMetadata(
    associatedObjectID: string,
    associatedObjectType: string,
    associatedDatalinkID?: string,
    associatedDataAssociationID?: string,
    associatedNodeID?: string,
  ) {
    try {
      // Construct the URL based on the type of associated object
      let url = "";
      if (associatedObjectType === "node") {
        url = `/api/nodes/${associatedObjectID}/metadata?include_inherited=true`;
      } else if (associatedObjectType === "sample" && associatedNodeID) {
        url = `/api/nodes/${associatedNodeID}/samples/${associatedObjectID}/metadata?include_inherited=true`;
      } else if (associatedObjectType === "sample" && !associatedNodeID) {
        url = `/api/nodes/${nodeStore.currentNid}/samples/${associatedObjectID}/metadata?include_inherited=true`;
      } else if (associatedObjectType === "data") {
        url = `/api/data/${associatedObjectID}/metadata?datalink=${associatedDatalinkID}&include_inherited=true`;
      } else if (associatedObjectType === "data_association") { // if the associated object is a data association
        url = `/api/data/${associatedDataAssociationID}/associations/${associatedObjectID}/metadata?include_inherited=true`;
      }

      // Clear existing maps
      metadataMap.value.clear();

      // Fetch metadata from the server
      const allMetadata = await recFetch(url, {
        headers: { "X-Workspace": workspaceStore.currentWid },
      });

      // Prepare to fetch schema data only for schemas that haven't been fetched already
      const schemaPromises = allMetadata.map(async (metadata: any) => {
        const schemaUrl = metadata.field.schema;

        // Check if the schema already exists in schemaMap
        if (!schemaMap.value.has(schemaUrl)) {
          // If not, fetch the schema data
          const schemaData = await fetchSchemaData(schemaUrl);
          // Store the schema data in schemaMap
          schemaMap.value.set(schemaUrl, schemaData);
        }
      });

      // Wait for all schema fetch operations to complete
      await Promise.all(schemaPromises);

      // Process and store metadata
      for (const metadata of allMetadata) {
        const selected = await getMetadataSelected(convertJson(metadata));
        const metadataObj: Metadata = {
          id: metadata.field.id,
          field: {
            id: metadata.field.id,
            slug: metadata.field.slug,
            source: metadata.field.source,
            type: metadata.field.type,
            name: metadata.field.name,
            description: metadata.field.description,
            schema: metadata.field.schema,
            metadataMapping: metadata.field.metadata_mapping,
            relatedTypes: metadata.field.related_types,
          } as MetadataField,
          values: new Map(
            Object.values(convertJson(metadata.values) as MetadataValue[]).map(value => [
              value.id,
              value as MetadataValue,
            ]),
          ),
          isSelected: typeof selected?.id !== "undefined",
          // if a value is selected, put it
          // otherwise, check if all values are identical, and if so, pick the first one as default
          selected: selected?.id
            ? (selected as MetadataValue)
            : metadata.values.every((val, _, arr) => val.value === arr[0].value)
              ? metadata.values[0]
              : null,
          heritage:
            metadata.values.size > 1
            || (selected !== null && selected?.source.id !== associatedObjectID),
        };

        metadataMap.value.set(metadata.field.id, metadataObj);
      }

      if (associatedObjectType === "node") {
        metadataNodeSize.value = metadataMap.value.size;
      }
    } catch (error) {
      console.error("Error fetching metadata: ", error);
    }
  }

  /**
   * Fetches a single metadatum from the server.
   *
   * @param {string} metadataID - The ID of the metadatum to fetch.
   * @param {string} associatedObjectID - The ID of the associated object.
   * @param {string} associatedObjectType - The type of the associated object.
   * @param {string} [associatedDatalinkID] - The ID of the datalink of the associated object.
   * It is optional, especially since this information is only relevant when the associated
   * object is of type data.
   * @returns A Promise that resolves to the fetched metadatum.
   */
  async function fetchMetadatum(
    metadataID: string,
    associatedObjectID: string,
    associatedObjectType: string,
    associatedDatalinkID?: string,
    associatedDataAssociationID?: string,
  ) {
    const metadata = getMetadataByID(metadataID);
    if (metadata) {
      return metadata;
    } else {
      try {
        let url = "";
        if (associatedObjectType === "node") { // if the associated object is a node
          url = `/api/nodes/${associatedObjectID}/metadata/${metadataID}`;
        } else if (associatedObjectType === "sample") { // if the associated object is a sample
          url
            = `/api/nodes/${
              nodeStore.currentNid
            }/samples/${
              associatedObjectID
            }/metadata/${
              metadataID}`;
        } else if (associatedObjectType === "data") { // if the associated object is a data
          url
            = `/api/data/${
              associatedObjectID
            }/metadata/${
              metadataID
            }?datalink=${
              associatedDatalinkID}`;
        } else if (associatedObjectType === "data_association") { // if the associated object is a data association
          url
            = `/api/data/${
              associatedDataAssociationID
            }/associations/${
              associatedObjectID
            }/metadata/${
              metadataID}`;
        }

        const { data: metadata } = await useAPIFetch(url, {
          headers: { "X-Workspace": workspaceStore.currentWid },
        });
        return metadata.value;
      } catch (error) {
        console.error("Error fetching metadata:", error);
      }
    }
  }

  /**
   * Creates a metadata for an associated object.
   * @param metadataField - The metadata field.
   * @param metadataValue - The value of the metadata.
   * @param associatedObjectID - The ID of the associated object.
   * @param associatedObjectType - The type of the associated object.
   * @returns A promise that resolves with the created metadata.
   */
  async function createMetadata(
    metadataField: string,
    metadataFieldType: "madbot_field" | "connector_field",
    metadataValue: string,
    associatedObjectID: string,
    associatedObjectType: string,
    associatedDatalinkID?: string,
    associatedDataAssociationID?: string,
    isSelectedValue: boolean = false,
  ) {
    return new Promise((resolve, reject) => {
      try {
        let url = "";
        if (associatedObjectType === "node") { // if the associated object is a node
          url = `/api/nodes/${associatedObjectID}/metadata`;
        } else if (associatedObjectType === "sample") { // if the associated object is a sample
          url
            = `/api/nodes/${nodeStore.currentNid}/samples/${associatedObjectID}/metadata`;
        } else if (associatedObjectType === "data") { // if the associated object is a data
          url = `/api/data/${associatedObjectID}/metadata?datalink=${associatedDatalinkID}`;
        } else if (associatedObjectType === "data_association") { // if the associated object is a data association
          url = `/api/data/${associatedDataAssociationID}/associations/${associatedObjectID}/metadata`;
        }

        const body: { [key: string]: any } = {
          [`${metadataFieldType}`]: metadataField,
        };

        /* eslint-disable camelcase */
        if (isSelectedValue) {
          body.selected_value = metadataValue;
        } else {
          body.value = metadataValue;
        }
        /* eslint-enable */

        useAPIFetch(url, {
          method: "POST",
          headers: { "X-Workspace": workspaceStore.currentWid },
          body: body,
        }).then(async (res) => {
          const error = res.error.value;
          if (error) {
            reject(error);
          }
          resolve(res.data.value);
        });
      } catch (error) {
        console.error("Error creating metadata: ", error);
        reject(error);
      }
    });
  }

  /**
   * Updates the metadata value for the specified metadata ID and associated object.
   * If the associated object type is not provided, it will be retrieved from the store.
   *
   * @param {string} metadataID - The ID of the metadata.
   * @param {string} metadataValue - The new value for the metadata.
   * @param {string} associatedObjectID - The ID of the associated object.
   * @param {string} associatedObjectType - The type of the associated object.
   * @param {string} [associatedDatalinkID] - The ID of the datalink of the associated object.
   * It is optional, especially since this information is only relevant when the associated
   * object is of type data.
   * @returns A promise that resolves with the updated metadata value.
   */
  async function updateMetadata(
    metadataID: string,
    metadataValue: string,
    associatedObjectID: string,
    associatedObjectType: string,
    associatedDatalinkID?: string,
    associatedDataAssociationID?: string,
  ) {
    return new Promise((resolve, reject) => {
      try {
        let url = "";
        if (associatedObjectType === "node") { // if the associated object is a node
          url = `/api/nodes/${associatedObjectID}/metadata/${metadataID}`;
        } else if (associatedObjectType === "sample") { // if the associated object is a sample
          url
            = `/api/nodes/${
              nodeStore.currentNid
            }/samples/${
              associatedObjectID
            }/metadata/${
              metadataID}`;
        } else if (associatedObjectType === "data") { // if the associated object is a data
          url
            = `/api/data/${
              associatedObjectID
            }/metadata/${
              metadataID
            }?datalink=${
              associatedDatalinkID}`;
        } else if (associatedObjectType === "data_association") { // if the associated object is a data association
          url
            = `/api/data/${
              associatedDataAssociationID
            }/associations/${
              associatedObjectID
            }/metadata/${
              metadataID}`;
        }

        useAPIFetch(url, {
          method: "PUT",
          headers: { "X-Workspace": workspaceStore.currentWid },
          body: {
            value: metadataValue,
          },
        }).then(async (res) => {
          const error = res.error.value;
          if (error) {
            reject(error);
          }
          await fetchMetadata(associatedObjectID, associatedObjectType, associatedDatalinkID);
          resolve(res.data.value);
        });
      } catch (error) {
        console.error("Error updating metadata: ", error);
        reject(error);
      }
    });
  }

  /**
   * Deletes a metadata associated with an object.
   *
   * @param {string} metadataID - The ID of the metadata to delete.
   * @param {string} associatedObjectID - The ID of the associated object.
   * @param {string} associatedObjectType - The type of the associated object (node, sample, or data).
   * @param {string} [associatedDatalinkID] - The ID of the datalink of the associated object.
   * It is optional, especially since this information is only relevant when the associated
   * object is of type data.
   * @returns A promise that resolves with the deleted metadata.
   */
  async function deleteMetadata(
    metadataID: string,
    associatedObjectID: string,
    associatedObjectType: string,
    associatedDatalinkID?: string,
    associatedDataAssociationID?: string,
  ) {
    return new Promise((resolve, reject) => {
      try {
        let url = "";
        if (associatedObjectType === "node") { // if the associated object is a node
          url = `/api/nodes/${associatedObjectID}/metadata/${metadataID}`;
        } else if (associatedObjectType === "sample") { // if the associated object is a sample
          url
            = `/api/nodes/${
              nodeStore.currentNid
            }/samples/${
              associatedObjectID
            }/metadata/${
              metadataID}`;
        } else if (associatedObjectType === "data") { // if the associated object is a data
          url
            = `/api/data/${
              associatedObjectID
            }/metadata/${
              metadataID
            }?datalink=${
              associatedDatalinkID}`;
        } else if (associatedObjectType === "data_association") { // if the associated object is a data association
          url
            = `/api/data/${
              associatedDataAssociationID
            }/associations/${
              associatedObjectID
            }/metadata/${metadataID}`;
        }

        useAPIFetch(url, {
          method: "DELETE",
          headers: { "X-Workspace": workspaceStore.currentWid },
        }).then(async (res) => {
          const error = res.error.value;
          if (error) {
            reject(error);
          }
          await fetchMetadata(associatedObjectID, associatedObjectType, associatedDatalinkID);
          resolve(res.data.value);
        });
      } catch (error) {
        console.error("Error deleting metadata: ", error);
        reject(error);
      }
    });
  }

  return {
    ////////////////////
    // actions
    metadataMap,
    schemaMap,
    unsavedMetadata,
    metadataNodeSize,

    ////////////////////
    // getters
    // -----------------
    getMetadataByID,
    getMetadataByFieldID,

    ////////////////////
    // actions
    fetchMetadata,
    fetchMetadatum,
    createMetadata,
    updateMetadata,
    deleteMetadata,
  };
});

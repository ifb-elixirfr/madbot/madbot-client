import { useWorkspaceStore } from "@/stores/useWorkspaceStore";
import router from "next/router";
import { defineStore } from "pinia";

export const useWebsocketStore = defineStore("websocket", () => {
  const workspaceStore = useWorkspaceStore();
  const submissionStore = useSubmissionStore();
  //////////////////////////////////////////////////////////////////////////////////////
  // States
  //////////////////////////////////////////////////////////////////////////////////////

  const websockets = new Map();
  const message = ref(null);

  //////////////////////////////////////////////////////////////////////////////////////
  // actions
  //////////////////////////////////////////////////////////////////////////////////////

  // Function to initialize WebSocket connection
  async function initWebSocket(path) {
    if (!websockets.has(path)) {
      const { data: session } = useAuth();

      let location = window.location.host;
      if (location === "localhost:3000") {
        location = "localhost:8000";
      }

      if (session.value !== null) {
        document.cookie = `AuthorizationToken=${session?.value?.token}; path=/;`;
      }

      const wsProtocol = window.location.protocol === "https:" ? "wss:" : "ws:";

      const ws = new WebSocket(`${wsProtocol}//${location}/ws/${path}`);

      // Add message handler to WebSocket instance
      ws.onmessage = event => handleMessage(event, path);

      websockets.set(path, ws); // Store the WebSocket instance in the map
    }

    return websockets.get(path);
  }

  function handleMessage(event, path) {
    const websocketMessage = JSON.parse(event.data);

    const websocketChannel = path.split("/")[0];

    switch (websocketChannel) {
      case "notifications":
        if (message.value !== websocketMessage) {
          message.value = websocketMessage; // Update the message value
        }
        break;
      case "submissions": {
        const submissionId = websocketMessage.data.submission_id;
        if (submissionId === submissionStore.currentSubid) {
          if (websocketMessage.data.action === "update") {
            // refresh the submission
            submissionStore.fetchSubmission(submissionId);
            // refresh the metadata objects
            submissionStore.fetchMetadataObjects();
            // delete any metadata
            submissionStore.clearMetadata();
          } else if (websocketMessage.data.action === "delete") {
            submissionStore.deleteSubmission(submissionId);
            // redirect to submissions page
            router.push("/submissions");
          } else if (websocketMessage.data.action === "error") {
            // refresh the submission
            submissionStore.fetchSubmission(submissionId);
            // refresh the metadata objects
            submissionStore.fetchMetadataObjects();
            // delete any metadata
            submissionStore.clearMetadata();
          }
        }
        break;
      }
      case "workspace":
        // Handle workspace messages
        workspaceStore.fetchWorkspaces();
        break;

      default:
        console.warn(`Unknown websocket path: ${path}`);
    }
  }

  function closeWebSocket(path) {
    if (websockets.has(path)) {
      const ws = websockets.get(path);
      ws.close();
      websockets.delete(path);
    }
  }
  //////////////////////////////////////////////////////////////////////////////////////
  // GETTER
  //////////////////////////////////////////////////////////////////////////////////////

  function getMessage() {
    return message.value;
  }

  //////////////////////////////////////////////////////////////////////////////////////
  // Returns
  //////////////////////////////////////////////////////////////////////////////////////

  return {
    websockets,
    message,
    initWebSocket,
    getMessage,
    closeWebSocket,
  };
});

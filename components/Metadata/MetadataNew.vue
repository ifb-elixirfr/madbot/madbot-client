<script setup lang="ts">
// ----------------------------------------------------------------
// Import
// ----------------------------------------------------------------

import { useAPIFetch } from "@/composables/useAPIFetch";
import { useConnectionStore } from "@/stores/useConnectionStore";
import { useMetadataStore } from "@/stores/useMetadataStore";
import _ from "lodash";
import { onMounted, ref, watch } from "vue";

// ----------------------------------------------------------------
// PROPS
// ----------------------------------------------------------------

const props = defineProps({
  addMetadataDialog: {
    type: Boolean,
    required: true,
  },
  objectRelated: {
    type: Object,
    required: true,
  },
  dataAssociationID: {
    type: String,
    required: false,
  },
  metadataItems: {
    type: Object,
    required: true,
  },
});

// ----------------------------------------------------------------
// EMITS
// ----------------------------------------------------------------

const emits = defineEmits(["addMetadataDialog", "newMetadata"]);

// ----------------------------------------------------------------
// Variables
// ----------------------------------------------------------------
const connectionStore = useConnectionStore();
const metadataStore = useMetadataStore();
const search = ref<string>("");
const mdfCount = ref(0);
const connectors = ref<any[]>([]);
const searchResults = ref<any[]>([]);
const selectedMetadataFields = ref<any[]>([]);
const metadataDetails = ref<any | null>(null);
const drawer = ref<boolean>(false);
const loadingMDF = ref<boolean>(false);
const page = ref(1);
const totalPages = ref(0);
const currentPage = ref(0);

function escapeRegex(string) {
  // Échapper les caractères spéciaux pour éviter les erreurs de regex
  return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
}

function highlightedParts(text) {
  if (!search.value) return [{ text, isHighlighted: false }]; // Retour du texte non modifié si pas de recherche

  const regex = new RegExp(`(${escapeRegex(search.value)})`, "gi");
  const parts = text.split(regex); // Divise le texte en parties selon le terme recherché

  return parts.map(part => ({
    text: part,
    isHighlighted: regex.test(part), // Surligner si correspond à la recherche
  }));
}

// ----------------------------------------------------------------
// Watch
// ----------------------------------------------------------------

watch(
  () => props.addMetadataDialog,
  (newValue) => {
    if (newValue) {
      drawer.value = newValue;
    }
  },
);

watch(
  search,
  _.debounce(async () => {
    page.value = 1;
    await searchMetadata();
  }, 0),
);

// ----------------------------------------------------------------
// Lifecycle
// ----------------------------------------------------------------

onMounted(async () => {
  if (connectionStore.connectorMap.size === 0) {
    await connectionStore.fetchConnectors();
  }
  connectors.value = Object.values(Object.fromEntries(connectionStore.connectorMap));
});

// ----------------------------------------------------------------
// Functions
// ----------------------------------------------------------------

function close() {
  drawer.value = false;
  emits("addMetadataDialog", false);
  selectedMetadataFields.value = [];
  searchResults.value = [];
  metadataDetails.value = null;
  search.value = "";
  mdfCount.value = 0;
}

// search for metadata fields
async function searchMetadata() {
  if (search.value.length < 3) {
    searchResults.value = [];
    mdfCount.value = 0;
    return;
  }

  if (loadingMDF.value) {
    return;
  }

  try {
    loadingMDF.value = true;
    const { data: results } = await useAPIFetch(
      `/api/metadata_fields?search=${search.value}&p=${page.value}`,
    );
    if (page.value === 1) {
      searchResults.value = results.value.results;
    } else {
      searchResults.value = [...searchResults.value, ...results.value.results];
    }
    currentPage.value = results.value.current_page;
    totalPages.value = results.value.total_pages;
    mdfCount.value = results.value.count;
  } catch (error) {
    console.error("Error searching metadata:", error);
  } finally {
    loadingMDF.value = false;
  }
}

// Add a metadata field to the selection list
function addMetadataField(field: any) {
  if (!selectedMetadataFields.value.find(item => item.id === field.id)) {
    selectedMetadataFields.value.push(field);
  }
}

// Check if field is already selected
function isSelected(field: any) {
  return selectedMetadataFields.value.some(item => item.id === field.id);
}

// Check if field id already exist in Object
function alreadyExist(id: any) {
  return props.metadataItems.some(metadataItem => metadataItem.id === id);
}

// Remove a metadata field from the selection list
function removeMetadataField(field: any) {
  selectedMetadataFields.value = selectedMetadataFields.value.filter(item => item !== field);
}

// Function to clear the list of selected metadata fields
function clearSelectedMetadataFields() {
  selectedMetadataFields.value = [];
}

// View details of a metadata field
function viewMetadataDetails(field: any) {
  metadataDetails.value = field;
}

// Add the selected metadata fields
async function addSelectedMetadataFields() {
  for (const metadataField of selectedMetadataFields.value) {
    let schema = metadataField.schema;
    const response = await fetch(schema);
    schema = await response.json();
    let value;
    if ("items" in schema) {
      value = [];
    } else if ("type" in schema && schema.type === "object") {
      value = {};
    } else if ("type" in schema && schema.type === "boolean") {
      value = false;
    } else {
      value = "";
    }

    await metadataStore.createMetadata(
      metadataField.id,
      metadataField.type,
      value,
      props.objectRelated.id,
      props.objectRelated.type,
      props.objectRelated.datalinkID || undefined,
      props.dataAssociationID || undefined,
    );
  }
  emits("newMetadata", selectedMetadataFields.value);
  close();
}

// Use regex to extract the name before "Connector"
function extractConnectorName(connector: string): string {
  const match = connector.match(/(\w+)Connector/i);
  if (match && match.length > 1) {
    return match[1];
  }
  return connector;
}

// Group metadata mappings by connector name.
function groupedMappings(item: any): Record<string, string[]> {
  const grouped: Record<string, string[]> = {};

  item.metadata_mapping.forEach((mapping) => {
    let connectorName = extractConnectorName(mapping.connector);

    if (item.type === "connector_field") {
      connectorName = "Madbot";
      if (!grouped[connectorName]) {
        grouped[connectorName] = [];
      }
      grouped[connectorName].push(mapping.madbot_metadata_field.name);
    } else if (item.type === "madbot_field") {
      if (!grouped[connectorName]) {
        grouped[connectorName] = [];
      }
      grouped[connectorName].push(mapping.connector_metadata_field.name);
    }
  });

  return grouped;
}

// Infinite Scroll Load Function
async function load({ done }) {
  if (loadingMDF.value || search.value.length < 3) {
    return;
  }
  if (currentPage.value < totalPages.value) {
    page.value += 1;
    await searchMetadata();
    done("ok");
  } else {
    done("empty");
  }
}
</script>

<template>
  <v-dialog
    v-model="drawer"
    width="100%"
    height="100%"
    style="max-width: 1250px !important; max-height: 800px !important"
    persistent
  >
    <v-card>
      <v-card-title class="d-flex">
        <v-sheet
          class="me-auto"
          style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis"
        >
          Add metadata
        </v-sheet>
        <v-sheet>
          <v-btn
            icon="$close"
            density="comfortable"
            variant="plain"
            @click="close"
          />
        </v-sheet>
      </v-card-title>

      <v-row>
        <v-col class="d-flex flex-column fill-height" style="max-width: 100%">
          <v-card-text>
            <v-text-field
              v-model="search"
              label="Search..."
              clearable
              append-inner-icon="mdi-magnify"
            />
            <p class="mb-1">
              Metadata fields found: {{ mdfCount }}
            </p>
            <div style="min-height: 380px; overflow-y: auto; max-height: 380px">
              <template v-if="searchResults.length > 0">
                <v-infinite-scroll :items="searchResults" :onLoad="load">
                  <template v-for="(item, _index) in searchResults" :key="item.id">
                    <div
                      :class="{
                        'bg-grey-lighten-3': !item.related_types.includes(objectRelated.type),
                        'bg-white': item.related_types.includes(objectRelated.type),
                        'border-s-lg border-primary':
                          metadataDetails && item.id === metadataDetails.id,
                      }"
                      style="cursor: pointer"
                      @click="viewMetadataDetails(item)"
                    >
                      <v-row class="pt-3 pr-5 ml-auto">
                        <img
                          v-if="item.source.toLowerCase() === 'referential'"
                          class="align-self-center me-4"
                          src="/img/logo.png"
                          alt=""
                          style="max-width: 60px; max-height: 40px"
                        >
                        <img
                          v-else-if="connectors.find(connector => connector.name.toLowerCase() === item.source.toLowerCase())"
                          class="align-self-center me-4"
                          :src="connectors.find(connector => connector.name.toLowerCase() === item.source.toLowerCase()).icon"
                          alt=""
                          style="max-width: 60px; max-height: 40px"
                        >
                        <v-col>
                          <b :id="_index.toString()">
                            <template v-for="(part, subIndex) in highlightedParts(item.name)" :key="subIndex">
                              <span :style="part.isHighlighted ? 'color: blue;' : ''">{{ part.text }}</span>
                            </template>
                          </b>
                          <div>
                            <template v-for="(part, subIndex) in highlightedParts(item.description)" :key="subIndex">
                              <span :style="part.isHighlighted ? 'color: blue;' : ''">{{ part.text }}</span>
                            </template>
                          </div>
                          <div v-if="item.metadata_mapping.length > 0" class="text-caption">
                            <v-list style="background-color: inherit">
                              <v-list-item
                                v-for="(groupedMapping, connector) in groupedMappings(
                                  item,
                                )"
                                :key="connector"
                              >
                                <v-chip small class="text-caption">
                                  {{ connector }}
                                </v-chip>
                                Mapped with
                                <template v-for="(fieldName, index) in groupedMapping" :key="index">
                                  <template v-for="(part, subIndex) in highlightedParts(fieldName)" :key="subIndex">
                                    <span :style="part.isHighlighted ? 'color: blue;' : ''">{{ part.text }}</span>
                                  </template>
                                  <span v-if="index !== groupedMapping.length - 1">, </span>
                                </template>
                                <span v-if="item.type === 'connector_field'">
                                  from the Madbot referential. We recommend using the field from the referential.
                                </span>
                              </v-list-item>
                            </v-list>
                          </div>
                        </v-col>
                        <v-col cols="auto" class="d-flex align-center">
                          <v-tooltip
                            end
                            :disabled="item.related_types.includes(objectRelated.type)"
                          >
                            <template v-slot:activator="{ props: basketProps }">
                              <span v-bind="basketProps">
                                <v-btn
                                  icon="mdi-basket-plus"
                                  :disabled="
                                    !item.related_types.includes(objectRelated.type)
                                      || isSelected(item)
                                      || alreadyExist(item.id)
                                  "
                                  density="comfortable"
                                  @click.stop="addMetadataField(item)"
                                />
                              </span>
                            </template>
                            <span>
                              The following metadata cannot be associated in the object where you
                              are located ( {{ objectRelated.type }} ) and is therefore not
                              selected.<br>
                              {{ item.name }} can be defined only in
                              <span v-for="(type, index) in item.related_types" :key="index">
                                {{ type }}
                                <template v-if="index === item.related_types.length - 2">
                                  or
                                </template>
                              </span>
                            </span>
                          </v-tooltip>
                        </v-col>
                      </v-row>
                      <v-divider />
                    </div>
                  </template>
                </v-infinite-scroll>
              </template>
            </div>
            <div class="mt-2">
              <v-sheet class="d-flex align-center">
                <div class="w-100">
                  <v-card
                    v-bind="props"
                    :elevation="6"
                    class="mb-6 mx-1 pa-3 ml-2"
                    height="110"
                    max-height="110"
                    max-width="815"
                  >
                    <div class="d-flex justify-space-between align-center">
                      <h4>Selected metadata fields</h4>
                      <v-btn
                        v-if="selectedMetadataFields.length > 0"
                        size="x-small"
                        @click="clearSelectedMetadataFields"
                      >
                        Clear
                      </v-btn>
                    </div>
                    <v-container>
                      <v-row>
                        <v-col>
                          <v-chip-group>
                            <v-chip
                              v-for="field in selectedMetadataFields"
                              :key="field.name"
                              closable
                              @click:close="removeMetadataField(field)"
                            >
                              {{ field.name }}
                            </v-chip>
                          </v-chip-group>
                        </v-col>
                      </v-row>
                    </v-container>
                  </v-card>
                </div>
              </v-sheet>
            </div>
          </v-card-text>

          <v-btn
            class="ml-auto add-button"
            @click="addSelectedMetadataFields"
          >
            Add to metadata
            <span v-if="selectedMetadataFields.length > 0">
              ({{ selectedMetadataFields.length }})</span>
          </v-btn>
        </v-col>

        <v-divider class="mx-2 mb-6" vertical />
        <v-col
          cols="3"
          class="d-flex flex-column fill-height"
          style="max-width: 100%"
        >
          <v-sheet class="d-flex" height="70">
            <div v-if="metadataDetails">
              <v-row class="d-flex align-center">
                <v-card-title class="flex-grow-1">
                  {{ metadataDetails.name }}
                </v-card-title>
                <v-tooltip
                  end
                  :disabled="metadataDetails.related_types.includes(objectRelated.type)"
                >
                  <template v-slot:activator="{ props: basketAllProps }">
                    <span v-bind="basketAllProps">
                      <v-btn
                        variant="text"
                        class="clickable-icon pa-0 rounded-circle ml-auto mr-7"
                        min-width="36"
                        min-height="36"
                        icon="mdi-basket-plus"
                        :disabled="
                          !metadataDetails.related_types.includes(objectRelated.type)
                            || isSelected(metadataDetails)
                            || alreadyExist(metadataDetails.id)
                        "
                        density="comfortable"
                        @click.stop="addMetadataField(metadataDetails)"
                      />
                    </span>
                  </template>
                  <span>The following metadata cannot be associated in the object where you<br>
                    are located ( {{ objectRelated.type }} ) and is therefore not selected.<br>
                    {{ metadataDetails.name }} can be defined only in
                    <span v-for="(type, index) in metadataDetails.related_types" :key="index">
                      {{ type }}
                      <template v-if="index === metadataDetails.related_types.length - 2">
                        or
                      </template></span></span>
                </v-tooltip>
              </v-row>
              <v-card-text>
                <p>{{ metadataDetails.description }}</p>
                <div v-if="metadataDetails.metadata_mapping.length > 0">
                  <h5>Mapped with</h5>
                  <div>
                    <v-chip-group>
                      <v-chip
                        v-for="(groupedMapping, connector) in groupedMappings(
                          metadataDetails,
                        )"
                        :key="connector"
                        class="mr-1"
                      >
                        {{ extractConnectorName(connector) }}
                      </v-chip>
                    </v-chip-group>
                  </div>
                </div>
                <h5>Associated with</h5>
                <div>
                  <v-chip-group>
                    <v-chip
                      v-for="(relatedType, index) in metadataDetails.related_types"
                      :key="index"
                      class="mr-1"
                    >
                      {{ relatedType }}
                    </v-chip>
                  </v-chip-group>
                </div>
              </v-card-text>
            </div>
          </v-sheet>
        </v-col>
      </v-row>
    </v-card>
  </v-dialog>
</template>

<style>
.add-button {
  margin-top: -20px;
  margin-bottom: 20px;
}
.v-dialog .v-card {
  height: 100%;
  overflow: hidden !important;
}
</style>

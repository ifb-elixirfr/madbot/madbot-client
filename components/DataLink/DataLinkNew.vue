<script setup lang="ts">
// ----------------------------------------------------------------
// Import
// ----------------------------------------------------------------

import {
  type Connection,
  type Connector,
  useConnectionStore,
} from "@/stores/useConnectionStore";
import { useWorkspaceStore } from "@/stores/useWorkspaceStore";

// ----------------------------------------------------------------
// PROPS
// ----------------------------------------------------------------

const props = defineProps({
  newDatalinkDialog: {
    type: Boolean,
    required: true,
  },
});

// ----------------------------------------------------------------
// Emits
// ----------------------------------------------------------------

const emits = defineEmits(["newDatalinkDialog", "newDataLinkCreated"]);

// ----------------------------------------------------------------
// Variables
// ----------------------------------------------------------------

const workspaceStore = useWorkspaceStore();
const connectionStore = useConnectionStore();
const route = useRoute();

const drawer = ref(false);

const stepperItems = ["Choose a connector", "Connect", "Select a data"];
const step = ref(1);
const steps = 3;

const connectors = ref<Array<Connector>>([]);
const connections = ref<Array<Connection>>([]);
const connectionsEmpty = ref(false);
const currentPageConnection = ref(0);
const loadingConnection = ref(false);

const selectedConnector = ref<any>(null);
const expandedConnector = ref("");
const selectedConnection = ref<any>(null);

const sharedFields = ref({});
const sharedFieldSchemas = ref({});
const prefilledSharedFields = ref(false);
const privateFields = ref({});
const privateFieldSchemas = ref({});

const validationErrors = ref([]);
const invalidCount = ref<number>(0);

const externalData = ref([]);
const externalDataHasNoChildren = ref(0);
const loadingData = ref(false);

const rootValue = ref("");
const path = ref<Array<object>>([]);

const loadingCreateDatalink = ref(false);
const currentDataObject = ref(null);
const currentDataObjectParent = ref(null);

const errorMessage = ref(null);

const selectedDataObject = ref(new Map());
const selectAllBtnText = ref("Select all linkable data");
const selectAllBtnIcon = ref("mdi-checkbox-blank-outline");

// Define the Snackbar properties
const errorMessageSnackbar = reactive({
  show: false,
  message: "",
});

// ----------------------------------------------------------------
// Watch
// ----------------------------------------------------------------

watch(
  () => props.newDatalinkDialog,
  async (newValue) => {
    if (newValue) {
      resetPath();
      drawer.value = newValue;
    }
  },
);

// ----------------------------------------------------------------
// Lifecycle
// ----------------------------------------------------------------

onMounted(async () => {
  if (connectionStore.connectorMap.size === 0) {
    await connectionStore.fetchConnectors();
  }
  connectors.value = Object.values(
    Object.fromEntries(connectionStore.connectorMap),
  ).filter(connector => connector.types?.includes("data_connector"));
});

// ----------------------------------------------------------------
// Functions
// ----------------------------------------------------------------

/**
 * Closes the dialog, resets all relevant states and fields and emits the
 * "newDatalinkDialog" event to notify that the dialog has been closed.
 */
function close() {
  drawer.value = false;
  clearStates();
  emits("newDatalinkDialog", false);
}

/**
 * Resets all the states and fields to their initial values.
 *
 * This function is used to clear the state when closing the dialog or resetting the form.
 * It resets the stepper to the first step, clears the connections array and flags,
 * resets the selected connector and connection ID, clears shared and private fields,
 * resets the path, clears external data and loading flags, and resets error messages.
 */
function clearStates() {
  step.value = 1;

  connections.value = [];
  connectionsEmpty.value = false;
  currentPageConnection.value = 0;
  loadingConnection.value = false;

  selectedConnector.value = null;
  expandedConnector.value = "";
  selectedConnection.value = null;

  sharedFields.value = {};
  prefilledSharedFields.value = false;
  privateFields.value = {};
  sharedFieldSchemas.value = {};
  privateFieldSchemas.value = {};

  validationErrors.value = [];
  invalidCount.value = 0;

  resetPath();

  externalData.value = [];
  externalDataHasNoChildren.value = 0;
  loadingData.value = false;

  loadingCreateDatalink.value = false;
  currentDataObject.value = null;
  currentDataObjectParent.value = null;

  errorMessage.value = null;
  selectedDataObject.value.clear();
}

/**
 * Loads connections when the drawer is open and updates the state based on the fetched connections.
 *
 * If the modal is open and connections are not marked as empty, it fetches connections.
 * If connections are fetched successfully and there are still more connections that can be fetched, it calls the `done` callback with "ok".
 * Otherwise, it marks connections as empty and calls the `done` callback with "empty".
 *
 * @param {object} params - The parameters object.
 * @param {Function} params.done - The callback function to be called with the status of the operation.
 */
async function loadConnections({ done }) {
  if (drawer.value) {
    if (!connectionsEmpty.value) {
      currentPageConnection.value = currentPageConnection.value + 1;
      const { data: resConnections } = await useAPIFetch(
        `/api/connections?p=${currentPageConnection.value}&connector_type=data_connector`,
        { headers: { "X-Workspace": workspaceStore.currentWid } },
      );

      resConnections.value?.results.forEach((connection) => {
        connections.value.push(connectionStore.asConnection(connection));
      });

      if (
        resConnections.value?.next !== null
        && resConnections.value?.next !== undefined
      ) {
        done("ok");
      } else {
        connectionsEmpty.value = true;
        done("empty");
      }
    } else {
      done("empty");
    }
  }
}

function expandConnector(connector) {
  if (expandedConnector.value === connector.id) {
    expandedConnector.value = "";
  } else {
    expandedConnector.value = connector.id;
  }
}

function selectConnector(connector) {
  selectedConnector.value = connector;
  step.value = 2;
}

/**
 * Establishes a connection by either updating an existing connection or creating a new one.
 */
async function connect() {
  loadingConnection.value = true;
  try {
    // Check if a connection already exists
    if (selectedConnection.value?.id) {
      // If a connection exists, update it
      await updateConnection();
    } else {
      // If no connection exists, create a new one
      await createConnection();
    }
  } catch (error) {
    loadingConnection.value = false;
    throw error;
  }
  loadingConnection.value = false;
  step.value = 3;
  await getExternalData(selectedConnection.value.id);
}

/**
 * Updates an existing connection with new private fields
 * by calling the `updateConnection` method from the `connectionStore`.
 */
async function updateConnection() {
  const privateParams = Object.assign(
    {},
    ...Object.values(privateFields.value),
  );
  await connectionStore.updateConnection(
    selectedConnection.value.id,
    privateParams,
  );
}

/**
 * Creates a new connection using the selected connector and the provided shared and private parameters
 * by calling the `createConnection` method from the `connectionStore`.
 */
async function createConnection() {
  await connectionStore
    .createConnection(
      selectedConnector.value.id,
      Object.assign({}, ...Object.values(sharedFields.value)),
      Object.assign({}, ...Object.values(privateFields.value)),
    )
    .then((connection) => {
      selectedConnection.value = connection;
      connections.value.push(connection);
      rootValue.value = `${connection.root?.name}/`;
      path.value = [{ name: rootValue.value }];
    })
    .catch((error) => {
      throw error;
    });
}

async function connectFromConnection(connectionId) {
  let connectionIndex = -1; // Declare connectionIndex outside the try block

  try {
    // Find the index of the connection being loaded
    connectionIndex = connections.value.findIndex(
      connection => connection.id === connectionId,
    );

    if (connectionIndex !== -1) {
      // Retrieve the connection object at the found index
      const connection = connections.value[connectionIndex];
      selectedConnection.value = connection;
      selectedConnector.value = connection.connector;

      // Set loading state for the specific connection
      connections.value[connectionIndex].loading = true;

      if (connection.status === "connected") {
        try {
          // Call the checkConnection method from the connection store
          await connectionStore.checkConnection(connectionId);

          // Once checked, fetch the updated connection details
          await connectionStore.fetchConnection(selectedConnection.value.id);
          rootValue.value = `${connection.root?.name}/`;
          path.value = [{ name: rootValue.value }];
          step.value = 3;

          await getExternalData(selectedConnection.value.id);
        } catch (error) {
          prefilledSharedFields.value = true;
          await connectionStore.fetchConnection(selectedConnection.value.id);
          rootValue.value = `${connection.root?.name}/`;
          path.value = [{ name: rootValue.value }];
          step.value = 2;
          loadingConnection.value = false;
          throw error;
        }
      } else {
        prefilledSharedFields.value = true;
        await connectionStore.fetchConnection(selectedConnection.value.id);
        rootValue.value = `${connection.root?.name}/`;
        path.value = [{ name: rootValue.value }];
        step.value = 2;
        loadingConnection.value = false;
      }
    }
  } catch (error) {
    errorMessage.value = error.value?.data;
    loadingData.value = false;
  } finally {
    // Reset loading state for the specific connection
    if (connectionIndex !== -1) {
      connections.value[connectionIndex].loading = false;
    }
  }
}

async function browseDataObject(object) {
  // Check if the selected object has children
  if (object.has_children) {
    currentDataObject.value = null;
    currentDataObjectParent.value = object;

    try {
      // Fetch external data for the selected object
      await getExternalData(selectedConnection.value.id, object.id);
      // If the parent object exists and has a name, append its name to the path
      if (currentDataObjectParent.value && currentDataObjectParent.value.name) {
        path.value.push({
          name: `${currentDataObjectParent.value.name}/`,
          id: currentDataObjectParent.value.id,
        });
      }
    } catch (error) {
      // Handle errors thrown by getExternalData
      errorMessageSnackbar.message
        = error.value.response._data.details[0].message;
      errorMessageSnackbar.show = true;
    }
  } else {
    // If the selected object does not have children, select it without updating the path
    selectDataObject(object);
  }
}

async function getExternalData(connectionID, parentId = null) {
  loadingData.value = true;
  let url = `/api/connections/${connectionID}/external_data`;
  if (parentId !== null) {
    url += `?parent=${parentId}`;
  }

  externalData.value = await recFetch(url, {
    headers: { "X-Workspace": workspaceStore.currentWid },
    /* eslint-disable  camelcase */
    query: { page_size: 500 },
    /* eslint-enable */
  });
  externalDataHasNoChildren.value = externalData.value.filter(obj => !obj.has_children).length;
  updateSelectBtn();
  loadingData.value = false;
}

function selectDataObject(object) {
  currentDataObject.value = object;
}

function updateSelectBtn() {
  const contMatch = externalData.value.filter(obj => !obj.has_children && selectedDataObject.value.has(obj.id)).length;
  if (contMatch === 0) {
    selectAllBtnIcon.value = "mdi-checkbox-blank-outline";
    selectAllBtnText.value = "Select all linkable data";
  } else if (contMatch === externalDataHasNoChildren.value) {
    selectAllBtnIcon.value = "mdi-checkbox-outline";
    selectAllBtnText.value = "Unselect all linkable data";
  } else {
    selectAllBtnIcon.value = "mdi-minus-box-outline";
    selectAllBtnText.value = "Select all linkable data";
  }
}

function selectAllBtnAction() {
  if (selectAllBtnText.value === "Select all linkable data") {
    externalData.value.forEach((obj) => {
      if (!obj.has_children) {
        selectedDataObject.value.set(obj.id, obj);
      }
    });
  } else {
    externalData.value.forEach((obj) => {
      if (!obj.has_children) {
        selectedDataObject.value.delete(obj.id);
      }
    });
  }
  updateSelectBtn();
}

function checkDataObject(event, object) {
  if (event === false) {
    selectedDataObject.value.delete(object.id);
  } else {
    selectedDataObject.value.set(object.id, object);
  }
  updateSelectBtn();
}

function removeSelectedDataObject(dataObject) {
  selectedDataObject.value.delete(dataObject.id);
  updateSelectBtn();
}

function clearSelectedDataObject() {
  selectedDataObject.value.clear();
  updateSelectBtn();
}

async function createData() {
  for (const DataObject of selectedDataObject.value.values()) {
    if (
      DataObject !== null
      && DataObject.has_children === false
    ) {
      loadingCreateDatalink.value = true;

      try {
        await connectionStore
          .createData(selectedConnection.value.id, DataObject.id)
          .then(async (DataID) => {
            if (DataID) {
              await connectionStore
                .createDatalink(route.params.nid, DataID)
                .then((created) => {
                  if (created === true) {
                    // drawer.value = false;
                    // loadingCreateDatalink.value = false;
                    // emits("newDataLinkCreated");
                    // close();
                  }
                })
                .catch((error) => {
                  throw error;
                });
            }
          })
          .catch((error) => {
            throw error;
          });
      } catch (error) {
        loadingConnection.value = false;
        throw error;
      }
    }
  }
  drawer.value = false;
  loadingCreateDatalink.value = false;
  emits("newDataLinkCreated");
  close();
}

// Add a method to reset the path
function resetPath() {
  path.value = [];
  rootValue.value = "";
}

/**
 * Navigates to a specific item in the path and fetches the external data for that item.
 *
 * This function slices the path up to the specified index (inclusive) and updates the `path` state.
 * If the specified index is 0, it calls `getExternalData` to fetch the root data.
 * Otherwise, it calls `getExternalData` with the ID of the item at the specified index in the path as the parent.
 *
 * @param {number} index - The index of the item in the path to navigate to.
 */
async function goToPathItem(index) {
  path.value = [...path.value.slice(0, index + 1)];

  if (index === 0) {
    // If the clicked item is at index 0, call getExternalData for root
    await getExternalData(selectedConnection.value.id);
  } else {
    await getExternalData(selectedConnection.value.id, path.value[index].id);
  }
}
</script>

<template>
  <v-dialog
    v-model="drawer"
    persistent
    width="100%"
    height="850px"
    style="max-width: 1150px !important"
  >
    <v-card height="100%">
      <v-card-title class="d-flex">
        <v-sheet
          class="me-auto"
          style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis"
        >
          <b>{{ $t("add_dl") }}</b>
        </v-sheet>
        <v-sheet>
          <v-btn
            class="ms-2"
            icon="$close"
            density="comfortable"
            variant="plain"
            @click="close"
          />
        </v-sheet>
      </v-card-title>
      <v-card-text class="pt-0">
        <v-stepper
          v-model="step"
          elevation="0"
          height="100%"
          hide-actions
        >
          <template v-slot:default>
            <v-stepper-header>
              <template v-for="n in steps" :key="`${n}-step`">
                <v-stepper-item
                  class="py-4"
                  :complete="step > n"
                  step="Step {{ n }}"
                  :title="stepperItems[n - 1]"
                  :value="n"
                />

                <v-divider v-if="n !== steps" :key="n" />
              </template>
            </v-stepper-header>

            <v-stepper-window class="mt-1">
              <v-stepper-window-item
                v-for="n in steps"
                :key="`${n}-content`"
                :value="n"
              >
                <v-card
                  height="730px"
                  style="max-height: calc(100vh - 200px) !important"
                >
                  <template v-if="step === 1">
                    <v-row class="fill-height">
                      <v-col style="width: 49%" class="fill-height">
                        <h3>{{ $t("choose_existing_co") }}</h3>
                        <div class="overflow-y-auto fill-height">
                          <v-infinite-scroll
                            :items="connections"
                            :onLoad="loadConnections"
                          >
                            <template
                              v-for="(connection, index) in connections"
                              :key="index"
                            >
                              <v-card
                                class="ma-1 pa-2"
                                :loading="connection.loading"
                              >
                                <div
                                  class="d-flex align-center"
                                  style="cursor: pointer"
                                  @click="connectFromConnection(connection.id)"
                                >
                                  <v-badge
                                    :color="
                                      connectionStore.getStatus(
                                        connection.id,
                                        connection.status,
                                      ).color
                                    "
                                    inline
                                    class="me-2"
                                  />

                                  <img
                                    class="align-self-center me-4"
                                    :src="connection.connector.icon"
                                    alt=""
                                    style="max-width: 60px; max-height: 40px"
                                  >

                                  <div>
                                    <p>
                                      <b>{{ connection.connector.name }}</b>
                                    </p>
                                    <p>{{ connection.name }}</p>
                                  </div>
                                </div>
                              </v-card>
                            </template>
                            <template v-slot:empty>
                              <i> No more items</i>
                            </template>
                          </v-infinite-scroll>
                        </div>
                      </v-col>

                      <v-col
                        style="max-width: 50px"
                        class="d-flex flex-column align-center text-center"
                      >
                        <div class="vertical-line" />

                        <div class="center-content">
                          or
                        </div>

                        <div class="vertical-line" />
                      </v-col>

                      <v-col class="fill-height" style="width: 49%">
                        <h3>{{ $t("create_a_connection") }}</h3>
                        <v-skeleton-loader
                          v-if="connectors.length === 0"
                          type="card"
                        />
                        <div v-else class="mt-3">
                          <v-row>
                            <v-col
                              v-for="(connector, index) in connectors"
                              :key="index"
                              :cols="
                                expandedConnector === connector.id ? 12 : 6
                              "
                            >
                              <v-card
                                class="pl-3 pb-1"
                                link
                                :ripple="false"
                              >
                                <div
                                  class="d-flex justify-space-between align-start"
                                >
                                  <v-sheet
                                    class="d-flex flex-grow-1"
                                    height="70"
                                    @click="selectConnector(connector)"
                                  >
                                    <img
                                      class="align-self-center me-4"
                                      :src="connector.icon"
                                      alt=""
                                      style="max-width: 60px; max-height: 40px"
                                    >
                                    <p class="align-self-center">
                                      {{
                                        expandedConnector === connector.id
                                          ? connector.name
                                          : truncateTitle(connector.name, 8)
                                      }}
                                    </p>
                                  </v-sheet>
                                  <v-card-actions>
                                    <v-btn
                                      :icon="
                                        expandedConnector === connector.id
                                          ? 'mdi-arrow-collapse'
                                          : 'mdi-arrow-expand'
                                      "
                                      variant="plain"
                                      :ripple="false"
                                      size="small"
                                      @click="expandConnector(connector)"
                                    />
                                  </v-card-actions>
                                </div>
                                <v-expand-transition>
                                  <div
                                    v-if="expandedConnector === connector.id"
                                    class="text-body-2"
                                    @click="selectConnector(connector)"
                                  >
                                    <em>{{ connector.description }}</em>
                                  </div>
                                </v-expand-transition>
                              </v-card>
                            </v-col>
                          </v-row>
                        </div>
                      </v-col>
                    </v-row>
                  </template>
                  <template v-if="step === 2">
                    <v-row class="fill-height">
                      <v-col class="fill-height d-flex flex-column">
                        <div class="mb-auto">
                          <h3>
                            {{ $t("complete_form_to_connect") }}
                            {{ selectedConnector.name }}
                          </h3>
                          <br>
                          <ConnectionFields
                            v-model:currentConnector="selectedConnector"
                            v-model:currentConnection="selectedConnection"
                            v-model:sharedFields="sharedFields"
                            v-model:privateFields="privateFields"
                            v-model:sharedFieldSchemas="sharedFieldSchemas"
                            v-model:privateFieldSchemas="privateFieldSchemas"
                            v-model:readOnlySharedFields="
                              prefilledSharedFields
                            "
                            v-model:validationErrors="validationErrors"
                            v-model:invalidCount="invalidCount"
                          />
                        </div>
                        <v-sheet class="d-flex justify-end">
                          <v-btn
                            class="ma-2"
                            width="150"
                            @click="clearStates"
                          >
                            Back
                          </v-btn>

                          <v-tooltip bottom :disabled="invalidCount === 0">
                            <template v-slot:activator="{ props: StepProps }">
                              <div v-bind="StepProps" class="d-inline-block">
                                <v-btn
                                  :loading="loadingConnection"
                                  :disabled="invalidCount > 0"
                                  class="ma-2"
                                  width="150"
                                  @click="connect()"
                                >
                                  Connect
                                </v-btn>
                              </div>
                            </template>
                            <span>Shared and Private parameters are required.</span>
                          </v-tooltip>
                        </v-sheet>
                      </v-col>

                      <v-divider class="mx-4" vertical />

                      <v-col cols="3" class="d-flex flex-column fill-height">
                        <v-sheet class="d-flex" height="70">
                          <img
                            class="align-self-center me-4"
                            :src="selectedConnector.icon"
                            alt=""
                            style="height: 30px"
                          >
                          <p class="align-self-center">
                            {{ selectedConnector.name }}
                          </p>
                        </v-sheet>
                        <p>{{ selectedConnector.description }}</p>
                      </v-col>
                    </v-row>
                  </template>
                  <template v-if="step === 3">
                    <v-row class="fill-height">
                      <v-col class="fill-height d-flex flex-column">
                        <div class="d-flex flex-row align-center">
                          <template v-for="(item, index) in path" :key="index">
                            <v-btn
                              color="black"
                              variant="text"
                              rounded="0"
                              density="compact"
                              style="
                                text-transform: none;
                                padding: 4px 3px;
                                min-width: 0;
                              "
                              :disabled="index === path.length - 1"
                              class="custom-disabled-button"
                              @click.prevent="goToPathItem(index)"
                            >
                              <h4>{{ item.name }}</h4>
                            </v-btn>
                          </template>
                        </div>
                        <v-divider />
                        <div>
                          <v-btn
                            v-if="!loadingData && externalDataHasNoChildren !== 0"
                            :prepend-icon="selectAllBtnIcon"
                            variant="plain"
                            class="ms-1 ps-2"
                            @click="selectAllBtnAction"
                          >
                            {{ selectAllBtnText }}
                          </v-btn>
                        </div>

                        <div class="mb-auto overflow-y-auto">
                          <v-skeleton-loader
                            v-if="loadingData"
                            type="list-item-avatar"
                          />
                          <div v-else>
                            <div v-if="errorMessage !== null" class="mt-2">
                              <v-alert type="error">
                                <v-alert-title>
                                  {{ errorMessage.message }}
                                </v-alert-title>

                                <p>Details</p>

                                <ul class="ms-7">
                                  <li
                                    v-for="(
                                      detail, index
                                    ) in errorMessage.details"
                                    :key="index"
                                  >
                                    {{ detail.resource }}: {{ detail.code }}
                                  </li>
                                </ul>
                              </v-alert>
                            </div>
                            <div class="mt-2">
                              <DataObjectItem
                                v-for="object in externalData"
                                :key="object.id"
                                :object="object"
                                :selected="selectedDataObject.has(object.id)"
                                :active="
                                  currentDataObject
                                    && object.id === currentDataObject.id
                                "
                                @choose="selectDataObject(object)"
                                @open="browseDataObject(object)"
                                @select="(event) => checkDataObject(event, object)"
                              />
                            </div>
                          </div>
                        </div>
                        <div class="mt-2">
                          <v-sheet class="d-flex align-center">
                            <div class="w-100">
                              <v-card
                                v-bind="props"
                                :elevation="3"
                                class="mb-2 mx-1 pa-3 ms-2"
                                height="110"
                                max-height="110"
                                max-width="700"
                              >
                                <div class="d-flex justify-space-between align-center">
                                  <h4>{{ $t("selected_data") }}</h4>
                                  <v-btn
                                    v-if="selectedDataObject.size > 0"
                                    size="x-small"
                                    @click="clearSelectedDataObject"
                                  >
                                    Clear
                                  </v-btn>
                                </div>
                                <v-container>
                                  <v-row>
                                    <v-col>
                                      <v-chip-group>
                                        <v-chip
                                          v-for="dataObject in selectedDataObject.values()"
                                          :key="dataObject.name"
                                          closable
                                          @click:close="removeSelectedDataObject(dataObject)"
                                        >
                                          <DataObjectIcon
                                            :data-object-icon="dataObject.icon"
                                          />
                                          {{ truncateTitle(dataObject.name, 30) }}
                                          <v-tooltip
                                            activator="parent"
                                            location="start"
                                          >
                                            <p class="text-h6">
                                              Overview
                                            </p>
                                            <p><b>Type</b>: {{ dataObject.type }}</p>
                                            <p><b>Media Type</b>: {{ dataObject.media_type }}</p>
                                            <p><b>Size</b>: {{ formatBytes(dataObject.size.value) }}</p>
                                          </v-tooltip>
                                        </v-chip>
                                      </v-chip-group>
                                    </v-col>
                                  </v-row>
                                </v-container>
                              </v-card>
                            </div>
                          </v-sheet>
                        </div>
                        <v-sheet class="d-flex justify-end">
                          <v-btn
                            class="ma-1"
                            width="170"

                            @click="clearStates"
                          >
                            Back
                          </v-btn>

                          <v-btn
                            :disabled="
                              selectedDataObject.size === 0
                            "
                            class="ma-1 me-2"
                            width="200"

                            :loading="loadingCreateDatalink"
                            @click="createData()"
                          >
                            Create datalink ({{ selectedDataObject.size }})
                          </v-btn>
                        </v-sheet>
                      </v-col>

                      <v-divider class="mx-4" vertical />

                      <v-col cols="3" class="fill-height">
                        <v-sheet height="70">
                          <div class="d-flex">
                            <img
                              class="align-self-center me-4"
                              :src="selectedConnector.icon"
                              alt=""
                              style="height: 30px"
                            >
                            <p class="align-self-center">
                              {{ selectedConnector.name }}
                            </p>
                          </div>

                          <p class="mt-2">
                            <i>{{ selectedConnection.name }}</i>
                          </p>

                          <v-tooltip activator="parent">
                            <p>Shared parameters</p>
                            <ul
                              v-for="(field, indexField) in sharedFields"
                              :key="indexField"
                            >
                              <li class="ms-6">
                                {{ field }}
                              </li>
                            </ul>
                          </v-tooltip>
                        </v-sheet>

                        <div
                          v-if="currentDataObject"
                          class="mx-auto text-center mt-6"
                        >
                          <DataObjectIcon
                            :data-object-icon="currentDataObject.icon"
                          />
                          <h4 v-if="currentDataObject.url" class="mt-2">
                            <a :href="currentDataObject.url" target="_blank">{{
                              currentDataObject.name
                            }}</a>
                          </h4>
                          <h4 v-else class="mt-2">
                            {{ currentDataObject.name }}
                          </h4>

                          <p class="text-caption mt-1">
                            {{ currentDataObject.type }}
                          </p>
                          <br>
                          <span
                            v-if="currentDataObject.description"
                            class="clamp"
                          ><i>{{ currentDataObject.description }}</i></span>
                        </div>
                      </v-col>
                    </v-row>
                  </template>
                </v-card>
              </v-stepper-window-item>
            </v-stepper-window>
            <v-snackbar
              v-model="errorMessageSnackbar.show"
              :attach="true"
              :timeout="5000"
              color="error"
            >
              {{ errorMessageSnackbar.message }}
              <template v-slot:action="{ attrs }">
                <v-btn
                  color="white"
                  text
                  v-bind="attrs"
                  @click="errorMessageSnackbar.show = false"
                >
                  Close
                </v-btn>
              </template>
            </v-snackbar>
          </template>
        </v-stepper>
      </v-card-text>
    </v-card>
  </v-dialog>
</template>

<style scoped>
.v-stepper-header {
  box-shadow: none !important;
}
.divider {
  border-left: 1px solid #ccc;
  height: 100%;
}
.vertical-line {
  width: 2px;
  height: 100%;
  background-color: #ccc;
}

.center-content {
  margin: auto;
}
</style>

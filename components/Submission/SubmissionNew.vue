<script setup lang="ts">
// ----------------------------------------------------------------
// Import
// ----------------------------------------------------------------

import AnyOfControlRenderer from "@/components/JsonFormRenderers/AnyOfRenderer.vue";
import ArrayControlRenderer from "@/components/JsonFormRenderers/ArrayControlRenderer.vue";
import ArrayLayoutRenderer from "@/components/JsonFormRenderers/ArrayLayoutRenderer.vue";
import MultiStringControlRenderer from "@/components/JsonFormRenderers/MultiStringControlRenderer.vue";
import OneOfEnumControlRenderer from "@/components/JsonFormRenderers/OneOfEnumControlRenderer.vue";
import OneOfControlRenderer from "@/components/JsonFormRenderers/OneOfRenderer.vue";
import { type Connection, type Connector, useConnectionStore } from "@/stores/useConnectionStore";
import { useSubmissionStore } from "@/stores/useSubmissionStore";
import { useWorkspaceStore } from "@/stores/useWorkspaceStore";
import {
  and,
  formatIs,
  isAnyOfControl,
  isObjectArrayControl,
  isObjectArrayWithNesting,
  isOneOfControl,
  isOneOfEnumControl,
  isPrimitiveArrayControl,
  isStringControl,
  or,
} from "@jsonforms/core";
import { type JsonFormsRendererRegistryEntry, rankWith, type Tester } from "@jsonforms/core";
import { JsonForms } from "@jsonforms/vue";
import { vuetifyRenderers } from "@jsonforms/vue-vuetify";
import { computed, markRaw, onMounted, ref } from "vue";

// ----------------------------------------------------------------
// PROPS
// ----------------------------------------------------------------

const props = defineProps({
  newSubmissionDialog: {
    type: Boolean,
    required: true,
  },
});

// ----------------------------------------------------------------
// Emits
// ----------------------------------------------------------------

const emits = defineEmits(["newSubmissionDialog"]);

// ----------------------------------------------------------------
// Variables
// ----------------------------------------------------------------

// Initialize AJV
let ajv = newAjv();

function buildRendererRegistryEntry(testRenderer: any, controlType: Tester) {
  const entry: JsonFormsRendererRegistryEntry = {
    renderer: testRenderer,
    tester: rankWith(3, controlType),
  };
  return entry;
}

const OneOfRendererEntry = buildRendererRegistryEntry(OneOfControlRenderer, isOneOfControl);
const AnyOfRendererEntry = buildRendererRegistryEntry(AnyOfControlRenderer, isAnyOfControl);

const ArrayControlEntry = buildRendererRegistryEntry(
  ArrayControlRenderer,
  or(isObjectArrayControl, isPrimitiveArrayControl),
);
const ArrayLayoutEntry = buildRendererRegistryEntry(ArrayLayoutRenderer, isObjectArrayWithNesting);
const OneOfEnumControlEntry = buildRendererRegistryEntry(
  OneOfEnumControlRenderer,
  isOneOfEnumControl,
);

const MultiStringControlEntry = buildRendererRegistryEntry(
  MultiStringControlRenderer,
  and(isStringControl, formatIs("multi")),
);

const customEntryNames = [
  "one-of-select-renderer",
  "array-control-renderer",
  "array-layout-renderer",
  "any-of-select-renderer",
  "oneof-enum-control-renderer",
  "MultiStringControlRenderer",
];

const renderers = [
  markRaw(OneOfEnumControlEntry),
  markRaw(OneOfRendererEntry),
  markRaw(ArrayControlEntry),
  markRaw(ArrayLayoutEntry),
  markRaw(AnyOfRendererEntry),
  markRaw(MultiStringControlEntry),
];

for (const key in vuetifyRenderers) {
  if (!customEntryNames.includes(vuetifyRenderers[key].renderer.name)) {
    renderers.push(markRaw(vuetifyRenderers[key]));
  }
}

const workspaceStore = useWorkspaceStore();
const connectionStore = useConnectionStore();
const submissionStore = useSubmissionStore();

const drawer = ref(false);

const stepperItems = ["Choose a connector", "Connect", "Finalize"];
const step = ref(1);
const steps = 3;

const connectors = ref<Array<Connector>>([]);
const connections = ref<Array<Connection>>([]);
const connectionsEmpty = ref(false);
const currentPageConnection = ref(0);
const loadingConnection = ref(false);

const selectedConnector = ref<any>(null);
const expandedConnector = ref("");
const selectedConnection = ref<any>(null);

const sharedFields = ref({});
const sharedFieldSchemas = ref({});
const prefilledSharedFields = ref(false);
const privateFields = ref({});
const privateFieldSchemas = ref({});

const submissionTitle = ref("");
const submissionTitleSchema = ref(null);
const settings = ref({});
const settingsSchemas = ref({});

const validationErrors = ref([]);
const invalidCount = ref<number>(0);

// ----------------------------------------------------------------
// Watch
// ----------------------------------------------------------------

watch(
  () => props.newSubmissionDialog,
  async (newValue) => {
    if (newValue) {
      drawer.value = newValue;
      clearStates();
    }
  },
);

// ----------------------------------------------------------------
// Lifecycle
// ----------------------------------------------------------------

onMounted(async () => {
  if (connectionStore.connectorMap.size === 0) {
    await connectionStore.fetchConnectors();
  }
  connectors.value = Object.values(Object.fromEntries(connectionStore.connectorMap)).filter(
    connector => connector.types?.includes("submission_connector"),
  );
});

// ----------------------------------------------------------------
// Functions
// ----------------------------------------------------------------

const hasSharedParams = computed(() => {
  const safeSharedFields = sharedFields.value || {};

  return Object.values(safeSharedFields).every(field => (
    Object.keys(field).length > 0
    && Object.values(field).every(value => value !== "")
    && validationErrors.value.filter(error => error.setting !== field).length === 0
  ));
});

/**
 * Closes the dialog, resets all relevant states and fields and emits the
 * "newSubmissionDialog" event to notify that the dialog has been closed.
 */
function close() {
  drawer.value = false;
  clearStates();
  emits("newSubmissionDialog", false);
}

/**
 * Resets all the states and fields to their initial values.
 *
 * This function is used to clear the state when closing the dialog or resetting the form.
 * It resets the stepper to the first step, clears the connections array and flags,
 * resets the selected connector and connection ID, clears shared and private fields,
 * clears external data and loading flags, and resets error messages.
 */
function clearStates() {
  connections.value = [];
  connectionsEmpty.value = false;
  currentPageConnection.value = 0;
  loadingConnection.value = false;

  selectedConnector.value = null;
  expandedConnector.value = "";
  selectedConnection.value = null;

  // Reset shared fields
  sharedFields.value = {};
  prefilledSharedFields.value = false;
  privateFields.value = {};
  sharedFieldSchemas.value = {};
  privateFieldSchemas.value = {};

  // Reset submission title and settings
  submissionTitle.value = "";
  submissionTitleSchema.value = null;
  settings.value = {};
  settingsSchemas.value = {};

  // Reset validation errors
  validationErrors.value = [];
  invalidCount.value = 0;

  // Reset stepper to the first step
  step.value = 1;
}

/**
 * Skips the current connection creation process and proceeds to the settings step.
 *
 * This function attempts to create an empty connection by calling `createConnection(true)`,
 * indicating that no credentials or private/shared parameters should be provided.
 *
 * If the connection creation is successful, it loads the settings for the selected connection
 * and moves to step 3 in the process. If the connection creation fails, it stops the loading
 * state and rethrows the error.
 *
 */
async function skipConnection() {
  loadingConnection.value = true;
  try {
    // If no connection exists, create a new one
    await createConnection();
  } catch (error) {
    loadingConnection.value = false;
    throw error;
  }
  loadingConnection.value = false;
  await loadSettings(selectedConnection.value.connector);
  step.value = 3;
}

/**
 * Loads connections when the drawer is open and updates the state based on the fetched connections.
 *
 * If the modal is open and connections are not marked as empty, it fetches connections.
 * If connections are fetched successfully and there are still more connections that can be fetched, it calls the `done` callback with "ok".
 * Otherwise, it marks connections as empty and calls the `done` callback with "empty".
 *
 * @param {object} params - The parameters object.
 * @param {Function} params.done - The callback function to be called with the status of the operation.
 */
async function loadConnections({ done }) {
  if (drawer.value) {
    if (!connectionsEmpty.value) {
      currentPageConnection.value = currentPageConnection.value + 1;
      const { data: resConnections } = await useAPIFetch(
        `/api/connections?p=${
          currentPageConnection.value
        }&connector_type=submission_connector`,
        { headers: { "X-Workspace": workspaceStore.currentWid } },
      );

      resConnections.value?.results.forEach((connection) => {
        connections.value.push(connectionStore.asConnection(connection));
      });

      if (resConnections.value?.next !== null && resConnections.value?.next !== undefined) {
        done("ok");
      } else {
        connectionsEmpty.value = true;
        done("empty");
      }
    } else {
      done("empty");
    }
  }
}

function expandConnector(connector) {
  if (expandedConnector.value === connector.id) {
    expandedConnector.value = "";
  } else {
    expandedConnector.value = connector.id;
  }
}

function selectConnector(connector) {
  selectedConnector.value = connector;
  step.value = 2;
}

async function loadSettings(connector) {
  ajv = newAjv();

  // load title schema
  const { data: titleSchema } = await useAPIFetch("/api/schemas/referential/text");
  submissionTitleSchema.value = titleSchema.value;

  // load settings and settings schemas
  for (const schemaURL of connector.settings) {
    const { data: schema } = await useAPIFetch(schemaURL);
    const schemaName = schemaURL.substring(schemaURL.lastIndexOf("/") + 1);
    settingsSchemas.value[schemaName] = schema;
    settings.value[schemaName] = "";
  }
}

async function connect() {
  loadingConnection.value = true;
  try {
    // Check if a connection already exists
    if (selectedConnection.value?.id) {
      // If a connection exists, update it
      await updateConnection();
    } else {
      // If no connection exists, create a new one
      await createConnection();
    }
  } catch (error) {
    loadingConnection.value = false;
    throw error;
  }
  loadingConnection.value = false;
  validationErrors.value = [];
  invalidCount.value = 0;
  await loadSettings(selectedConnection.value.connector);
  step.value = 3;
}

/**
 * Updates an existing connection with new private fields
 * by calling the `updateConnection` method from the `connectionStore`.
 */
async function updateConnection() {
  const privateParams = Object.assign({}, ...Object.values(privateFields.value));
  await connectionStore.updateConnection(selectedConnection.value.id, privateParams);
}

/**
 * Creates a new connection using the selected connector and the provided shared and private parameters
 * by calling the `createConnection` method from the `connectionStore`.
 */
async function createConnection() {
  // Call the unified createConnection function with the appropriate arguments
  const connection = await connectionStore.createConnection(
    selectedConnector.value.id,
    Object.assign({}, ...Object.values(sharedFields.value)), // Shared fields
    Object.assign({}, ...Object.values(privateFields.value)), // Private fields
  );

  // Update the selectedConnection and push the new connection into connections
  selectedConnection.value = connection;
  connections.value.push(connection);
}

async function connectFromConnection(connectionId) {
  try {
    // Find the index of the connection being loaded
    const connectionIndex = connections.value.findIndex(
      connection => connection.id === connectionId,
    );

    if (connectionIndex !== -1) {
      // Retrieve the connection object at the found index
      const connection = connections.value[connectionIndex];
      selectedConnection.value = connection;
      selectedConnector.value = connection.connector;

      // Set loading state for the specific connection
      connections.value[connectionIndex].loading = true;

      if (connection.status === "connected") {
        try {
          // Call the checkConnection method from the connection store
          await connectionStore.checkConnection(connectionId);

          // Fetch updated connection details
          await connectionStore.fetchConnection(selectedConnection.value.id);
          await loadSettings(selectedConnection.value.connector);
          step.value = 3;
        } catch (error) {
          // Handle error during patching
          prefilledSharedFields.value = true;
          await connectionStore.fetchConnection(selectedConnection.value.id);
          step.value = 2;
          throw error;
        } finally {
          loadingConnection.value = false;
          // Reset loading state for the specific connection
          connections.value[connectionIndex].loading = false;
        }
      } else {
        prefilledSharedFields.value = true;
        await connectionStore.fetchConnection(selectedConnection.value.id);
        step.value = 2;
        loadingConnection.value = false;
      }
    }
  } catch (error) {
    console.error(error);
  }
}

// Function to get changes from JSON form
function updateSettingsValue(event, schemaSlug) {
  ajv = newAjv();

  let value;
  let schema;
  if (schemaSlug === "title") {
    submissionTitle.value = event;
    value = submissionTitle.value;
    schema = submissionTitleSchema.value;
  } else {
    if (event !== null && event !== undefined) {
      settings.value[schemaSlug] = event;
    }
    value = settings.value[schemaSlug];
    schema = settingsSchemas.value[schemaSlug];
  }

  // Validate the new data and update validationErrors
  const validate = ajv.compile(schema);
  const valid = validate(value);

  if (!valid) {
    const fieldErrors = validate.errors.map(error => ({
      ...error,
      setting: schemaSlug,
    }));

    // Accumulate validation errors for all settings
    validationErrors.value = [
      ...validationErrors.value.filter(e => e.setting !== schemaSlug),
      ...fieldErrors,
    ];
  } else {
    // Remove previous errors for this field if validation passes
    validationErrors.value = validationErrors.value.filter(error => error.setting !== schemaSlug);
  }

  const invalidCountPerField = validationErrors.value
    .map(({ setting }) => setting)
    .reduce((settings, setting) => {
      const count = settings[setting] || 0;
      settings[setting] = count + 1;
      return settings;
    }, {});

  invalidCount.value = Object.keys(invalidCountPerField).length;
}

async function createSubmission() {
  await submissionStore
    .createSubmission(submissionTitle.value, selectedConnection.value.id, settings.value)
    .then((res) => {
      navigateTo(`/${workspaceStore.currentWSlug}/submissions-${res.value.id}`);
      close();
    })
    .catch((err) => {
      console.error(err);
      close();
    });
}
</script>

<template>
  <v-dialog
    v-model="drawer"
    persistent
    width="100%"
    height="850px"
    style="max-width: 1150px !important"
  >
    <v-card height="100%">
      <v-card-title class="d-flex">
        <v-sheet
          class="me-auto"
          style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis"
        >
          <b>Create a submission</b>
        </v-sheet>
        <v-sheet>
          <v-btn
            class="ms-2"
            icon="$close"
            density="comfortable"
            variant="plain"
            @click="close"
          />
        </v-sheet>
      </v-card-title>
      <v-card-text>
        <v-stepper
          v-model="step"
          elevation="0"
          height="100%"
          hide-actions
        >
          <template v-slot:default>
            <v-stepper-header>
              <template v-for="n in steps" :key="`${n}-step`">
                <v-stepper-item
                  class="py-4"
                  :complete="step > n"
                  step="Step {{ n }}"
                  :title="stepperItems[n - 1]"
                  :value="n"
                />

                <v-divider v-if="n !== steps" :key="n" />
              </template>
            </v-stepper-header>

            <v-stepper-window>
              <v-stepper-window-item
                v-for="n in steps"
                :key="`${n}-content`"
                :value="n"
              >
                <v-card height="650px" style="max-height: calc(100vh - 200px) !important">
                  <template v-if="step === 1">
                    <v-row class="fill-height">
                      <v-col style="width: 49%" class="fill-height">
                        <h3>{{ $t("choose_existing_co") }}</h3>
                        <div class="overflow-y-auto fill-height">
                          <v-infinite-scroll :items="connections" :onLoad="loadConnections">
                            <template v-for="(connection, index) in connections" :key="index">
                              <v-card class="ma-1 pa-2" :loading="connection.loading">
                                <div
                                  class="d-flex align-center"
                                  style="cursor: pointer"
                                  @click="connectFromConnection(connection.id)"
                                >
                                  <v-badge
                                    :color="
                                      connectionStore.getStatus(connection.id, connection.status)
                                        .color
                                    "
                                    inline
                                    class="me-2"
                                  />

                                  <img
                                    class="align-self-center me-4"
                                    :src="connection.connector.icon"
                                    alt=""
                                    style="max-width: 60px; max-height: 40px"
                                  >

                                  <div>
                                    <p>
                                      <b>{{ connection.connector.name }}</b>
                                    </p>
                                    <p>{{ connection.name }}</p>
                                  </div>
                                </div>
                              </v-card>
                            </template>
                            <template v-slot:empty>
                              <i> No more items</i>
                            </template>
                          </v-infinite-scroll>
                        </div>
                      </v-col>

                      <v-col
                        style="max-width: 50px"
                        class="d-flex flex-column align-center text-center"
                      >
                        <div class="vertical-line" />

                        <div class="center-content">
                          or
                        </div>

                        <div class="vertical-line" />
                      </v-col>

                      <v-col class="fill-height" style="width: 49%">
                        <h3>{{ $t("create_a_connection") }}</h3>
                        <v-skeleton-loader
                          v-if="connectors.length === 0"
                          type="card"
                        />
                        <div v-else class="mt-3">
                          <v-row>
                            <v-col
                              v-for="(connector, index) in connectors"
                              :key="index"
                              :cols="expandedConnector === connector.id ? 12 : 6"
                            >
                              <v-card
                                class="pl-3 pb-1"
                                link
                                :ripple="false"
                              >
                                <div class="d-flex justify-space-between align-start">
                                  <v-sheet
                                    class="d-flex flex-grow-1"
                                    height="70"
                                    @click="selectConnector(connector)"
                                  >
                                    <img
                                      class="align-self-center me-4"
                                      :src="connector.icon"
                                      alt=""
                                      style="max-width: 60px; max-height: 40px"
                                    >
                                    <p class="align-self-center">
                                      {{
                                        expandedConnector === connector.id
                                          ? connector.name
                                          : truncateTitle(connector.name, 8)
                                      }}
                                    </p>
                                  </v-sheet>
                                  <v-card-actions>
                                    <v-btn
                                      :icon="
                                        expandedConnector === connector.id
                                          ? 'mdi-arrow-collapse'
                                          : 'mdi-arrow-expand'
                                      "
                                      variant="plain"
                                      :ripple="false"
                                      size="small"
                                      @click="expandConnector(connector)"
                                    />
                                  </v-card-actions>
                                </div>
                                <v-expand-transition>
                                  <div
                                    v-if="expandedConnector === connector.id"
                                    class="text-body-2"
                                    @click="selectConnector(connector)"
                                  >
                                    <em>{{ connector.description }}</em>
                                    <p>
                                      For further informations please refer to the
                                      <a
                                        :href="connector.documentation"
                                        target="_blank"
                                        rel="noopener noreferrer"
                                      >
                                        {{ connector.name }} documentation</a>.
                                    </p>
                                  </div>
                                </v-expand-transition>
                              </v-card>
                            </v-col>
                          </v-row>
                        </div>
                      </v-col>
                    </v-row>
                  </template>
                  <template v-if="step === 2">
                    <v-row class="fill-height">
                      <v-col class="fill-height d-flex flex-column">
                        <div class="mb-auto">
                          <h3>{{ $t("complete_form_to_connect") }} {{ selectedConnector.name }}</h3>
                          <br>
                          <ConnectionFields
                            v-model:current-connector="selectedConnector"
                            v-model:current-connection="selectedConnection"
                            v-model:shared-fields="sharedFields"
                            v-model:private-fields="privateFields"
                            v-model:shared-field-schemas="sharedFieldSchemas"
                            v-model:private-field-schemas="privateFieldSchemas"
                            v-model:read-only-shared-fields="prefilledSharedFields"
                            v-model:validation-errors="validationErrors"
                            v-model:invalid-count="invalidCount"
                          />
                        </div>
                        <v-sheet class="d-flex justify-end">
                          <v-btn
                            class="ma-2"
                            width="150"
                            @click="clearStates"
                          >
                            Back
                          </v-btn>
                          <v-tooltip bottom :disabled="hasSharedParams">
                            <template v-slot:activator="{ props: slotProps }">
                              <div v-bind="slotProps" class="d-inline-block">
                                <v-btn
                                  v-bind="props"
                                  class="ma-2"
                                  width="150"
                                  :disabled="!hasSharedParams"
                                  @click="skipConnection"
                                >
                                  skip
                                </v-btn>
                              </div>
                            </template>
                            <span>Shared parameters are required.</span>
                          </v-tooltip>

                          <v-tooltip bottom :disabled="invalidCount === 0">
                            <template v-slot:activator="{ props: slotProps }">
                              <div v-bind="slotProps" class="d-inline-block">
                                <v-btn
                                  :loading="loadingConnection"
                                  :disabled="invalidCount > 0"
                                  class="ma-2"
                                  width="150"
                                  @click="connect()"
                                >
                                  Connect
                                </v-btn>
                              </div>
                            </template>
                            <span>Shared and Private parameters are required.</span>
                          </v-tooltip>
                        </v-sheet>
                      </v-col>

                      <v-divider class="mx-4" vertical />

                      <v-col cols="3" class="d-flex flex-column fill-height">
                        <v-sheet class="d-flex" height="70">
                          <img
                            class="align-self-center me-4"
                            :src="selectedConnector.icon"
                            alt=""
                            style="height: 30px"
                          >
                          <p class="align-self-center">
                            {{ selectedConnector.name }}
                          </p>
                        </v-sheet>
                        <p>{{ selectedConnector.description }}</p>
                        <p>
                          For further informations please refer to the
                          <a
                            :href="selectedConnector.documentation"
                            target="_blank"
                            rel="noopener noreferrer"
                          >
                            {{ selectedConnector.name }} documentation</a>.
                        </p>
                      </v-col>
                    </v-row>
                  </template>
                  <template v-if="step === 3">
                    <v-row class="fill-height">
                      <v-col class="fill-height d-flex flex-column">
                        <div class="mb-auto">
                          <h3 class="mb-4">
                            Submission title
                          </h3>
                          <JsonForms
                            :data="`New ${selectedConnection.connector.name} submission`"
                            :schema="submissionTitleSchema"
                            :renderers="renderers"
                            :ajv="ajv"
                            @change="(event) => updateSettingsValue(event.data, 'title')"
                          />
                          <h3 class="mb-4">
                            {{ $t("submissionSettings") }}
                          </h3>
                          <v-container fluid class="pa-0">
                            <div style="max-height: 400px; overflow-y: auto;">
                              <div v-if="Object.keys(settings).length > 0">
                                <div v-for="(value, schema, index) in settings" :key="index">
                                  <JsonForms
                                    :data="value"
                                    :schema="settingsSchemas[schema]"
                                    :renderers="renderers"
                                    :ajv="ajv"
                                    @change="(event) => updateSettingsValue(event.data, schema)"
                                  />
                                </div>
                              </div>

                              <div v-else class="pl-4">
                                No available settings for this connector, please proceed.
                              </div>
                            </div>
                          </v-container>
                        </div>

                        <v-sheet class="d-flex justify-end">
                          <v-btn
                            class="ma-2"
                            width="150"
                            @click="clearStates"
                          >
                            Back
                          </v-btn>

                          <v-tooltip bottom :disabled="invalidCount === 0">
                            <template v-slot:activator="{ props: slotProps }">
                              <div v-bind="slotProps">
                                <v-btn
                                  class="ma-2 align-self-center"
                                  width="220"
                                  :disabled="invalidCount > 0"
                                  @click="createSubmission"
                                >
                                  Create submission

                                  <!-- Badge with the error count -->
                                  <v-badge
                                    v-if="validationErrors.length > 0"
                                    :content="invalidCount"
                                    color="red"
                                    class="position-absolute top-0 right-0 cursor-default"
                                  />
                                </v-btn>
                              </div>
                            </template>
                            <span> You must resolve errors first </span>
                          </v-tooltip>
                        </v-sheet>
                      </v-col>

                      <v-divider class="mx-4" vertical />

                      <v-col cols="3" class="d-flex flex-column fill-height">
                        <v-sheet class="d-flex" height="70">
                          <img
                            class="align-self-center me-4"
                            :src="selectedConnector.icon"
                            alt=""
                            style="height: 30px"
                          >
                          <p class="align-self-center">
                            {{ selectedConnector.name }}
                          </p>
                        </v-sheet>
                        <p>{{ selectedConnector.description }}</p>
                      </v-col>
                    </v-row>
                  </template>
                </v-card>
              </v-stepper-window-item>
            </v-stepper-window>
          </template>
        </v-stepper>
      </v-card-text>
    </v-card>
  </v-dialog>
</template>

<script setup>
// ----------------------------------------------------------------
// Import
// ----------------------------------------------------------------

import { useSubmissionStore } from "@/stores/useSubmissionStore";
import { useWorkspaceStore } from "@/stores/useWorkspaceStore";

// ----------------------------------------------------------------
// Emits
// ----------------------------------------------------------------

const emits = defineEmits(["showDialog"]);

// ----------------------------------------------------------------
// Variables
// ----------------------------------------------------------------

const workspaceStore = useWorkspaceStore();
const submissionStore = useSubmissionStore();
const editedName = ref(submissionStore.currentSubmission?.title);
const isEditingTitle = ref(false);

const submissionTitleInput = ref("");
const deleteModal = ref(false);
const leaveModal = ref(false);

// ----------------------------------------------------------------
// Computed
// ----------------------------------------------------------------

const submissionTitle = computed(() => submissionStore.currentSubmission?.title);

// ----------------------------------------------------------------
// Functions
// ----------------------------------------------------------------

function enableEditing() {
  isEditingTitle.value = true;
}

function cancelEdit() {
  editedName.value = submissionStore.currentSubmission?.title;
  isEditingTitle.value = false;
}

async function saveTitle() {
  submissionStore.currentSubmission.title = editedName.value;
  submissionStore.addUpdatedSubmission(submissionStore.currentSubid);
  await submissionStore.saveAndRefresh();
  isEditingTitle.value = false;
}

async function deleteSubmission() {
  // Handle deleting workspace
  await submissionStore.deleteSubmission(submissionStore.currentSubid);
  // Redirect to "/"
  await navigateTo(`/${workspaceStore.currentWSlug}/submissions`);
  closeDeleteModal();
  emits("showDialog", false);
}

function openDeleteModal() {
  deleteModal.value = true;
}

function closeDeleteModal() {
  deleteModal.value = false;
}

// This `canModify` function is checking if the current user can modify the submission.
function canModify() {
  return ["owner", "data broker", "manager", "contributor"].includes(submissionStore.currentMember?.value.role.role);
}

// This `canLeaveCurrentSubmission` function is checking if the current member of a
// submission is an owner or not.
function canLeaveCurrentSubmission() {
  if (submissionStore.currentMember && submissionStore.currentMember.value.id !== null) {
    if (submissionStore.currentMember?.value.role.role === "owner") {
      return false;
    } else {
      return true;
    }
  }
  return false;
}

function openLeaveModal() {
  leaveModal.value = true;
}

function closeLeaveModal() {
  leaveModal.value = false;
}

async function leaveWorkspace() {
  submissionStore.deleteMember(submissionStore.currentMember?.value).then(async (res) => {
    if (res === true) {
      await navigateTo(`/${workspaceStore.currentWSlug}/submissions`);
      closeLeaveModal();
      emits("showDialog", false);
    }
  });
}
</script>

<template>
  <v-sheet class="me-auto" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis">
    <h1>{{ $t("general") }}</h1>
  </v-sheet>
  <hr class="mb-3">
  <div class="mt-4">
    <h3 class="mb-3 me-auto">
      {{ $t("title") }}
    </h3>

    <v-row align="center">
      <v-col cols="10">
        <v-tooltip
          location="end"
          :disabled="canModify()"
        >
          <template v-slot:activator="{ props: formProps }">
            <span v-bind="formProps">
              <v-text-field
                v-model="editedName"
                class="text-field w-100"
                :readonly="!canModify() || submissionStore.isLocked"
                @input="enableEditing"
              />
            </span>
          </template>
          <span v-if="submissionStore.currentMember?.value.role.role === null">
            You can't modify content as you are not a member of the submission.
          </span>
          <span v-else-if="submissionStore.currentMember?.value.role.role === 'collaborator'">
            Collaborators can't modify content.
          </span>
        </v-tooltip>
      </v-col>
      <v-col
        v-if="isEditingTitle"
        class="mb-5"
        cols="2"
      >
        <v-icon class="check-icon cursor-pointer" @click="saveTitle">
          mdi-check
        </v-icon>
        <v-icon class="cancel-icon cursor-pointer" @click="cancelEdit">
          mdi-close
        </v-icon>
      </v-col>
    </v-row>
  </div>

  <!-- Section "Danger" -->
  <div class="mt-4">
    <h3 class="mb-3 me-auto text-error">
      Danger zone
    </h3>
    <div>
      <v-tooltip location="bottom" :disabled="canLeaveCurrentSubmission() && !submissionStore.isLocked">
        <template v-slot:activator="{ props }">
          <div v-bind="props" class="d-inline-block">
            <v-btn
              :disabled="!canLeaveCurrentSubmission() || submissionStore.isLocked"
              class="mx-4"
              @click="openLeaveModal"
            >
              Leave Submission
            </v-btn>
          </div>
        </template>
        <span v-if="submissionStore.currentMember?.value.role.role === null">
          You can't leave this submission as you are not a member of it.
        </span>
        <span v-else-if="submissionStore.currentMember?.value.role.role === 'owner'">
          You can't leave this submission as you are the only owner.
        </span>
        <span v-else-if="submissionStore.isLocked">
          {{ $t("submission_locked") }}
        </span>
      </v-tooltip>

      <v-tooltip
        location="bottom"
        :disabled="submissionStore.currentMember?.value.role.role === 'owner'"
      >
        <template v-slot:activator="{ props }">
          <div v-bind="props" class="d-inline-block">
            <div class="d-inline-block">
              <v-btn
                color="error"
                :disabled="submissionStore.currentMember?.value.role.role !== 'owner'"
                @click="openDeleteModal"
              >
                Delete Submission
              </v-btn>
            </div>
          </div>
        </template>
        <span>You need to be owner to delete this Submission</span>
      </v-tooltip>
    </div>
  </div>

  <!-- Delete submission Modal -->
  <v-dialog v-model="deleteModal" max-width="500">
    <v-card>
      <v-card-title>Do you really want to delete this submission ?</v-card-title>
      <v-card-text>
        <p>By deleting this submission all submission elements will be deleted.</p>

        <div v-if="submissionTitle">
          <p class="mt-2">
            To confirm, type the name of the submission:
          </p>

          <v-text-field
            v-if="submissionTitleInput !== submissionTitle"
            v-model="submissionTitleInput"
            label="submission name"
            autofocus
            variant="solo"
          />
          <v-text-field
            v-else
            v-model="submissionTitleInput"
            label="submission name"
            variant="solo"
            autofocus
            @keyup.enter="deleteSubmission()"
          />
        </div>
      </v-card-text>
      <v-card-actions>
        <v-btn @click="closeDeleteModal">
          Cancel
        </v-btn>
        <v-btn
          color="error"
          :disabled="submissionTitleInput !== submissionTitle"
          @click="deleteSubmission()"
        >
          Delete the submission
        </v-btn>
      </v-card-actions>
    </v-card>
  </v-dialog>

  <!-- Leave Workspace Modal -->
  <v-dialog
    v-model="leaveModal"
    width="auto"
    persistent
  >
    <v-card>
      <v-card-title><v-icon>mdi-exit-to-app</v-icon> Leaving the workspace</v-card-title>
      <v-card-text>
        You are about to leave the submission <b>{{ submissionStore.currentSubmission.title }}</b>. <br>
        Are you sure?
      </v-card-text>
      <v-card-actions class="d-flex flex-row-reverse">
        <v-btn color="error" @click="leaveWorkspace()">
          Leave
        </v-btn>
        <v-btn @click="closeLeaveModal()">
          Cancel
        </v-btn>
      </v-card-actions>
    </v-card>
  </v-dialog>
</template>

<style></style>

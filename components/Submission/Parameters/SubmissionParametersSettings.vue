<script setup lang="ts">
// ----------------------------------------------------------------
// Import
// ----------------------------------------------------------------

import AnyOfControlRenderer from "@/components/JsonFormRenderers/AnyOfRenderer.vue";
import ArrayControlRenderer from "@/components/JsonFormRenderers/ArrayControlRenderer.vue";
import ArrayLayoutRenderer from "@/components/JsonFormRenderers/ArrayLayoutRenderer.vue";
import OneOfEnumControlRenderer from "@/components/JsonFormRenderers/OneOfEnumControlRenderer.vue";
import OneOfControlRenderer from "@/components/JsonFormRenderers/OneOfRenderer.vue";
import { useConnectionStore } from "@/stores/useConnectionStore";
import { useSubmissionStore } from "@/stores/useSubmissionStore";
import {
  isAnyOfControl,
  isObjectArrayControl,
  isObjectArrayWithNesting,
  isOneOfControl,
  isOneOfEnumControl,
  isPrimitiveArrayControl,
  or,
} from "@jsonforms/core";
import { type JsonFormsRendererRegistryEntry, rankWith, type Tester } from "@jsonforms/core";
import { JsonForms } from "@jsonforms/vue";
import { vuetifyRenderers } from "@jsonforms/vue-vuetify";
import { markRaw } from "vue";

// ----------------------------------------------------------------
// Variables
// ----------------------------------------------------------------

const connectionStore = useConnectionStore();
const submissionStore = useSubmissionStore();

const currentConnection = ref(null);

const settings = ref({});
const newSettings = ref({});
const settingsSchemas = ref({});
const validationErrors = ref([]);
const invalidCount = ref<number>(0);
const unsavedSettings = ref(false);

// ----------------------------------------------------------------
// LIFECYCLE
// ----------------------------------------------------------------

onMounted(async () => {
  if (submissionStore.currentSubid) {
    await connectionStore.fetchConnections();
    currentConnection.value = connectionStore.getConnection(
      submissionStore.currentSubmission.connectionID,
    );
    await loadSettings();
  }
});

// ----------------------------------------------------------------
// JSONForm
// ----------------------------------------------------------------

// Initialize AJV
let ajv = newAjv();

function buildRendererRegistryEntry(testRenderer: any, controlType: Tester) {
  const entry: JsonFormsRendererRegistryEntry = {
    renderer: testRenderer,
    tester: rankWith(3, controlType),
  };
  return entry;
}

const OneOfRendererEntry = buildRendererRegistryEntry(OneOfControlRenderer, isOneOfControl);
const AnyOfRendererEntry = buildRendererRegistryEntry(AnyOfControlRenderer, isAnyOfControl);

const ArrayControlEntry = buildRendererRegistryEntry(
  ArrayControlRenderer,
  or(isObjectArrayControl, isPrimitiveArrayControl),
);
const ArrayLayoutEntry = buildRendererRegistryEntry(ArrayLayoutRenderer, isObjectArrayWithNesting);
const OneOfEnumControlEntry = buildRendererRegistryEntry(
  OneOfEnumControlRenderer,
  isOneOfEnumControl,
);

const customEntryNames = [
  "one-of-select-renderer",
  "array-control-renderer",
  "array-layout-renderer",
  "any-of-select-renderer",
  "oneof-enum-control-renderer",
];

const renderers = [
  markRaw(OneOfEnumControlEntry),
  markRaw(OneOfRendererEntry),
  markRaw(ArrayControlEntry),
  markRaw(ArrayLayoutEntry),
  markRaw(AnyOfRendererEntry),
];

for (const key in vuetifyRenderers) {
  if (!customEntryNames.includes(vuetifyRenderers[key].renderer.name)) {
    renderers.push(markRaw(vuetifyRenderers[key]));
  }
}

// ----------------------------------------------------------------
// FUNCTIONS
// ----------------------------------------------------------------

// This `canModify` function is checking if the current user can modify the submission.
function canModify() {
  return ["owner", "data broker", "manager", "contributor"].includes(submissionStore.currentMember?.value.role.role);
}

/**
 * Loads the settings for the current submission, fetches schemas from the API,
 * and prepares the settings for display in the form. It initializes the settings
 * from the submission store and sets up the schemas for validation.
 */
async function loadSettings() {
  await submissionStore.fetchSubmissionSettings();
  ajv = newAjv();

  for (const schemaURL of currentConnection.value.connector.settings) {
    const { data: schema } = await useAPIFetch(schemaURL);
    const schemaName = schemaURL.substring(schemaURL.lastIndexOf("/") + 1);
    settingsSchemas.value[schemaName] = schema;
    settings.value[schemaName] = submissionStore.currentSubmission.settings[schemaName].value
      ? submissionStore.currentSubmission.settings[schemaName].value
      : "";
    newSettings.value[schemaName] = settings.value[schemaName];
  }
}

/**
 * Saves the modified settings for the current submission to the backend.
 * It checks if there are any changes and only sends updated fields.
 * Each updated field is identified by its `field` name, which is mapped to its `settingsId`.
 */
async function saveSettings() {
  const currentSubmission = submissionStore.currentSubmission;
  if (!currentSubmission) {
    console.warn("No current submission available.");
    return;
  }

  // Isolate only modified settings
  const updatedFields = Object.entries(newSettings.value).reduce((acc, [field, value]) => {
    if (settings.value[field] !== value) {
      acc[field] = { value: typeof value === "object" ? JSON.stringify(value) : value };
    }
    return acc;
  }, {});

  // Check if there are any fields to update
  if (Object.keys(updatedFields).length > 0) {
    for (const [field, payload] of Object.entries(updatedFields)) {
      const settingsId = currentSubmission.fieldToIdMap[field]; // Get the settingsId
      if (!settingsId) {
        console.warn(`No ID found for field '${field}'. Skipping.`);
        continue;
      }

      await submissionStore.updateSubmissionSettings(submissionStore.currentSubid, settingsId, payload);
    }

    // Update local state
    unsavedSettings.value = false;
    settings.value = { ...newSettings.value };
  }
}

/**
 * Updates the value of a specific setting in the form and performs validation.
 * The updated value is stored in `newSettings`, and validation errors are updated accordingly.
 * It also updates the validation error list if validation fails for the new setting.
 */
function updateSettingsValue(event, schemaSlug) {
  ajv = newAjv();

  if (event !== null && event !== undefined) {
    newSettings.value[schemaSlug] = event;
    if (newSettings.value[schemaSlug] !== settings.value[schemaSlug]) {
      unsavedSettings.value = true;
    }
  }

  // Validate the new data and update validationErrors
  const validate = ajv.compile(settingsSchemas.value[schemaSlug]);
  const valid = validate(newSettings.value[schemaSlug]);

  if (!valid) {
    const fieldErrors = validate.errors.map(error => ({
      ...error,
      setting: schemaSlug,
    }));

    // Accumulate validation errors for all settings
    validationErrors.value = [
      ...validationErrors.value.filter(e => e.setting !== schemaSlug),
      ...fieldErrors,
    ];
  } else {
    // Remove previous errors for this field if validation passes
    validationErrors.value = validationErrors.value.filter(error => error.setting !== schemaSlug);
  }

  const invalidCountPerField = validationErrors.value
    .map(({ setting }) => setting)
    .reduce((settings, setting) => {
      const count = settings[setting] || 0;
      settings[setting] = count + 1;
      return settings;
    }, {});

  invalidCount.value = Object.keys(invalidCountPerField).length;
}
</script>

<template>
  <v-sheet class="me-auto" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis">
    <h1>{{ $t("submissionSettings") }}</h1>
  </v-sheet>
  <hr class="mb-3">

  <div v-if="Object.keys(settings).length > 0" class="d-flex flex-column h-100">
    <div
      v-for="(value, schema, index) in newSettings"
      :key="index"
      class="flex-grow-0"
      style="max-height: 670px; overflow-y: auto"
    >
      <v-tooltip
        location="end"
        :disabled="canModify()"
      >
        <template v-slot:activator="{ props: formProps }">
          <span v-bind="formProps">
            <JsonForms
              :data="newSettings[schema]"
              :schema="settingsSchemas[schema]"
              :renderers="renderers"
              :ajv="ajv"
              :readonly="!canModify()"
              @change="(event) => updateSettingsValue(event.data, schema)"
            />
          </span>
        </template>
        <span v-if="submissionStore.currentMember?.value.role.role === null">
          You can't modify content as you are not a member of the submission.
        </span>
        <span v-else-if="submissionStore.currentMember?.value.role.role === 'collaborator'">
          Collaborators can't modify content.
        </span>
      </v-tooltip>
    </div>

    <v-sheet class="d-flex justify-end">
      <v-btn
        class="ma-2"
        width="100"
        @click="loadSettings"
      >
        Cancel
      </v-btn>

      <v-tooltip bottom :disabled="!(invalidCount > 0 || submissionStore.isLocked)">
        <template v-slot:activator="{ props }">
          <div v-bind="props">
            <v-btn
              class="ma-2 align-self-center"
              width="100"
              :disabled="invalidCount > 0 || !unsavedSettings || submissionStore.isLocked"
              :color="unsavedSettings ? 'warning' : ''"
              @click="saveSettings()"
            >
              Save

              <!-- Badge with the error count -->
              <v-badge
                v-if="validationErrors.length > 0"
                :content="invalidCount"
                color="red"
                class="position-absolute top-0 right-0 cursor-default"
              />
            </v-btn>
          </div>
        </template>
        <span v-if="invalidCount > 0"> You must resolve errors first </span>
        <span v-if="submissionStore.isLocked">
          {{ $t("submission_locked") }}
        </span>
      </v-tooltip>
    </v-sheet>
  </div>
  <div v-else class="pl-4">
    No available settings for this submission, please proceed.
  </div>
</template>

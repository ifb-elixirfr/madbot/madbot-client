<script setup>
// ----------------------------------------------------------------
// Import
// ----------------------------------------------------------------

import { useConnectionStore } from "@/stores/useConnectionStore";
import { useSubmissionStore } from "@/stores/useSubmissionStore";
import { useWorkspaceStore } from "@/stores/useWorkspaceStore";

// ----------------------------------------------------------------
// Props
// ----------------------------------------------------------------

const _props = defineProps({
  currentConnection: {
    type: Object,
    required: true,
    default: () => ({ connector: { supportedFiles: [] } }),
  },
});

// ----------------------------------------------------------------
// Emits
// ----------------------------------------------------------------

const emits = defineEmits([
  "dataCount",
]);

// ----------------------------------------------------------------
// Variables
// ----------------------------------------------------------------

const router = useRouter();
const workspaceStore = useWorkspaceStore();
const submissionStore = useSubmissionStore();
const connectionStore = useConnectionStore();
const submissionData = ref([]);
const loadingSubmissionData = ref(false);
const search = ref("");
const dataCount = ref(0);

const submissionDataHeaders = [
  { title: "Filename", text: "Filename", value: "data.name", width: "25%" },
  { title: "Location", text: "Location", value: "data.datalinks.node", width: "25%" },
  { title: "Connections", text: "Connections", value: "data.connection", width: "25%" },
  { title: "Access", text: "Access", value: "data.access", width: "15%" },
  { title: "", text: "Actions", value: "actions", width: "10%" },
];

// ----------------------------------------------------------------
// Lifecycle
// ----------------------------------------------------------------

onMounted(async () => {
  loadingSubmissionData.value = true;
  await fetchSubmissionData({ page: 1 });
  loadingSubmissionData.value = false;
});

// ----------------------------------------------------------------
// Functions
// ----------------------------------------------------------------

// The above code is defining a function called `canModify` that returns `true` if the user can modify content on this node
function canModify() {
  return ["owner", "manager", "data broker", "contributor"].includes(submissionStore.currentMember?.value.role.role);
}

function formatBytes(bytes) {
  if (bytes === null) return null;
  if (bytes === undefined) return null;

  const units = ["octets", "Ko", "Mo", "Go", "To"];

  let i = 0;
  while (bytes >= 1024 && i < units.length - 1) {
    bytes /= 1024;
    i++;
  }

  return `${bytes.toFixed(2)} ${units[i]}`;
}

// Retrive submission data for the current submission.
async function fetchSubmissionData({ page = 1 }) {
  loadingSubmissionData.value = true;
  const response = await useAPIFetch(`/api/submissions/${submissionStore.currentSubmission.id}/data?p=${page}`, {
    headers: { "X-Workspace": workspaceStore.currentWid },
  });
  const linksData = response.data.value.results;

  submissionData.value = [];
  if (linksData.length > 0) {
    await connectionStore.fetchConnections();

    linksData.forEach(async (datasubmission) => {
      const connection = connectionStore.connectionMap.get(datasubmission.data.connection);

      submissionData.value.push({
        id: datasubmission.id,
        connectionId: connection.id,
        connectorName: connection.name,
        dataID: datasubmission.data.id,
        dataName: datasubmission.data.name,
        datalinks: datasubmission.data.datalinks,
        size: formatBytes(datasubmission.data.size.value),
        icon: datasubmission.data.icon,
        status: connection.status,
        dataAccess: datasubmission.data.data_access,
      });
    });

    dataCount.value = linksData.length;
    emits("dataCount", dataCount.value);
  }
  loadingSubmissionData.value = false;
}

function getUniqueData(dataSelected) {
  const uniqueData = new Set();

  return dataSelected.filter((item) => {
    const isDuplicateInSubmission = submissionData.value.some(
      submission => submission.dataID === item.data,
    );
    const isDuplicateInSelected = uniqueData.has(item.data);

    if (!isDuplicateInSubmission && !isDuplicateInSelected) {
      uniqueData.add(item.data);
      return true;
    }

    return false;
  });
}

// Create submission data from selected files for the current submission.
async function createSubmissionData(selectedFile) {
  const uniqueData = getUniqueData(selectedFile);
  if (uniqueData.length > 0) {
    await useAPIFetch(`/api/submissions/${submissionStore.currentSubmission.id}/data`, {
      method: "POST",
      body: { data_objects: uniqueData }, // eslint-disable-line camelcase
      headers: { "X-Workspace": workspaceStore.currentWid },
    });
    await fetchSubmissionData({ page: 1 });
  }
}

// Delete selected data for the current submission.
async function deleteSubmissionData(selectedData) {
  await useAPIFetch(`/api/submissions/${submissionStore.currentSubmission.id}/data/${selectedData}`, {
    method: "DELETE",
    headers: { "X-Workspace": workspaceStore.currentWid },
  });
  await fetchSubmissionData({ page: 1 });
}
</script>

<template>
  <div class="d-flex flex-column my-3 pe-2">
    <div class="d-flex justify-end align-center mb-2">
      <v-tooltip :disabled="canModify() && !submissionStore.isLocked">
        <template v-slot:activator="{ props: btnProps }">
          <span v-bind="btnProps">
            <FileExplorer
              btnTitle="Add file"
              :lockedFileExplorer="!canModify() || submissionStore.isLocked"
              :multiSelection="true"
              :supportedFiles="currentConnection?.connector?.supportedFiles || []"
              @selectedFile="createSubmissionData"
            />
          </span>
        </template>
        <span v-if="!submissionStore.currentMember || submissionStore.currentMember.id === null">
          You can't modify content as you are not a member of the
          {{ submissionStore.currentSubmission.title }}.
        </span>
        <span v-else-if="submissionStore.currentMember?.value.role.role === 'collaborator'">
          Collaborators can't modify content.
        </span>
        <span v-else-if="submissionStore.isLocked">
          {{ $t("submission_locked") }}
        </span>
      </v-tooltip>
    </div>

    <!-- Table container -->
    <v-data-table
      :headers="submissionDataHeaders"
      :items-length="dataCount"
      :items="submissionData"
      item-key="id"
      class="elevation-1 fill-height fill-width"
      :loading="loadingSubmissionData"
      :search="search"
    >
      <!-- Filename column -->
      <template v-slot:[`item.data.name`]="{ item }">
        <span style="font-weight: bold"><v-icon
          :class="item.icon"
          size="small"
          class="mr-2 ml-2"
        />{{ item.dataName }}</span>
        <div class="text-caption mr-2 ml-2">
          {{ item.size }}
        </div>
      </template>

      <!-- Location column -->
      <template v-slot:[`item.data.datalinks.node`]="{ item }">
        <span v-for="(datalink, index) in item.datalinks" :key="index">
          <v-tooltip location="bottom" :disabled="datalink.node_title?.length < 35">
            <template v-slot:activator="{ props: titleProps }">
              <div v-bind="titleProps" class="d-inline-block">
                {{ truncateTitle(datalink.node_title, 35) }}
              </div>
            </template>
            <span>
              {{ datalink.node_title }}
            </span>
          </v-tooltip>
          <v-btn
            icon="mdi-open-in-new"
            size="x-small"
            variant="plain"
            :href="router.resolve({
              path: `/${workspaceStore.currentWSlug}/${datalink.node}`,
              query: { tab: 'dl' } }).href"
            target="_blank"
          />
        </span>
        <span v-if="!item.datalinks || item.datalinks.length === 0">
          <v-tooltip location="bottom">
            <template #activator="{ props }">
              <span v-bind="props" style="display: flex; align-items: center; cursor: pointer;">
                <v-icon color="error" style="margin-right: 4px;">
                  mdi-minus-circle-outline
                </v-icon>
                No access
              </span>
            </template>
            <span>You do not have access to the node where the data is located</span>
          </v-tooltip>
        </span>
      </template>

      <!-- Connections column with badge -->
      <template v-slot:[`item.data.connection`]="{ item }">
        <div class="d-flex align-center">
          <span
            class="status-indicator"
            :style="{
              backgroundColor: connectionStore.getStatus(item.id, item.status)
                .hex,
            }"
          />
          {{ item.connectorName }}
        </div>
      </template>

      <!-- Data Access -->
      <template v-slot:[`item.data.access`]="{ item }">
        <div v-if="'access_right' in item.dataAccess">
          <v-tooltip>
            <p><b>Last update: </b>: {{ new Date(item.dataAccess.last_updated_time).toUTCString() }}</p>
            <p><b>Last verification</b>: {{ new Date(item.dataAccess.last_verification_time).toUTCString() }} </p>
            <template v-slot:activator="{ props }">
              <div v-bind="props">
                <div v-if="item.dataAccess.access_right === 'unreachable'">
                  <v-icon color="error">
                    mdi-server-off
                  </v-icon> Unreachable
                </div>
                <div v-else-if="item.dataAccess.access_right === 'unaccessible'">
                  <v-icon color="error">
                    mdi-file-cancel
                  </v-icon> Not accessible
                </div>
                <div v-else-if="item.dataAccess.access_right === 'accessible'">
                  <v-icon color="success">
                    mdi-file-check
                  </v-icon> Accessible
                </div>
                <div v-else>
                  <v-icon color="info">
                    mdi-cloud-question
                  </v-icon> Not verified
                </div>
              </div>
            </template>
          </v-tooltip>
        </div>
        <div v-else>
          <v-icon color="info">
            mdi-cloud-question
          </v-icon> Not verified
        </div>
      </template>

      <!-- Actions column -->
      <template v-slot:[`item.actions`]="{ item }">
        <div class="d-flex justify-end">
          <v-menu>
            <template v-slot:activator="{ props }">
              <v-btn
                icon="mdi mdi-dots-vertical"
                variant="plain"
                size="small"
                v-bind="props"
              />
            </template>
            <v-list density="compact">
              <v-list-item
                title="Show details"
              >
                <template v-slot:prepend>
                  <v-icon
                    icon="mdi mdi-dots-horizontal-circle-outline"
                  />
                </template>
              </v-list-item>
              <v-dialog max-width="500">
                <template v-slot:activator="{ props: activatorProps }">
                  <v-list-item
                    title="Delete"
                    :disabled="!canModify() || submissionStore.isLocked"
                    v-bind="activatorProps"
                  >
                    <template v-slot:prepend>
                      <v-icon icon="mdi-trash-can-outline" />
                    </template>
                  </v-list-item>
                </template>
                <template v-slot:default="{ isActive }">
                  <v-card title="Confirmation">
                    <v-card-text>
                      <p>Are you sure you want to delete this data from the submission :</p>
                      <pre>{{ item.dataName }}</pre>
                    </v-card-text>

                    <v-card-actions>
                      <v-spacer />
                      <v-btn
                        text="Cancel"
                        @click="isActive.value = false"
                      />
                      <v-btn
                        text="Delete"
                        color="error"
                        @click="
                          isActive.value = false;
                          deleteSubmissionData(item.id);
                        "
                      />
                    </v-card-actions>
                  </v-card>
                </template>
              </v-dialog>
            </v-list>
          </v-menu>
        </div>
      </template>
    </v-data-table>
  </div>
</template>

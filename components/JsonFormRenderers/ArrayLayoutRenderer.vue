<script lang="ts">
import type { ErrorObject } from "ajv";

import {
  composePaths,
  type ControlElement,
  createDefaultValue,
  findUISchema,
  getControlPath,
  getI18nKey,
  isObjectArrayWithNesting,
  type JsonFormsRendererRegistryEntry,
  type JsonSchema,
  rankWith,
  Resolve,
  type UISchemaElement,
} from "@jsonforms/core";
import {
  DispatchRenderer,
  rendererProps,
  type RendererProps,
  useJsonFormsArrayControl,
} from "@jsonforms/vue";
import {
  i18nDefaultMessages,
  useNested,
  useTranslator,
  useVuetifyArrayControl,
  ValidationBadge,
  ValidationIcon,
} from "@jsonforms/vue-vuetify";
import merge from "lodash/merge";
import { computed, defineComponent, ref } from "vue";
import {
  VAvatar,
  VBtn,
  VCard,
  VCardActions,
  VCardText,
  VCardTitle,
  VCol,
  VContainer,
  VDialog,
  VExpansionPanel,
  VExpansionPanels,
  VExpansionPanelText,
  VExpansionPanelTitle,
  VIcon,
  VRow,
  VSpacer,
  VToolbar,
  VTooltip,
} from "vuetify/components";

type I18nArrayLayoutKey = keyof typeof i18nDefaultMessages.arraylayout;

const controlRenderer = defineComponent({
  name: "ArrayLayoutRenderer",
  components: {
    DispatchRenderer,
    VCard,
    VCardActions,
    VCardTitle,
    VCardText,
    VAvatar,
    VDialog,
    VRow,
    VCol,
    VToolbar,
    VTooltip,
    VIcon,
    VBtn,
    VSpacer,
    VExpansionPanels,
    VExpansionPanel,
    VExpansionPanelTitle,
    VExpansionPanelText,
    VContainer,
    ValidationIcon,
    ValidationBadge,
  },
  props: {
    ...rendererProps<ControlElement>(),
  },
  setup(props: RendererProps<ControlElement>) {
    const control = useVuetifyArrayControl(useJsonFormsArrayControl(props));
    const currentlyExpanded = ref<null | number>(
      // control.appliedOptions.value.initCollapsed ? null : 0
      null,
    );
    const expansionPanelsProps = computed(() =>
      merge({ flat: false, focusable: true }, control.vuetifyProps("v-expansion-panels")),
    );
    const cardProps = computed(() => control.vuetifyProps("v-card"));
    const suggestToDelete = ref<null | number>(null);
    // indicate to our child renderers that we are increasing the "nested" level
    useNested("array");
    const t = useTranslator();
    return {
      ...control,
      currentlyExpanded,
      cardProps,
      expansionPanelsProps,
      suggestToDelete,
      t,
    };
  },
  computed: {
    addDisabled(): boolean {
      return (
        !this.control.enabled
        || (this.appliedOptions.restrict
          && this.arraySchema !== undefined
          && this.arraySchema.maxItems !== undefined
          && this.dataLength >= this.arraySchema.maxItems)
      );
    },
    dataLength(): number {
      return this.control.data ? this.control.data.length : 0;
    },
    foundUISchema(): UISchemaElement {
      return findUISchema(
        this.control.uischemas,
        this.control.schema,
        this.control.uischema.scope,
        this.control.path,
        undefined,
        this.control.uischema,
        this.control.rootSchema,
      );
    },
    arraySchema(): JsonSchema | undefined {
      return Resolve.schema(
        this.control.rootSchema,
        this.control.uischema.scope,
        this.control.rootSchema,
      );
    },
    hideAvatar(): boolean {
      return !!this.appliedOptions.hideAvatar;
    },
    translatedLabels(): { [key in I18nArrayLayoutKey]: string } {
      const elementToDeleteText = this.childLabelForIndex(this.suggestToDelete);
      return {
        add: this.translateLabel("add"),
        delete: this.translateLabel("delete"),
        moveUp: this.translateLabel("moveUp"),
        moveDown: this.translateLabel("moveDown"),
        dialogTitle: this.translateLabel(
          "dialogTitle",
          {
            element: elementToDeleteText,
          },
          message => message.replace(/\{\{\s?element\s?\}\}/, elementToDeleteText || "element"),
        ),
        dialogText: this.translateLabel("dialogText"),
        dialogCancel: this.translateLabel("dialogCancel"),
        dialogConfirm: this.translateLabel("dialogConfirm"),
      };
    },
  },
  methods: {
    composePaths,
    createDefaultValue,
    addButtonClick() {
      this.addItem(
        this.control.path,
        createDefaultValue(this.control.schema, this.control.rootSchema),
      )();

      if (!this.appliedOptions.collapseNewItems && this.control.data?.length) {
        this.currentlyExpanded = this.dataLength - 1;
      }
    },
    moveUpClick(event: Event, toMove: number): void {
      event.stopPropagation();
      this.moveUp?.(this.control.path, toMove)();
    },
    moveDownClick(event: Event, toMove: number): void {
      event.stopPropagation();
      this.moveDown?.(this.control.path, toMove)();
    },
    removeItemsClick(toDelete: number[] | null): void {
      if (toDelete !== null) {
        this.removeItems?.(this.control.path, toDelete)();
      }
    },
    childErrors(index: number): ErrorObject[] {
      return this.control.childErrors.filter((e: ErrorObject) => {
        const errorDataPath = getControlPath(e);
        return errorDataPath.startsWith(this.composePaths(this.control.path, `${index}`));
      });
    },
    translateLabel(
      labelType: I18nArrayLayoutKey,
      additionalContext: Record<string, unknown> | undefined = undefined,
      transformMessage: (message: string) => string = text => text,
    ): string {
      const i18nKey = getI18nKey(
        this.arraySchema,
        this.control.uischema,
        this.control.path,
        labelType,
      );
      const context = {
        schema: this.control.schema,
        uischema: this.control.uischema,
        path: this.control.path,
        data: this.control.data,
        ...additionalContext,
      };
      const translation = this.t(i18nKey, undefined, context);
      if (translation !== undefined) {
        return translation;
      }
      return this.t(
        `arraylayout.${labelType}`,
        transformMessage(i18nDefaultMessages.arraylayout[labelType]),
        context,
      );
    },
    titlePreparation(additionalMetadata: object) {
      let temp = "";
      const key = Object.keys(additionalMetadata).find(key => key.endsWith("_tag"));
      if (key) {
        temp += additionalMetadata[key];
      }
      temp += ": ";

      const value = Object.keys(additionalMetadata).find(key => key.endsWith("_value"));

      if (value) {
        if (key === "date_time_tag") {
          temp += new Date(additionalMetadata[value]).toUTCString();
        } else if (key === "date_tag") {
          temp += new Date(additionalMetadata[value]).toDateString();
        } else {
          temp += additionalMetadata[value];
        }
      }

      return temp;
    },
  },
});

export default controlRenderer;

export const entry: JsonFormsRendererRegistryEntry = {
  renderer: controlRenderer,
  tester: rankWith(4, isObjectArrayWithNesting),
};
</script>

<template>
  <VCard
    v-if="control.visible"
    :class="styles.arrayList.root"
    v-bind="cardProps"
  >
    <VCardTitle>
      <VToolbar
        flat
        color="rgba(255, 0, 0, 0)"
        density="compact"
      >
        <!-- <v-toolbar-title :class="styles.arrayList.label">{{ computedLabel }}</v-toolbar-title> -->
        <slot
          name="toolbar-elements"
          :labels="translatedLabels"
          :addClass="styles.arrayList.addButton"
          :addDisabled="addDisabled"
          :addClick="addButtonClick"
          :control="control"
          :appliedOptions="appliedOptions"
          :styles="styles"
        >
          <VTooltip bottom>
            <template v-slot:activator="{ props }">
              <VBtn
                fab
                text
                elevation="0"
                small
                :aria-label="translatedLabels.add"
                v-bind="props"
                :class="styles.arrayList.addButton"
                :disabled="addDisabled"
                @click="addButtonClick"
              >
                <VIcon>mdi-plus</VIcon>
              </VBtn>
            </template>
            {{ translatedLabels.add }}
          </VTooltip>
        </slot>
        <VSpacer />
        <ValidationIcon
          v-if="control.childErrors.length > 0 && !appliedOptions.hideArraySummaryValidation"
          :errors="control.childErrors"
          :class="styles.arrayList.validationIcon"
        />
      </VToolbar>
    </VCardTitle>
    <VCardText>
      <VContainer
        justify-space-around
        align-content-center
        :class="styles.arrayList.container"
      >
        <VRow justify="center">
          <VExpansionPanels
            v-bind="expansionPanelsProps"
            v-model="currentlyExpanded"
            accordion
          >
            <VExpansionPanel
              v-for="(_element, index) in control.data"
              :key="`${control.path}-${index}`"
              :class="styles.arrayList.item"
            >
              <VExpansionPanelTitle :class="styles.arrayList.itemHeader">
                <VContainer py-0 :class="styles.arrayList.itemContainer">
                  <VRow
                    :style="`display: grid; grid-template-columns: ${
                      !hideAvatar ? 'min-content' : ''
                    } auto min-content ${
                      appliedOptions.showSortButtons ? 'min-content min-content' : ''
                    }`"
                  >
                    <VCol
                      v-if="!hideAvatar"
                      align-self="center"
                      class="pl-0"
                    >
                      <ValidationBadge
                        overlap
                        bordered
                        :errors="childErrors(index)"
                      >
                        <VAvatar
                          size="30"
                          aria-label="Index"
                          color="primary"
                        >
                          <span class="primary--text text--lighten-5">{{
                            index + 1
                          }}</span>
                        </VAvatar>
                      </ValidationBadge>
                    </VCol>

                    <VCol
                      align-self="center"
                      :class="`pl-0 text-truncate ${styles.arrayList.itemLabel}`"
                    >
                      <h3 v-if="'person' in _element">
                        <VIcon icon="mdi-account" start />{{ _element.person.firstname }}
                        {{ _element.person.lastname }}
                      </h3>

                      <h3 v-if="'institution' in _element">
                        <VIcon icon="mdi-domain" start />{{ _element.institution }}
                      </h3>

                      <h3 v-if="['pubmedID', 'doi'].some(key => key in _element)">
                        <VIcon icon="mdi-newspaper-variant" start />{{ _element.title }}
                      </h3>
                      <h3
                        v-if="'identifier' in _element
                          && typeof _element.identifier === 'object'
                          && _element.identifier !== null
                          && ['pubmedID', 'doi'].some(key => key in _element.identifier)
                        "
                      >
                        <VIcon icon="mdi-newspaper-variant" start />{{ _element.identifier.title }}
                      </h3>

                      <!-- Organization Name -->
                      <h3 v-if="'organization' in _element">
                        <VIcon icon="mdi mdi-town-hall" start />{{ _element.organization.name }}
                      </h3>

                      <h3 v-if=" control.rootSchema.title === 'Additional Metadata' && Object.keys(_element).length !== 0">
                        <VIcon
                          v-if="Object.keys(_element).includes('date_tag')"
                          icon="mdi-calendar-today"
                          start
                        />
                        <VIcon
                          v-else-if="Object.keys(_element).includes('date_time_tag')"
                          icon="mdi-calendar-clock"
                          start
                        />
                        <VIcon
                          v-else-if="Object.keys(_element).includes('time_tag')"
                          icon="mdi-clock"
                          start
                        />
                        <VIcon
                          v-else-if="Object.keys(_element).includes('text_tag')"
                          icon="mdi-text-box-outline"
                          start
                        />
                        <VIcon
                          v-else-if="Object.keys(_element).includes('email_tag')"
                          icon="mdi-at"
                          start
                        />
                        <VIcon
                          v-else-if="Object.keys(_element).includes('boolean_tag')"
                          icon="mdi-checkbox-intermediate-variant"
                          start
                        />
                        <VIcon
                          v-else-if="Object.keys(_element).includes('integer_tag')"
                          icon="mdi-numeric"
                          start
                        />
                        <VIcon
                          v-else-if="Object.keys(_element).includes('number_tag')"
                          icon="mdi-numeric"
                          start
                        />
                        <VIcon
                          v-else
                          icon="mdi-tag"
                          start
                        />
                        {{ titlePreparation(_element) }}
                      </h3>
                      <h3 v-if=" control.rootSchema.title === 'Protocol' && Object.keys(_element).length !== 0">
                        <VIcon
                          icon="mdi-tag"
                          start
                        />
                        {{ _element.title }}
                      </h3>

                      <h3 v-if="'term' in _element">
                        <VIcon icon="mdi-tag" start />{{ _element.term }}
                      </h3>
                    </VCol>
                    <VCol align-self="center d-flex flex-row ga-1">
                      <VTooltip bottom>
                        <template v-slot:activator="{ props }">
                          <VBtn
                            v-bind="props"
                            fab
                            text
                            elevation="0"
                            small
                            class="v-expansion-panel-title__icon"
                            :aria-label="translatedLabels.moveUp"
                            :disabled="index <= 0 || !control.enabled"
                            :class="styles.arrayList.itemMoveUp"
                            icon="mdi-arrow-up"
                            size="small"
                            @click="moveUpClick($event, index)"
                          />
                        </template>
                        {{ translatedLabels.moveUp }}
                      </VTooltip>

                      <VTooltip bottom>
                        <template v-slot:activator="{ props }">
                          <VBtn
                            v-bind="props"
                            fab
                            text
                            elevation="0"
                            small
                            class="v-expansion-panel-title__icon"
                            :aria-label="translatedLabels.moveDown"
                            :disabled="index >= dataLength - 1 || !control.enabled"
                            :class="styles.arrayList.itemMoveDown"
                            icon="mdi-arrow-down"
                            size="small"
                            @click="moveDownClick($event, index)"
                          />
                        </template>
                        {{ translatedLabels.moveDown }}
                      </VTooltip>

                      <VTooltip bottom>
                        <template v-slot:activator="{ props }">
                          <VBtn
                            v-bind="props"
                            fab
                            text
                            elevation="0"
                            small
                            class="v-expansion-panel-title__icon"
                            :aria-label="translatedLabels.delete"
                            :class="styles.arrayList.itemDelete"
                            :disabled="
                              !control.enabled
                                || (appliedOptions.restrict
                                  && arraySchema !== undefined
                                  && arraySchema.minItems !== undefined
                                  && dataLength <= arraySchema.minItems)
                            "
                            icon="mdi-delete"
                            size="small"
                            @click.stop="suggestToDelete = index"
                          />
                        </template>
                        {{ translatedLabels.delete }}
                      </VTooltip>
                    </VCol>
                  </VRow>
                </VContainer>
              </VExpansionPanelTitle>
              <VExpansionPanelText :class="styles.arrayList.itemContent">
                <DispatchRenderer
                  :schema="control.schema"
                  :uischema="foundUISchema"
                  :path="composePaths(control.path, `${index}`)"
                  :enabled="control.enabled"
                  :renderers="control.renderers"
                  :cells="control.cells"
                />
              </VExpansionPanelText>
            </VExpansionPanel>
          </VExpansionPanels>
        </VRow>
      </VContainer>
      <VContainer v-if="dataLength === 0" :class="styles.arrayList.noData">
        No data
      </VContainer>
    </VCardText>
    <VCardActions v-if="$slots.actions" class="pb-8">
      <slot
        name="actions"
        :labels="translatedLabels"
        :addClass="styles.arrayList.addButton"
        :addDisabled="addDisabled"
        :addClick="addButtonClick"
        :control="control"
        :appliedOptions="appliedOptions"
        :styles="styles"
      />
    </VCardActions>
    <VDialog
      :model-value="suggestToDelete !== null"
      max-width="600"
      @keydown.esc="suggestToDelete = null"
      @click:outside="suggestToDelete = null"
    >
      <VCard>
        <VCardTitle class="text-h5">
          {{ translatedLabels.dialogTitle }}
        </VCardTitle>

        <VCardText> {{ translatedLabels.dialogText }} </VCardText>

        <VCardActions>
          <VSpacer />

          <VBtn text @click="suggestToDelete = null">
            {{ translatedLabels.dialogCancel }}
          </VBtn>
          <VBtn
            text
            @click="
              removeItemsClick(suggestToDelete === null ? null : [suggestToDelete]);
              suggestToDelete = null;
            "
          >
            {{ translatedLabels.dialogConfirm }}
          </VBtn>
        </VCardActions>
      </VCard>
    </VDialog>
  </VCard>
</template>

<style scoped>
.notranslate {
  transform: none !important;
}
:deep(.v-toolbar__content) {
  padding-left: 0;
}
.v-expansion-panel-title {
  padding: 0px 15px;
}
</style>

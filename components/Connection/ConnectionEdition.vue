<script setup lang="ts">
// ----------------------------------------------------------------
// Import
// ----------------------------------------------------------------

import {
  type Connector,
  useConnectionStore,
} from "@/stores/useConnectionStore";

// ----------------------------------------------------------------
// PROPS
// ----------------------------------------------------------------

const props = defineProps({
  connectionEditionDialog: {
    type: Boolean,
    required: true,
  },
  selectedConnectionID: {
    type: String,
    default: "",
  },
});

// ----------------------------------------------------------------
// Emits
// ----------------------------------------------------------------

const emits = defineEmits([
  "connectionEditionDialog",
  "selectedConnectionID",
  "newConnectionCreated",
]);

// ----------------------------------------------------------------
// Variables
// ----------------------------------------------------------------

const connectionStore = useConnectionStore();

const drawer = ref(false);

const stepperItems = ["Choose a connector", "Connect"];
const step = ref(1);
const steps = 2;

const connectors = ref<Array<Connector>>([]);
const selectedConnector = ref<any>(null);
const expandedConnector = ref("");
const selectedConnection = ref<any>(null);

const sharedFields = ref({});
const privateFields = ref({});

const sharedFieldSchemas = ref({});
const privateFieldSchemas = ref({});

const connections = ref([]);

const loadingConnection = ref(false);

const validationErrors = ref([]);
const invalidCount = ref(0);

const errorMessageSnackbar = reactive({
  show: false,
  message: "",
});

// ----------------------------------------------------------------
// Watch
// ----------------------------------------------------------------

watch(
  () => props.connectionEditionDialog,
  async (newValue) => {
    if (newValue) {
      if (props.selectedConnectionID) {
        selectedConnection.value = connectionStore.connectionMap.get(
          props.selectedConnectionID,
        );
        selectedConnector.value = selectedConnection.value.connector;
        step.value = 2;
      }
      drawer.value = newValue;
    }
  },
);

// ----------------------------------------------------------------
// Lifecycle
// ----------------------------------------------------------------

onMounted(async () => {
  if (connectionStore.connectorMap.size === 0) {
    await connectionStore.fetchConnectors();
  }
  connectors.value = Object.values(
    Object.fromEntries(connectionStore.connectorMap),
  );
});

// ----------------------------------------------------------------
// Functions
// ----------------------------------------------------------------

/**
 * Closes the dialog, resets all relevant states and fields and emits the
 * "connectionEditionDialog" event to notify that the dialog has been closed.
 */
function close() {
  drawer.value = false;
  clearStates();
  emits("connectionEditionDialog", false);
  emits("selectedConnectionID", "");
}

/**
 * Resets all the states and fields to their initial values.
 *
 * This function is used to clear the state when closing the dialog or resetting the form.
 * It resets the stepper to the first step, clears the connections array and flags,
 * resets the selected connector and connection ID, clears shared and private fields,
 * resets the path, clears external data and loading flags, and resets error messages.
 */
function clearStates() {
  connections.value = [];

  selectedConnector.value = null;
  expandedConnector.value = "";
  selectedConnection.value = null;

  sharedFields.value = {};
  privateFields.value = {};

  sharedFieldSchemas.value = {};
  privateFieldSchemas.value = {};

  loadingConnection.value = false;

  validationErrors.value = [];
  invalidCount.value = 0;

  step.value = 1;
}

function expandConnector(connector) {
  if (expandedConnector.value === connector.id) {
    expandedConnector.value = "";
  } else {
    expandedConnector.value = connector.id;
  }
}

function selectConnector(connector: Connector) {
  step.value = 2;
  selectedConnector.value = connector;
}

/**
 * Establishes a connection by either updating an existing connection or creating a new one.
 */
async function connect() {
  loadingConnection.value = true;
  try {
    // Check if a connection already exists
    if (props.selectedConnectionID) {
      // If a connection exists, update it
      await updateConnection();
    } else {
      // If no connection exists, create a new one
      await createConnection();
    }
  } catch (error) {
    loadingConnection.value = false;
    errorMessageSnackbar.message = errorMessageSnackbar.message = error
      ?.response
      ._data
      .details[0]
      .message
      ? error.response._data.details[0].message
      : "An error occurred, please try again.";
    errorMessageSnackbar.show = true;
  }
  loadingConnection.value = false;
  await connectionStore.fetchConnections();
  if (!errorMessageSnackbar.show) {
    close();
  }
}

/**
 * Updates an existing connection with new private fields
 * by calling the `updateConnection` method from the `connectionStore`.
 */
async function updateConnection() {
  const privateParams = Object.assign(
    {},
    ...Object.values(privateFields.value),
  );
  await connectionStore.updateConnection(
    selectedConnection.value.id,
    privateParams,
  );
}

/**
 * Creates a new connection using the selected connector and the provided shared and private parameters
 * by calling the `createConnection` method from the `connectionStore`.
 */
async function createConnection() {
  await connectionStore
    .createConnection(
      selectedConnector.value.id,
      Object.assign({}, ...Object.values(sharedFields.value)), // Shared fields
      Object.assign({}, ...Object.values(privateFields.value)), // Private fields
    )
    .then(() => {
      emits("newConnectionCreated");
    })
    .catch((error) => {
      throw error;
    });
}
</script>

<template>
  <v-dialog
    v-model="drawer"
    persistent
    width="100%"
    height="100%"
    style="max-width: 1000px !important; max-height: 800px"
  >
    <v-card height="100%">
      <v-card-title class="d-flex">
        <v-sheet
          class="me-auto"
          style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis"
        >
          <b>{{
            $t(
              props.selectedConnectionID === ""
                ? "add_connection"
                : "edit_connection",
            )
          }}</b>
        </v-sheet>
        <v-sheet>
          <v-btn
            class="ms-2"
            icon="$close"
            density="comfortable"
            variant="plain"
            @click="close"
          />
        </v-sheet>
      </v-card-title>
      <v-card-text>
        <v-stepper
          v-model="step"
          elevation="0"
          height="100%"
          hide-actions
        >
          <template v-slot:default>
            <v-stepper-header
              :style="props.selectedConnectionID === '' ? '' : 'display: none'"
            >
              <template v-for="n in steps" :key="`${n}-step`">
                <v-stepper-item
                  :complete="step > n"
                  step="Step {{ n }}"
                  :title="stepperItems[n - 1]"
                  :value="n"
                />

                <v-divider v-if="n !== steps" :key="n" />
              </template>
            </v-stepper-header>

            <v-stepper-window>
              <v-stepper-window-item
                v-for="n in steps"
                :key="`${n}-content`"
                :value="n"
              >
                <v-card
                  height="90%"
                  style="max-height: calc(100vh - 200px) !important"
                >
                  <template v-if="step === 1">
                    <v-col class="fill-height">
                      <h3>{{ $t("create_a_connection") }}</h3>
                      <v-skeleton-loader
                        v-if="connectors.length === 0"
                        type="card"
                      />
                      <div v-else class="mt-3">
                        <v-row>
                          <v-col
                            v-for="(connector, index) in connectors"
                            :key="index"
                            :cols="expandedConnector === connector.id ? 6 : 3"
                          >
                            <v-card
                              class="pl-3 pb-1"
                              link
                              :ripple="false"
                              :height="
                                expandedConnector === connector.id
                                  ? 'auto'
                                  : '70px'
                              "
                            >
                              <div
                                class="d-flex justify-space-between align-start"
                              >
                                <v-sheet
                                  class="d-flex flex-grow-1"
                                  height="70"
                                  @click="selectConnector(connector)"
                                >
                                  <img
                                    class="align-self-center me-4"
                                    :src="connector.icon"
                                    alt=""
                                    style="max-width: 60px; max-height: 40px"
                                  >
                                  <p class="align-self-center">
                                    {{
                                      expandedConnector === connector.id
                                        ? connector.name
                                        : truncateTitle(connector.name, 8)
                                    }}
                                  </p>
                                </v-sheet>
                                <v-card-actions>
                                  <v-btn
                                    :icon="
                                      expandedConnector === connector.id
                                        ? 'mdi-arrow-collapse'
                                        : 'mdi-arrow-expand'
                                    "
                                    variant="plain"
                                    :ripple="false"
                                    size="small"
                                    @click="expandConnector(connector)"
                                  />
                                </v-card-actions>
                              </div>
                              <v-expand-transition>
                                <div
                                  v-if="expandedConnector === connector.id"
                                  class="text-body-2"
                                  @click="selectConnector(connector)"
                                >
                                  <em>{{ connector.description }}</em>
                                </div>
                              </v-expand-transition>
                            </v-card>
                          </v-col>
                        </v-row>
                      </div>
                    </v-col>
                  </template>
                  <template v-if="step === 2">
                    <v-row class="fill-height">
                      <v-col class="fill-height d-flex flex-column">
                        <div
                          class="mb-auto"
                          style="max-height: 500px; overflow-y: auto"
                        >
                          <h3>
                            {{ $t("complete_form_to_connect") }}
                            {{ selectedConnector.name }}
                          </h3>
                          <br>
                          <ConnectionFields
                            v-model:currentConnector="selectedConnector"
                            v-model:currentConnection="selectedConnection"
                            v-model:sharedFields="sharedFields"
                            v-model:privateFields="privateFields"
                            v-model:sharedFieldSchemas="sharedFieldSchemas"
                            v-model:privateFieldSchemas="privateFieldSchemas"
                            v-model:validationErrors="validationErrors"
                            v-model:invalidCount="invalidCount"
                            :readOnlySharedFields="
                              selectedConnectionID === '' ? false : true
                            "
                          />
                        </div>
                        <v-sheet class="d-flex justify-end mt-4">
                          <v-btn
                            v-if="props.selectedConnectionID === ''"
                            class="ma-2"
                            width="150"
                            @click="clearStates"
                          >
                            Back
                          </v-btn>
                          <v-tooltip bottom :disabled="invalidCount === 0">
                            <template v-slot:activator="{ props: slotProps }">
                              <div v-bind="slotProps" class="d-inline-block">
                                <v-btn
                                  :loading="loadingConnection"
                                  :disabled="invalidCount > 0"
                                  class="ma-2"
                                  width="150"
                                  @click="connect()"
                                >
                                  Connect
                                </v-btn>
                              </div>
                            </template>
                            <span>Shared and Private parameters are required.</span>
                          </v-tooltip>
                        </v-sheet>
                      </v-col>

                      <v-divider class="mx-4" vertical />

                      <v-col cols="3" class="d-flex flex-column fill-height">
                        <v-sheet class="d-flex" height="70">
                          <img
                            class="align-self-center me-4"
                            :src="selectedConnector.icon"
                            alt=""
                            style="height: 30px"
                          >
                          <p class="align-self-center">
                            {{ selectedConnector.name }}
                          </p>
                        </v-sheet>
                        <p>{{ selectedConnector.description }}</p>
                        <p>
                          For further information please refer to the
                          <a
                            :href="selectedConnector.documentation"
                            target="_blank"
                            rel="noopener noreferrer"
                          >
                            {{ selectedConnector.name }} documentation</a>.
                        </p>
                      </v-col>
                    </v-row>
                    <v-snackbar
                      v-model="errorMessageSnackbar.show"
                      :attach="true"
                      :timeout="5000"
                      color="error"
                    >
                      {{ errorMessageSnackbar.message }}
                      <template v-slot:action="{ attrs }">
                        <v-btn
                          color="white"
                          text
                          v-bind="attrs"
                          @click="errorMessageSnackbar.show = false"
                        >
                          Close
                        </v-btn>
                      </template>
                    </v-snackbar>
                  </template>
                </v-card>
              </v-stepper-window-item>
            </v-stepper-window>
          </template>
        </v-stepper>
      </v-card-text>
    </v-card>
  </v-dialog>
</template>

<style>
.v-stepper-header {
  box-shadow: none !important;
}
.divider {
  border-left: 1px solid #ccc;
  height: 100%;
}
.vertical-line {
  width: 2px;
  height: 100%;
  background-color: #ccc;
}

.center-content {
  margin: auto;
}
</style>

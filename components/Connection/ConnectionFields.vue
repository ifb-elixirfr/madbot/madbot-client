<script setup lang="ts">
// ----------------------------------------------------------------
// Import
// ----------------------------------------------------------------

import type { Connection, Connector } from "@/stores/useConnectionStore";

import AnyOfControlRenderer from "@/components/JsonFormRenderers/AnyOfRenderer.vue";
import ArrayControlRenderer from "@/components/JsonFormRenderers/ArrayControlRenderer.vue";
import ArrayLayoutRenderer from "@/components/JsonFormRenderers/ArrayLayoutRenderer.vue";
import OneOfControlRenderer from "@/components/JsonFormRenderers/OneOfRenderer.vue";
import {
  isAnyOfControl,
  isObjectArrayControl,
  isObjectArrayWithNesting,
  isOneOfControl,
  isPrimitiveArrayControl,
  or,
} from "@jsonforms/core";
import { type JsonFormsRendererRegistryEntry, rankWith, type Tester } from "@jsonforms/core";
import { JsonForms } from "@jsonforms/vue";
import { vuetifyRenderers } from "@jsonforms/vue-vuetify";
import { markRaw } from "vue";

// ----------------------------------------------------------------
// Model
// ----------------------------------------------------------------

const currentConnector = defineModel<Connector>("currentConnector", { required: true });
const currentConnection = defineModel<Connection>("currentConnection", { required: false });

const expandablePrivateParams = defineModel<boolean>("expandablePrivateParams", { default: false });

// fields
const sharedFields = defineModel<object>("sharedFields", { required: true });
const privateFields = defineModel<object>("privateFields", { required: true });

const readOnlySharedFields = defineModel<boolean>("readOnlySharedFields", { default: false });
const readOnlyPrivateFields = defineModel<boolean>("readOnlyPrivateFields", { default: false });

// schemas
const sharedFieldSchemas = defineModel<object>("sharedFieldSchemas", { required: true });
const privateFieldSchemas = defineModel<object>("privateFieldSchemas", { required: true });

// validation errors
const validationErrors = defineModel<Array<object>>({
  default: () => [],
});
const invalidCount = defineModel<number>("invalidCount", { default: 0 });

const changesMade = defineModel<boolean>("changesMade", { default: false });

// ----------------------------------------------------------------
// Variables
// ----------------------------------------------------------------

const editingPrivateFields = ref(false);
const jsonformsInitializedFields = ref<Array<string>>([]);

const prefilledSharedFields = ref<object>({});

const loading = ref(true);

// ----------------------------------------------------------------
// AJV Initialization
// ----------------------------------------------------------------

let ajv = newAjv();

const defaultConfigOptions = {
  showUnfocusedDescription: true,
};

function buildRendererRegistryEntry(testRenderer: any, controlType: Tester) {
  const entry: JsonFormsRendererRegistryEntry = {
    renderer: testRenderer,
    tester: rankWith(3, controlType),
  };
  return entry;
}

const OneOfRendererEntry = buildRendererRegistryEntry(OneOfControlRenderer, isOneOfControl);
const AnyOfRendererEntry = buildRendererRegistryEntry(AnyOfControlRenderer, isAnyOfControl);

const ArrayControlEntry = buildRendererRegistryEntry(
  ArrayControlRenderer,
  or(isObjectArrayControl, isPrimitiveArrayControl),
);
const ArrayLayoutEntry = buildRendererRegistryEntry(ArrayLayoutRenderer, isObjectArrayWithNesting);

const customEntryNames = [
  "one-of-select-renderer",
  "array-control-renderer",
  "array-layout-renderer",
  "any-of-select-renderer",
];

const renderers = [
  markRaw(OneOfRendererEntry),
  markRaw(ArrayControlEntry),
  markRaw(ArrayLayoutEntry),
  markRaw(AnyOfRendererEntry),
];

for (const key in vuetifyRenderers) {
  if (!customEntryNames.includes(vuetifyRenderers[key].renderer.name)) {
    renderers.push(markRaw(vuetifyRenderers[key]));
  }
}

// ----------------------------------------------------------------
// Lifecycle
// ----------------------------------------------------------------

onMounted(async () => {
  loading.value = true;
  if (currentConnection.value !== null && currentConnection.value !== undefined) {
    prefilledSharedFields.value = currentConnection.value.sharedParams.reduce((arr, field) => {
      arr[field.key] = { [field.key]: field.value };
      return arr;
    }, {});
  }

  await loadParameters(currentConnector, prefilledSharedFields.value);
  loading.value = false;
});

// ----------------------------------------------------------------
// Functions
// ----------------------------------------------------------------

/**
 * Initializes the shared and private fields based on the provided connector object.
 *
 * If the `sharedParameters` parameter is present then prefill the shared fields with the provided values.
 *
 * @param {object} connector - The connector object containing fields to be initialized.
 * @param {object} sharedParameters - An optional array of prefilled shared parameters to initialize the shared fields.
 */
async function loadParameters(connector, sharedParameters = {}) {
  ajv = newAjv();

  if ("shared" in connector.value.fields) {
    for (const fieldURL of connector.value.fields.shared) {
      const { data: schema } = await useAPIFetch(fieldURL);
      const schemaName = fieldURL.substring(fieldURL.lastIndexOf("/") + 1);

      sharedFields.value[fieldURL]
        = sharedParameters[schemaName] !== undefined ? sharedParameters[schemaName] : {};
      sharedFieldSchemas.value[fieldURL] = schema.value;
    }
  }
  if ("private" in connector.value.fields) {
    for (const fieldURL of connector.value.fields.private) {
      const { data: schema } = await useAPIFetch(fieldURL);

      privateFields.value[fieldURL] = {};
      privateFieldSchemas.value[fieldURL] = schema.value;
    }
  }
}

function updateFieldValue(event, access, key) {
  if (event.data !== null && event.data !== undefined) {
    ajv = newAjv();

    let schema;
    let shouldValidate;
    if (access === "shared") {
      // record if changes has been made
      if (
        !changesMade.value
        && sharedFields.value[key] !== event.data
        && jsonformsInitializedFields.value.includes(key)
      ) {
        changesMade.value = true;
      }

      // update the share fields list
      sharedFields.value[key] = event.data;

      // should the value be checked by AJV
      shouldValidate = !readOnlySharedFields.value;

      // save the schema for validation
      schema = sharedFieldSchemas.value[key];
    } else if (access === "private") {
      // record if changes has been made
      if (
        !changesMade.value
        && privateFields.value[key] !== event.data
        && jsonformsInitializedFields.value.includes(key)
      ) {
        changesMade.value = true;
      }

      // update the private fields list
      privateFields.value[key] = event.data;

      // should the value be checked by AJV
      shouldValidate = !readOnlyPrivateFields.value;

      // save the schema for validation
      schema = privateFieldSchemas.value[key];
    }
    // add the field to the initialized fields, if not already initialized
    if (!jsonformsInitializedFields.value.includes(key)) jsonformsInitializedFields.value.push(key);

    if (shouldValidate) {
      const ajvValidator = newAjv();
      const validate = ajvValidator.compile(schema);
      const valid = validate(event.data);

      if (!valid) {
        // if there are any validation error
        const fieldErrors = validate.errors.map(error => ({
          ...error,
          setting: key,
        }));

        // Accumulate validation errors for all settings
        validationErrors.value = [
          ...validationErrors.value.filter(error => error.setting !== key),
          ...fieldErrors,
        ];
      } else if (Object.values(event.data)[0] === "") {
        // we consider that every connection parameter (shared or private) is required and must not a an empty string
        validationErrors.value = [
          ...validationErrors.value.filter(error => error.setting !== key),
          { data: Object.values(event.data)[0], message: "must not be empty", setting: key },
        ];
      } else {
        // Remove previous errors for this field if validation passes
        validationErrors.value = validationErrors.value.filter(error => error.setting !== key);
      }

      const invalidCountPerField = validationErrors.value
        .map(({ setting }) => setting)
        .reduce((settings, setting) => {
          const count = settings[setting] || 0;
          settings[setting] = count + 1;
          return settings;
        }, {});

      invalidCount.value = Object.keys(invalidCountPerField).length;
    }
  }
}
</script>

<template>
  <div class="mb-auto">
    <v-skeleton-loader v-if="loading" type="article" />
    <div v-else>
      <div v-if="Object.keys(sharedFieldSchemas).length !== 0">
        <h4>
          {{ $t("shared_parameters") }}
          <v-tooltip location="end">
            <template v-slot:activator="{ props }">
              <div v-bind="props" class="d-inline-block">
                <v-icon style="font-size: 1.4rem" v-bind="props">
                  mdi-information
                </v-icon>
              </div>
            </template>
            <span>
              Shared parameters represent the essential information required to establish a
              connection.
            </span>
          </v-tooltip>
        </h4>
        <div v-for="(schema, key, index) of sharedFieldSchemas" :key="index">
          <JsonForms
            :data="sharedFields[key]"
            :schema="schema"
            :renderers="renderers"
            :ajv="ajv"
            :config="defaultConfigOptions"
            :readonly="readOnlySharedFields"
            @change="(event) => updateFieldValue(event, 'shared', key)"
          />
        </div>
      </div>

      <v-card v-if="Object.keys(privateFieldSchemas).length !== 0" elevation="0">
        <div class="d-flex justify-space-between">
          <h4>
            {{ $t("private_parameters") }}
            <v-tooltip location="end">
              <template #activator="{ props }">
                <div v-bind="props" class="d-inline-block">
                  <v-icon style="font-size: 1.4rem" v-bind="props">
                    mdi-information
                  </v-icon>
                </div>
              </template>
              <span>
                Private parameters are crucial for establishing a connection. They include sensitive
                information required for authentication and access,<br>
                they are kept confidential and securely handled.
              </span>
            </v-tooltip>
          </h4>
          <div
            v-if="
              currentConnection?.credential !== null
                && currentConnection?.credential?.id !== null
                && !readOnlyPrivateFields
                && expandablePrivateParams
            "
          >
            <v-btn
              :append-icon="editingPrivateFields ? 'mdi-chevron-up' : 'mdi-chevron-down'"
              variant="text"
              :ripple="false"
              @click="editingPrivateFields = !editingPrivateFields"
            >
              Update
            </v-btn>
          </div>
        </div>
        <v-expand-transition>
          <div
            v-if="
              !(
                currentConnection?.credential !== null && currentConnection?.credential?.id !== null
              )
                || editingPrivateFields
                || !expandablePrivateParams
            "
          >
            <div v-for="(schema, key, index) of privateFieldSchemas" :key="index">
              <JsonForms
                :data="privateFields[key]"
                :schema="schema"
                :renderers="renderers"
                :ajv="ajv"
                :config="defaultConfigOptions"
                :readonly="readOnlyPrivateFields"
                @change="(event) => updateFieldValue(event, 'private', key)"
              />
            </div>
          </div>
        </v-expand-transition>
      </v-card>
    </div>
  </div>
</template>

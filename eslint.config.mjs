import antfu from "@antfu/eslint-config";

export default antfu({
  typescript: true,
  vue: true,
  formatters: {
    css: true,
    html: true,
    json: true,
    markdown: true,
    toml: true,
    yaml: true,
  },
  stylistic: {
    indent: 2,
    quotes: "double",
    semi: true,
  },
  rules: {
    "perfectionist/sort-imports": "off",
    "antfu/if-newline": "off",
    "arrow-body-style": [
      "error",
      "as-needed",
      { requireReturnForObjectLiteral: false },
    ],
    "camelcase": ["warn"],
    "func-style": [
      "error",
      "declaration",
      { allowArrowFunctions: true },
    ],
    "import/no-namespace": "error",
    "max-depth": ["error", 4],
    "object-shorthand": "off",
    "prefer-arrow-callback": [
      "error",
      { allowNamedFunctions: true },
    ],
    "style/brace-style": [
      "error",
      "1tbs",
      {
        allowSingleLine: true,
      },
    ],
    "vue/attribute-hyphenation": "off",
    "vue/brace-style": [
      "error",
      "1tbs",
      {
        allowSingleLine: true,
      },
    ],
    "vue/component-name-in-template-casing": ["error", "PascalCase"],
    "vue/max-attributes-per-line": [
      "error",
      {
        multiline: {
          max: 1,
        },
        singleline: {
          max: 2,
        },
      },
    ],
    "vue/mustache-interpolation-spacing": [
      "warn",
      "always",
    ],
    "vue/no-async-in-computed-properties": "error",
    "vue/no-deprecated-router-link-tag-prop": "off",
    "vue/no-deprecated-vue-config-keycodes": "off",
    "vue/no-dupe-keys": "error",
    "vue/no-mutating-props": "error",
    "vue/no-parsing-error": "error",
    "vue/no-side-effects-in-computed-properties": "error",
    "vue/no-spaces-around-equal-signs-in-attribute": ["error"],
    "vue/no-unused-components": "error",
    "vue/no-unused-vars": "error",
    "vue/no-use-v-if-with-v-for": "error",
    "vue/no-v-html": "warn",
    "vue/object-shorthand": "off",
    "vue/one-component-per-file": "off",
    "vue/require-default-prop": "off",
    "vue/require-prop-types": "error",
    "vue/require-v-for-key": "error",
    "vue/return-in-computed-property": "error",
    "vue/v-on-event-hyphenation": "off",
    "vue/v-slot-style": "off",
    "vue/valid-v-else": "error",
    "vue/valid-v-else-if": "error",
    "vue/valid-v-if": "error",
  },
});

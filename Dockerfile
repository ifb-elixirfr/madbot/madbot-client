FROM node:20.17.0-alpine

COPY . /opt/madbot
RUN cd /opt/madbot && yarn install && yarn build
RUN chmod 0755 /opt/madbot/entrypoint.sh

EXPOSE 3000

ENTRYPOINT ["/opt/madbot/entrypoint.sh"]

import process from "node:process";

import pkg from "./package.json";

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  app: {
    head: {
      title: "Madbot",
      meta: [
        {
          name: "description",
          content:
            "Madbot (Metadata And Data Brokering Online Tool) is a web application that provides a dashboard for managing research data and metadata. Its philosophy is to aggregate metadata around scientific projects by creating links, via connectors, to where the data is stored without storing the datasets themselves.",
        },
      ],
      link: [{ rel: "icon", type: "image/png", href: "/img/logo.png" }],
    },
  },

  runtimeConfig: {
    secret: process.env.NUXT_SECRET,
    oidcWellKnown: process.env.NUXT_OIDC_WELL_KNOWN,
    oidcClientSecret: process.env.NUXT_OIDC_CLIENT_SECRET,
    public: {
      clientVersion: pkg.version,
      madbotApiURL: process.env.NUXT_PUBLIC_MADBOT_API_URL,
      oidcClientID: process.env.NUXT_PUBLIC_OIDC_CLIENT_ID,
    },
  },

  css: [
    "vuetify/lib/styles/main.sass",
    "@mdi/font/css/materialdesignicons.min.css",
    "@fortawesome/fontawesome-free/css/all.css",
    "@/assets/css/madbot.css",
    "@/assets/css/nerdfonts.css",
  ],

  build: {
    transpile: [
      "vuetify",
      "vue-sonner",
      "echarts",
      "zrender",
      "tslib",
      "vue-echarts",
      "resize-detector",
    ],
    transpileDependencies: ["@jsonforms/core", "@jsonforms/vue", "@jsonforms/vue-vuetify"],
  },

  vite: {
    define: {
      "process.env.DEBUG": false,
    },
  },

  modules: ["@sidebase/nuxt-auth", "@pinia/nuxt", "@dargmuesli/nuxt-cookie-control", "@vueuse/nuxt"],

  cookieControl: {
    isControlButtonEnabled: false,
    isAcceptNecessaryButtonEnabled: false,
    cookies: {
      necessary: [
        {
          description: {
            en: "This website requires cookies, and the limited processing of your personal data in order to function.",
            fr: "Ce site web nécessite des cookies et le traitement limité de vos données personnelles pour fonctionner.",
          },
          id: "n",
          name: {
            en: "Necessary Cookie",
            fr: "Cookie nécessaire",
          },
        },
      ],
      optional: [],
    },
    locales: ["en", "fr"],
  },

  auth: {
    globalAppMiddleware: {
      isEnabled: true,
    },
    origin: process.env.ORIGIN,
    defaultProvider: "madbot",
  },

  routeRules: {
    "/todo": { ssr: false },
    "/index": { ssr: false },
    "/first": { ssr: false },
    "/": { ssr: false },
    "/*/*": { ssr: false },
    "/*/*/*": { ssr: false },
  },

  experimental: {
    clientNodeCompat: true,
  },

  compatibilityDate: "2024-10-02",
});

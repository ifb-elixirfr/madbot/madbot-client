# Changelog

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.0-dev.143](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.142...v1.0.0-dev.143) (2025-03-10)

### Features

* add metadataField Source ([5d48225](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/5d4822577253e3788bedc909487230d18ea8bf7d))

## [1.0.0-dev.142](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.141...v1.0.0-dev.142) (2025-03-06)

### Bug Fixes

* remove the external field and fix the mapping ([c40a349](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/c40a349fbfdf7f89d7afd8f9b161d02395763838))

## [1.0.0-dev.141](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.140...v1.0.0-dev.141) (2025-03-05)

### Features

* use actions endpoint for submissions metadata generation ([d3e9f4e](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/d3e9f4e2b5d5c3423954ea55804ae7e428d0c8c1))

## [1.0.0-dev.140](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.139...v1.0.0-dev.140) (2025-03-04)

### Features

* handle errors in metadata generation ([8668d81](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/8668d8168d3649615aeb46632b2dd3aa83e92940))

## [1.0.0-dev.139](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.138...v1.0.0-dev.139) (2025-03-03)

### Features

* hide menu section that add metadata by mapping ([5fdb16b](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/5fdb16b43efc9850f7292cf1507441c897777090))

## [1.0.0-dev.138](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.137...v1.0.0-dev.138) (2025-02-28)

### Features

* Introduce a submission metadata component ([6e23805](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/6e238058f1bb63a1b5aea285b87819ed4695296b))

## [1.0.0-dev.137](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.136...v1.0.0-dev.137) (2025-02-27)

### Bug Fixes

* correct the readme ([7f8afcf](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/7f8afcfd24f6e631ed3a0eb4b75cef4c36f02322))

## [1.0.0-dev.136](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.135...v1.0.0-dev.136) (2025-02-19)

### Bug Fixes

* correct the schema url based on the new core structure ([8c83a03](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/8c83a03ca39dc6d0a1f3c9cf28b98366ce99566b))

## [1.0.0-dev.135](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.134...v1.0.0-dev.135) (2025-02-14)

### Features

* replace editorJS by Milkdown ([c2025d5](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/c2025d50c58294e6579d8b443e0320d5c18c072d))

## [1.0.0-dev.134](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.133...v1.0.0-dev.134) (2025-02-14)

### Features

* add a reload metadata generation button ([4e24bf7](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/4e24bf75c392a46c921667f0ddfc18bddc71539e))

## [1.0.0-dev.133](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.132...v1.0.0-dev.133) (2025-02-10)

### Features

* display metadata in metadata objects tabs ([090d83e](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/090d83e7136fed1fe0ecc529a1d2962c4fb97a77))

## [1.0.0-dev.132](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.131...v1.0.0-dev.132) (2025-02-10)

### Features

* multi selection data in data connector ([13666af](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/13666af46c4cdf3fbbac3c5206000e2ad891bb24))

## [1.0.0-dev.131](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.130...v1.0.0-dev.131) (2025-02-06)

### Features

* added "météo" to submission metadata objects tabs ([46be77f](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/46be77fd1c6bea390e0dbfc34cd614e11d1f2118))

## [1.0.0-dev.130](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.129...v1.0.0-dev.130) (2025-02-06)

### Features

* node description with editorjs ([6fa581e](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/6fa581ed558fa763e09f9a474db5c082ff6eb07e))

## [1.0.0-dev.129](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.128...v1.0.0-dev.129) (2025-02-06)

### Features

* Handle metadataObjects in submission store ([b4c9e03](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/b4c9e030403d677e62fd669ef550ce6f6609c426))
* submission websocket ([3fd91b9](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/3fd91b9945cedb0b512b25dfb6d998cabba53491))

## [1.0.0-dev.128](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.127...v1.0.0-dev.128) (2025-02-05)

### Features

* improve the renderers for the new schemas in referential ([bd40672](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/bd40672ba84bd2574c374d5bb43dcec707f4cb22))

## [1.0.0-dev.127](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.126...v1.0.0-dev.127) (2025-02-05)

### Features

* better handling of the submission status + added a reload button / spinning wheel ([e2b3e60](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/e2b3e60f1e6a7080f378fa7cc7c2e02bbbc88321))

## [1.0.0-dev.126](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.125...v1.0.0-dev.126) (2025-02-04)

### Features

* add generating metadata status ([a3eec6b](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/a3eec6b66f1c93a2ca2253bc5311c4d4c0816256))

## [1.0.0-dev.125](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.124...v1.0.0-dev.125) (2025-01-24)

### Features

* lock submission buttons when status is generating metadata ([b8b3f38](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/b8b3f3872634f49af379a871c6462f4f7b5f825f))

## [1.0.0-dev.124](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.123...v1.0.0-dev.124) (2025-01-22)

### Bug Fixes

* correct the url for the submission data ([7143612](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/7143612f2aa61d661049d56d42aee3f39c850525))

## [1.0.0-dev.123](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.122...v1.0.0-dev.123) (2025-01-20)

### Features

* create tabs from connector Metadata object types ([fcb1563](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/fcb156322cb851293b970bfac12e2557a44167ce))

## [1.0.0-dev.122](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.121...v1.0.0-dev.122) (2024-12-24)

### Features

* grey out files that are not supported ([1e8bd19](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/1e8bd19f08fe381917a3dd43ad84f1d2fcc37f6f))

## [1.0.0-dev.121](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.120...v1.0.0-dev.121) (2024-12-24)

### Features

* handle the ror in the contributing organization ([0381b1e](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/0381b1e9c47d2f0df2a6442e2985185c5bb4c176))

## [1.0.0-dev.120](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.119...v1.0.0-dev.120) (2024-12-24)

### Features

* handle settings submission ([5ef14bc](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/5ef14bc16995b810e96777b35f040b0bd3ad627f))

## [1.0.0-dev.119](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.118...v1.0.0-dev.119) (2024-12-20)

### Features

* add bearer to the copy clipboard ([96f876f](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/96f876fdb29d1a894f7598e514ebcf1fc0f8871e))

## [1.0.0-dev.118](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.117...v1.0.0-dev.118) (2024-12-19)

### Bug Fixes

* display settings value from submission parameters ([9eb479f](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/9eb479f3344ef71e2215605f061646f576165252))

## [1.0.0-dev.117](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.116...v1.0.0-dev.117) (2024-12-11)

### Features

* add a christmas and easter themes to madbot ([0a6cc56](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/0a6cc5605ab36936ec05b2e3a286f28c0d316a17))

## [1.0.0-dev.116](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.115...v1.0.0-dev.116) (2024-12-10)

### Features

* Create cookie banner ([3cc601c](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/3cc601c76b1d04c33a77797b5ff0756ceac7d283))

## [1.0.0-dev.115](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.114...v1.0.0-dev.115) (2024-11-28)

### Features

* responsive layout ([5124eab](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/5124eab27baa7f12d2a17c4027b3666af2ddea2c))

## [1.0.0-dev.114](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.113...v1.0.0-dev.114) (2024-11-26)

### Features

* control renderer orcid ([ad7178c](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/ad7178c504d0dee28e9850cb632c8ae6677a0ffa))

## [1.0.0-dev.113](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.112...v1.0.0-dev.113) (2024-11-25)

### Bug Fixes

* fix the oneOf renderer ([65dd80e](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/65dd80e866b6c1dbeda429c25a972315803d90a3))

## [1.0.0-dev.112](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.111...v1.0.0-dev.112) (2024-11-25)

### Features

* create new json forms renderers (doi, pubmed, ...) ([f344301](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/f344301e29793a6c111ba52df86cf5796cd019ec))

## [1.0.0-dev.111](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.110...v1.0.0-dev.111) (2024-11-22)

### Features

* handle conditions in the oneOf renderer ([9283d4f](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/9283d4f631ffcaaae8622bd791047fa168a52871))

## [1.0.0-dev.110](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.109...v1.0.0-dev.110) (2024-11-12)

### Features

* multi selection file in the file explorer ([a40ea7b](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/a40ea7be1775774428b93e2c6c01a4a1382ed0d3))

## [1.0.0-dev.109](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.108...v1.0.0-dev.109) (2024-11-06)

### Bug Fixes

* restrict some submission access based on access rights ([f85b377](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/f85b3775b7bd35666162e6d77962e68048942c5d))

## [1.0.0-dev.108](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.107...v1.0.0-dev.108) (2024-11-06)

### Features

* improve json form to manage additionnal metadata ([56ad057](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/56ad057e6ec3b4a67831cd7bf33d515e4da33dd2))

## [1.0.0-dev.107](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.106...v1.0.0-dev.107) (2024-11-05)

### Bug Fixes

* reverse the role and member selectors when adding members ([75bb9ac](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/75bb9acda412bf7fcdc6ed785a995350acb5638d))

## [1.0.0-dev.106](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.105...v1.0.0-dev.106) (2024-11-05)

### Bug Fixes

* better handle the server down when logging out ([4a0696a](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/4a0696a0272c2541f590359408acd4160cbcc63b))

## [1.0.0-dev.105](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.104...v1.0.0-dev.105) (2024-11-05)

### Features

* add data associations in the data object modal ([ee44f06](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/ee44f061dce57bbb94633a159ec3961e841b0442))

## [1.0.0-dev.104](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.103...v1.0.0-dev.104) (2024-11-04)

### Features

* create explore files explorer ([e0b5abc](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/e0b5abc57d0d40309f527082c3f0d0f7bc64f543))

## [1.0.0-dev.103](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.102...v1.0.0-dev.103) (2024-10-18)

### Bug Fixes

* display accurate datalink and sample counts from node object ([336df21](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/336df21ab248701520dc4e41dcb712121a1fe40a))

## [1.0.0-dev.102](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.101...v1.0.0-dev.102) (2024-10-18)

### Features

* display a sample in a modale and add options in samples list from a node ([ac528af](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/ac528afa83d3a3f157232a896e625c38359d2ebd))

## [1.0.0-dev.101](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.100...v1.0.0-dev.101) (2024-10-18)

### Bug Fixes

* **submission:** ensure updated submission title is used for deletion validation ([3081dbe](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/3081dbed396a0aebf35fa65860832ffa838c8c8b))

## [1.0.0-dev.100](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.99...v1.0.0-dev.100) (2024-10-17)

### Features

* **connection:** add check connection feature ([e17d971](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/e17d9719025de2b911b0a59696efec2cef66e328))

## [1.0.0-dev.99](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.98...v1.0.0-dev.99) (2024-10-17)

### Features

* display data access right ([2f009cf](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/2f009cffe30a1af791de08962ef5246046f7a318))

## [1.0.0-dev.98](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.97...v1.0.0-dev.98) (2024-10-15)

### Features

* add an empty tab for data association in the data object modal ([f5fcab9](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/f5fcab9feed21c6c44ebd7453c4154dc285bea68))

## [1.0.0-dev.97](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.96...v1.0.0-dev.97) (2024-10-14)

### Bug Fixes

* Add underline for titles of submission parameters ([58776f3](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/58776f35830327d465d2971dffa79fb630737d3f))

## [1.0.0-dev.96](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.95...v1.0.0-dev.96) (2024-10-14)

### Bug Fixes

* remove actions column from submissions list ([7d3e79e](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/7d3e79eca2710696133f3097a02507bd6d679995))

## [1.0.0-dev.95](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.94...v1.0.0-dev.95) (2024-10-14)

### Bug Fixes

* update packages and fix eslint warnings ([2ecacc1](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/2ecacc1ec4568610767a4fb26a2cd6417d0667ba))

## [1.0.0-dev.94](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.93...v1.0.0-dev.94) (2024-10-10)

### Bug Fixes

* **auth:** resolve duplicate auth handler setup ([88b0a19](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/88b0a19bf3babe80770fe84aaf5c0ce6b85e21d4))

## [1.0.0-dev.93](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.92...v1.0.0-dev.93) (2024-10-07)

### Bug Fixes

* capture schema already exists errors from Ajv ([cc344ca](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/cc344cabb48e0612ea923648893d2d42a79783db))

## [1.0.0-dev.92](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.91...v1.0.0-dev.92) (2024-10-02)

### Bug Fixes

* bug taxon schema ([a6c00d2](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/a6c00d2be4e357aba31cf07f7e1639c4ad40fc53))

## [1.0.0-dev.91](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.90...v1.0.0-dev.91) (2024-10-01)

### Features

* redirection if the server is unavailable using the health check ([71ae867](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/71ae8674261debfcf9ff2357fac003d6d218016c))

## [1.0.0-dev.90](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.89...v1.0.0-dev.90) (2024-10-01)

### Bug Fixes

* remove clearable from the oneOfEnum ([e736421](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/e7364214e2073972c5870893a9f7babe8c58432e))

## [1.0.0-dev.89](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.88...v1.0.0-dev.89) (2024-09-30)

### Bug Fixes

* resolve multiple bugs related to connection creation/edition ([a804822](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/a804822b697612c2b639181bc9e2c102ef7fb45a))

## [1.0.0-dev.88](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.87...v1.0.0-dev.88) (2024-09-27)

### Features

* new checklist renderer ([3f6dc3e](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/3f6dc3e389d9afe596e1a717cc08e62bebf114c4))

## [1.0.0-dev.87](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.86...v1.0.0-dev.87) (2024-09-27)

### Features

* handle the skip connection for the publication connectors ([b2f8b17](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/b2f8b176640276ccba740e018a0c9ca1abdad999))

## [1.0.0-dev.86](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.85...v1.0.0-dev.86) (2024-09-27)

### Bug Fixes

* validation errors on single parameters in the new datalink modal ([39ccbed](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/39ccbed2e2144eecc496ab5e6bd829dfc1a64002))

## [1.0.0-dev.85](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.84...v1.0.0-dev.85) (2024-09-26)

### Features

* handle the skip connection for the publication connectors ([cf411fd](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/cf411fd4101f67043202df5c6096dbb09dd06eaf))

## [1.0.0-dev.84](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.83...v1.0.0-dev.84) (2024-09-26)

### Bug Fixes

* redirection on unknown publication ([ba6419b](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/ba6419b42f30bd2df3f391aae2933b1da9aa2e9b))

## [1.0.0-dev.83](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.82...v1.0.0-dev.83) (2024-09-26)

### Features

* publication settings tab in the parameters modal ([f3c18b4](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/f3c18b44f3fa7a9a8e17322614c670c7a392b5f8))

## [1.0.0-dev.82](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.81...v1.0.0-dev.82) (2024-09-26)

### Features

* create the step 3 of publication creation ([b90b6a2](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/b90b6a2da696ee7c6ee89b0f5137bf9a254cdb62))

## [1.0.0-dev.81](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.80...v1.0.0-dev.81) (2024-09-25)

### Features

* create a publication parameters modal ([ca86c45](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/ca86c4591306087c472b1c45f8a6b780821dd1b9))

## [1.0.0-dev.80](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.79...v1.0.0-dev.80) (2024-09-20)

### Features

* improve data icon ([22dba7f](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/22dba7f2f9d546781df9a1887c0406d799a9859a))

## [1.0.0-dev.79](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.78...v1.0.0-dev.79) (2024-09-20)

### Features

* created a publication page ([fd8602d](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/fd8602dc8dae62fc682819337cbba2bb82dc955d))

## [1.0.0-dev.78](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.77...v1.0.0-dev.78) (2024-09-20)

### Features

* handle the display of the documentation in the publication connectors ([eb045bb](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/eb045bb5ef9ac27dd396cf9bcb7ea31bd68fabf6))

## [1.0.0-dev.77](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.76...v1.0.0-dev.77) (2024-09-20)

### Features

* **connection:** adapt the connection handling in workspace parameters ([807ae2a](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/807ae2abcf925382bb19caad0e0a31e828ebc20d))

## [1.0.0-dev.76](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.75...v1.0.0-dev.76) (2024-09-18)

### Features

* show existing connections in the new publication modal ([b617262](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/b6172623b216de311713b87cb3fe25687e77ef4c))

## [1.0.0-dev.75](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.74...v1.0.0-dev.75) (2024-09-17)

### Features

* add expand buttons on connectors in the new publication and new datalink modals ([dce2112](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/dce21122aca3f56c658b47f8f39c81b191e68e2a))

### Bug Fixes

* if metadata already exist, user shouldn't be able to select the corresponding metadata field ([03abfbd](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/03abfbd79235cce67f131e95b5953c24c5f0cf41))

## [1.0.0-dev.74](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.73...v1.0.0-dev.74) (2024-09-17)

### Features

* Add non functional actions delete and view buttons as actions in publication table ([fb34e89](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/fb34e8916fa70e4773c425eec330c06f63d5ee88))

## [1.0.0-dev.73](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.72...v1.0.0-dev.73) (2024-09-17)

### Features

* show publication connectors in the new publication modal ([6bd5848](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/6bd5848626785e247d4fc2e0f96295fa2b4ed3f0))

## [1.0.0-dev.72](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.71...v1.0.0-dev.72) (2024-09-17)

### Features

* add PublicationTable component ([7b9cb25](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/7b9cb25f1b72c6cb6784d3482b805d4c4aef8a36))

## [1.0.0-dev.71](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.70...v1.0.0-dev.71) (2024-09-17)

### Features

* create stepper for the new publication modal ([4c80dea](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/4c80deac8e72b3fd0191e2f8a4e1db28b69c283f))

## [1.0.0-dev.70](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.69...v1.0.0-dev.70) (2024-09-17)

### Bug Fixes

* fix import metadata by mapping ([ac5f5ed](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/ac5f5ed978d76b10ecca39414ea8a42c3c5e17b9))

## [1.0.0-dev.69](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.68...v1.0.0-dev.69) (2024-09-16)

### Features

* add button and blank modal to create a new publication ([a493c0e](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/a493c0e3b161e91909681590491c81e2d49a5177))

## [1.0.0-dev.68](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.67...v1.0.0-dev.68) (2024-09-16)

### Bug Fixes

* fix version of jsonform-vuetify ([8e5491c](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/8e5491cd992fa2c8e22d9e5048d9a6522ca3fb2e))

## [1.0.0-dev.67](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.66...v1.0.0-dev.67) (2024-09-16)

### Features

* create publication store ([e6c6e30](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/e6c6e305862c92ef8c5285b67ed1fa00bb8e9d79))

## [1.0.0-dev.66](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.65...v1.0.0-dev.66) (2024-09-16)

### Features

* create a publication menu and a publication page ([17ba056](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/17ba0561a8aed6fb2c72ce490bc010129036d58d))

## [1.0.0-dev.65](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.64...v1.0.0-dev.65) (2024-09-12)

### Features

* add the possibility to see the multiple inherited metadata values and solve metadata conflicts ([f36db54](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/f36db5473ac39ed6b9935add29c23af78218c9d0))

## [1.0.0-dev.64](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.63...v1.0.0-dev.64) (2024-09-12)

### Features

* improve sample creation by automatically fill the alias field ([221a4f1](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/221a4f117220f142fc48f3bdf00d4665454e165a))

## [1.0.0-dev.63](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.62...v1.0.0-dev.63) (2024-09-09)

### Features

* add a warning pop-up when the tab closes ([4ffe4a4](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/4ffe4a489beb4f65e82d77a257c107d3e3820156))

## [1.0.0-dev.62](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.61...v1.0.0-dev.62) (2024-09-06)

### Bug Fixes

* change localisation of redirection confirmation ([7e0aa46](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/7e0aa46626af90aee1187431201a66a4387c8f15))

## [1.0.0-dev.61](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.60...v1.0.0-dev.61) (2024-09-06)

### Bug Fixes

* optimize performance for adding multiple metadata entries ([4af188c](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/4af188c44ddc4e89e9353fa3140f747137888913))

## [1.0.0-dev.60](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.59...v1.0.0-dev.60) (2024-09-06)

### Bug Fixes

* resolve multiple bugs ([0ee8f55](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/0ee8f55e05414ce992f17dd617e6546d79b8e586))

## [1.0.0-dev.59](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.58...v1.0.0-dev.59) (2024-09-06)

### Bug Fixes

* Bug warning on save button when deleting a new metadata ([76ee932](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/76ee932db75dcd8f081d0357b773ae0f3773ca9d))

## [1.0.0-dev.58](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.57...v1.0.0-dev.58) (2024-09-04)

### Features

* When bounding a sample to a data, the sample and datalink node must have a direct parent-child relationship ([6ce6c14](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/6ce6c14c294b11287ea501c13835621a79d1ba53))

## [1.0.0-dev.57](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.56...v1.0.0-dev.57) (2024-09-03)

### Bug Fixes

* fix bugs  ([09b88df](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/09b88dfc8b700ae15141502f80f8320942bf994d))

## [1.0.0-dev.56](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.55...v1.0.0-dev.56) (2024-09-03)

### Features

* upgrade nuxt and vuetify versions ([167f3d5](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/167f3d566e153353b836c7a8265a1163897bb0f2))

### Bug Fixes

* resolve the display problem in viewtree ([d79bb54](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/d79bb54b8ef95a64666ef70af3541437034336e5))

## [1.0.0-dev.55](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.54...v1.0.0-dev.55) (2024-09-02)

### Features

* add a modal allowing the sample creation ([c36fbdb](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/c36fbdbb951b2c81a1d58588d7bd68764c7c8af2))
* add a query parameter for the current tab in nodes ([29a435b](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/29a435b4404108462ac732f16f351a7dc57f777a))
* add a search field ([c62bcc0](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/c62bcc053239b17746012a468eea174091bacce0))
* add a size parameter to the dataobject icon ([9d219a6](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/9d219a6b2823cd282a9b27677c94e7696626bb4f))
* Add metadata by mapping ([0773692](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/07736921124a1908f4f482310cbe8580d32af76b))
* Add Metadata table for data ([568a115](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/568a11559dc6c91740e2663421def17ffafaed73))
* add Metadata table for sample ([1d10475](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/1d10475c1ae083f5520c33010354eddac25e6cb8))
* add sample page ([3ff26b4](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/3ff26b4b8aef0ac68f09209c57325a7f1f6ebb2a))
* add the metadata table ([d95c951](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/d95c951b7977c8017439ed83ec15568db3c0ad6b))
* create a custom page when the current workspace is not found ([3a41114](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/3a41114c4a924a96a9394afaf6c356e5270bcb87))
* create an action menu to delete DLs ([7f3f463](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/7f3f4637fc8e8922f52a7d72e3a871bc0357e660))
* handle alias upon sample creation ([f9e5bf3](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/f9e5bf37f700c14fb75a13c129e7cd9e49909d40))
* let user explore, add and remove Data bound Samples ([b04903c](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/b04903cfb03cbe4bc473c534773573d2f88d53c5))
* metadata store ([cd2b997](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/cd2b997aa0b3653e3abdd78e38177336a9665577))
* modal to add metadata field ([a04fbac](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/a04fbac2c5d05a52fdf771ba73ed77cc2231b4ce))
* new way to display taxon ([60cd130](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/60cd1307d1f0c065edaa3fc5d4815f592fe48e9c))
* restrict the save button of the metadata ([05ca295](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/05ca295c357cd7abed5ef9ad64bb8510165336fa))
* Use the sample schema to validate the sample title and alias ([7a2e9c3](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/7a2e9c3c0b71b2a38c955662bd2c6bcc2afdcb5b))

### Bug Fixes

*  edit workspace name and check nodeid after workspace transition ([b903e5f](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/b903e5f64f5d71bd001e42a1edeae75c9e813d86))
* DataObject modal varying width ([8a25aba](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/8a25aba0ab95691050c8696e5fd6e4285705262f))
* generalize id used in the metadata table ([f71a25b](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/f71a25bae82623c6cfb2066c912e1d1055335faa))
* only display data connectors when manipulating datalinks ([2b4f3b0](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/2b4f3b024d973989146f0de9bbb14f03e757a334))
* prevent the datalinks array to have redundant datalinks by clearing the array ([09ff0d0](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/09ff0d03e6373ac76275b2124680d4e8840d1ffc))
* remove the scroll bar ([058fc07](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/058fc07314c0401f89653a558d86e89da85e3d80))
* resolve multiple bugs ([7efe54a](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/7efe54a89e154954bdfaaf4d8d294ba7d3d845c8))
* resolve multiple bugs ([4b6cbe8](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/4b6cbe8dad52cd1bd15d70289cda94b8938f0879))
* resolve multiple errors around the creation of datalink ([65ef2ea](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/65ef2ead433ba3077ad3e576a042acfa1d822957))
* Truncation of long titles ([40b5b7d](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/40b5b7dd034068c825d290a417461db5a5d5b24a))

## [1.0.0-dev.54](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.53...v1.0.0-dev.54) (2024-05-30)


### Features

* Create the sample tab ([7c49ce4](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/7c49ce49f49dd5ba6bcbe827698145ed1cda1a85))

## [1.0.0-dev.53](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.52...v1.0.0-dev.53) (2024-05-29)


### Features

* add the new store samples ([5b9be5a](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/5b9be5aea4182357d31872d70a9fc9601161e857))
* improve echarts imports ([a8b080f](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/a8b080f99ef9be1fe87df44dddce7432497e3ae0))
* improve user account section ([10bddfb](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/10bddfbb3214e2d591b95a6db156d2574a84d672))
* update nuxt, nuxt-auth, pinia and vuetify ([ae9ee58](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/ae9ee58176c09958640f778cd192c76b84008654))


### Bug Fixes

* adjust the node and workspaces roles ([5ee1463](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/5ee1463989293163a9a1ffc9176ed86bd77596fc))

## [1.0.0-dev.52](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.51...v1.0.0-dev.52) (2024-04-26)


### Features

* inform workspace owners that they are accessing a node because of the ownership rights ([67abf01](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/67abf011dcc413a668922c4196fe072a8dc4f31b))

## [1.0.0-dev.51](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.50...v1.0.0-dev.51) (2024-04-25)


### Features

* add members in batch from a node ([d4138e8](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/d4138e85e24c95fc1ab9204396e45b16f39ccca6))

## [1.0.0-dev.50](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.49...v1.0.0-dev.50) (2024-04-25)


### Features

* create first items in dashboard ([ee50daf](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/ee50dafbe25a3b2e17f11acad2d880f10be66bce))

## [1.0.0-dev.49](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.48...v1.0.0-dev.49) (2024-04-23)


### Features

* add websocket for the workspace ([6bacac1](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/6bacac1ecb8c904aa3b06be51ad9c0ac196a42e2))

## [1.0.0-dev.48](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.47...v1.0.0-dev.48) (2024-04-23)


### Bug Fixes

* workspace deletion message ([c5eb51e](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/c5eb51e5e01b323049deb8525ad4953041b1b4ef))

## [1.0.0-dev.47](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.46...v1.0.0-dev.47) (2024-04-22)


### Features

* add the workspace members' table ([e3e29a9](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/e3e29a995ddd634e64e3e82aa77de59bec71e6d4))

## [1.0.0-dev.46](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.45...v1.0.0-dev.46) (2024-04-22)


### Features

* add a loader on connections ([32b62d2](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/32b62d25edbaef7083d15486dc151336397152a7))

## [1.0.0-dev.45](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.44...v1.0.0-dev.45) (2024-04-19)


### Bug Fixes

* when adding a user on a root node, search between workspace members and not all user ([44b2e74](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/44b2e74549d70fb0384eb1f558659f02ee75770a))

## [1.0.0-dev.44](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.43...v1.0.0-dev.44) (2024-04-19)


### Features

* add user id in session data ([62e4ebc](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/62e4ebc59d3ddd28c3a992f3eee5d437daff5ea8))
* let workspace owners add themselves and modify their role as members of nodes ([f73a01a](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/f73a01a697e9bfb6630e9f843c317e3c5cb62b52))

## [1.0.0-dev.43](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.42...v1.0.0-dev.43) (2024-04-19)


### Features

* create issue templates ([c9d6e4c](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/c9d6e4cb1df5ca93917617f8bd33c6d7b84f6f76))

## [1.0.0-dev.42](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.41...v1.0.0-dev.42) (2024-04-19)


### Bug Fixes

* handle long workspaces names ([f8e964e](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/f8e964e6582ece271fe9b4f8c1837723baa6529a))

## [1.0.0-dev.41](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.40...v1.0.0-dev.41) (2024-04-19)


### Features

* delete or leave a workspace from the workspace settings ([939928c](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/939928c60373048393d85a4099bd014891926529))

## [1.0.0-dev.40](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.39...v1.0.0-dev.40) (2024-04-18)


### Features

* save edited workspace name ([a4c4b5b](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/a4c4b5ba32c9ac600fe93a76f605d52fb8b89f4d))

## [1.0.0-dev.39](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.38...v1.0.0-dev.39) (2024-04-17)


### Features

* create workspace ([ea45718](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/ea4571802fdb4f990dcdc5157b2d25c2c6209839))

## [1.0.0-dev.38](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.37...v1.0.0-dev.38) (2024-04-17)


### Features

* adapt notifications to workspaces ([8cabbfc](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/8cabbfc0b4a1beb12c69ad0b3717670a9d3095d4))

## [1.0.0-dev.37](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.36...v1.0.0-dev.37) (2024-04-17)


### Features

* add a workspace menu ([ade9532](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/ade953256f58f1b06b78fb6cd933971acae39daa))

## [1.0.0-dev.36](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.35...v1.0.0-dev.36) (2024-04-17)


### Bug Fixes

* include X-Workspace header on external data API ([c0295d5](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/c0295d584e5f844fc26508cad6a904f132064050))

## [1.0.0-dev.35](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.34...v1.0.0-dev.35) (2024-04-17)


### Features

* new modale for workspace settings ([686a7ea](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/686a7ea887c754908db3c9b742602a3ba769b7cc))

## [1.0.0-dev.34](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.33...v1.0.0-dev.34) (2024-04-16)


### Features

* adapt API calls to include the `X-Workspace` parameter ([f1050f9](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/f1050f96fa9d5d96d37e1216fcda41b905e0b905))
* adapt recFetch to accept UseFetch options ([b7e7378](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/b7e737805486cf0a78d6586d7ad184ab055135d9))

## [1.0.0-dev.33](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.32...v1.0.0-dev.33) (2024-04-16)


### Features

* add client and API version in floating button ([13910ec](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/13910ecf86e3e554591f73ba60f56183c1b84ffa))

## [1.0.0-dev.32](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.31...v1.0.0-dev.32) (2024-04-15)


### Features

* reorganize pages for workspaces and added a dashboard ([21b07e3](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/21b07e3ec3ce96341ca5b7f1d7d5918436aa09ed))

## [1.0.0-dev.31](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.30...v1.0.0-dev.31) (2024-04-10)


### Features

* add the store for workspace ([e59e2d9](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/e59e2d9dc88742c6440e1887a95eeef35509af41))

## [1.0.0-dev.30](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.29...v1.0.0-dev.30) (2024-03-29)


### Features

* Create connection modal in the parameters ([af0fc54](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/af0fc5439bdc7f731eaef9224a859d9772e0a5e3))

## [1.0.0-dev.29](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.28...v1.0.0-dev.29) (2024-03-29)


### Features

* update existing connection ([a1e0417](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/a1e041718465cf087a3ff9cef3a2c48f3d32ed59))

## [1.0.0-dev.28](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.27...v1.0.0-dev.28) (2024-03-28)


### Bug Fixes

* Address the interface errors identified on the client ([9ed23d2](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/9ed23d2019d663a329e9e923a3cc34a14b40b6cf))

## [1.0.0-dev.27](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.26...v1.0.0-dev.27) (2024-03-22)


### Features

* Notification for member ([c8b8b61](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/c8b8b61a0498df2683bcd8a57e51c21f6f1a6b08))

## [1.0.0-dev.26](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.25...v1.0.0-dev.26) (2024-03-22)


### Features

* Create a datalink from Galaxy ([2c0bfb4](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/2c0bfb44be7dc2f59d8cc085b3fd5dd78cce1db9))

## [1.0.0-dev.25](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.24...v1.0.0-dev.25) (2024-03-21)


### Features

* create a notification inbox and connect websocket ([0f4d199](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/0f4d1997b3ce39b8917c8efb2a4ad2ba93fe5bd2))

## [1.0.0-dev.24](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.23...v1.0.0-dev.24) (2024-03-11)


### Bug Fixes

* clear state of NodeAddMembers on close ([6ab331d](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/6ab331dc811ca0f2fb2aa10230a2d744fc09abdc))

## [1.0.0-dev.23](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.22...v1.0.0-dev.23) (2024-03-11)


### Bug Fixes

* remove old "investigations" laying around in the code ([cc02958](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/cc02958f9db0bd80d271d4e7f9371ae8146d1b6f))

## [1.0.0-dev.22](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.21...v1.0.0-dev.22) (2024-03-01)


### Features

* add an alert box to warn users about adding new members ([07e037d](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/07e037de884f5a38d469dd32963872b1729bc1b6))

## [1.0.0-dev.21](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.20...v1.0.0-dev.21) (2024-03-01)


### Features

* introduce members in nodes ([27cabbd](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/27cabbdd1778409be6c62d323a0a0fb33bf3f934))

## [1.0.0-dev.20](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.19...v1.0.0-dev.20) (2024-03-01)


### Features

* Setup of the node store ([2c5812b](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/2c5812b01e32abc7baedb3341c6c106fcf42d07c))


### Bug Fixes

* change the `v-list-item-text` property to `v-list-item-title` ([2fc2ef7](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/2fc2ef7abc428270d836f3783b6315a092ce4960))
* changed location of FloatingBtn menu to top ([1e107a8](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/1e107a86e0b9487e59e01dd44b82aa3e5640605d))

## [1.0.0-dev.19](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.18...v1.0.0-dev.19) (2024-01-05)


### Bug Fixes

* remove the copy function from user info ([8f0b0a2](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/8f0b0a2404b6991c50df5d4c520770e2a7d3a05a))

## [1.0.0-dev.18](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.17...v1.0.0-dev.18) (2024-01-04)


### Features

* add format and linter configuration ([0d56e9b](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/0d56e9bf2ca5d95340a9f5c2378f014ea43370d5))

## [1.0.0-dev.17](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.16...v1.0.0-dev.17) (2023-12-20)


### Features

* create a modal for user information ([7176bdf](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/7176bdf6775d043d82322193f0e9d76903159668))

## [1.0.0-dev.16](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.15...v1.0.0-dev.16) (2023-12-19)


### Features

* create a guide tour ([a4b6f5b](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/a4b6f5bc43c1979c18255fea2f492345de73c903))

## [1.0.0-dev.15](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.14...v1.0.0-dev.15) (2023-12-18)


### Bug Fixes

* remove the display of the current member in the studies ([daa00f7](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/daa00f7fe26cadb90c5213939d02b163381cc0a0))

## [1.0.0-dev.14](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.13...v1.0.0-dev.14) (2023-12-14)


### Features

* create a websocket for members ([fc600d6](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/fc600d672c084fd9c8c469918bf57d9f8b6a4317))

## [1.0.0-dev.13](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.12...v1.0.0-dev.13) (2023-12-12)


### Bug Fixes

* restrict the FloatingBtn to a absolute div ([51ffa1c](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/51ffa1ce92cb32f66cb8c0b492db9bd77c276944))

## [1.0.0-dev.12](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.11...v1.0.0-dev.12) (2023-12-11)


### Features

* create a floating help button ([139d2cf](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/139d2cfdf6989991a9e37c09f86a47dcad67fec5))

## [1.0.0-dev.11](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.10...v1.0.0-dev.11) (2023-12-07)


### Bug Fixes

* resolve the reload of UI elements ([18a98ed](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/18a98ed78ea6d6e623d0bab5ea7b875fcee5027c))

## [1.0.0-dev.10](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.9...v1.0.0-dev.10) (2023-11-24)


### Features

* add better support for unexpected errors ([5633a8c](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/5633a8cd1bd7e4060f7be98940df871f07eb80a8))

## [1.0.0-dev.9](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.8...v1.0.0-dev.9) (2023-11-21)


### Features

* add members pagination support ([9fb696c](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/9fb696ca8ab53a59299e94a9845da3d0bc7d1cea))

## [1.0.0-dev.8](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.7...v1.0.0-dev.8) (2023-11-20)


### Features

* dapt client to the uniform errors sent by API ([4fb3b42](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/4fb3b4200d521519e1f64278ca9990b3eedbecaf))

## [1.0.0-dev.7](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.6...v1.0.0-dev.7) (2023-11-07)


### Features

* Reconfigure authentication workflow to use Madbot API OIDC service ([b61ef52](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/b61ef5284dd850d21f7575bc6cd3056174e51d55))

## [1.0.0-dev.6](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.5...v1.0.0-dev.6) (2023-11-07)


### Features

* disable text areas for collaborators ([7af8333](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/7af83338b48b81dde8ea587cbd3e024c26884320))

## [1.0.0-dev.5](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.4...v1.0.0-dev.5) (2023-11-06)


### Features

* disable delete button for non-owners ([1f61242](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/1f6124222222668f83e6688c7a6397d7b3a39394))

## [1.0.0-dev.4](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.3...v1.0.0-dev.4) (2023-11-06)


### Features

* add gravatar icon ([1891fa0](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/1891fa00bc94152ab5910517efff962fcf3409dc))

## [1.0.0-dev.3](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.2...v1.0.0-dev.3) (2023-11-06)


### Features

* member display control ([7bd479f](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/7bd479f4149feb7dc916a04efba6cd2b5e34aa85))

## [1.0.0-dev.2](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/compare/v1.0.0-dev.1...v1.0.0-dev.2) (2023-11-06)


### Features

* enable pagination for studies list with search field ([43e2fcf](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/43e2fcfccd236b6e24553c77777d2561d4fd0f7e))

## 1.0.0-dev.1 (2023-11-06)


### Features

* "metadata" added to the text in reference to metark ([f69879b](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/f69879b4db16eb8a5898f7936caf4d797541b630))
* activate auto release ([e10bcf2](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/e10bcf25d0991d3d4b584fa2cf5988fc8f955c4c))
* adapt error display to conventional error messages ([92a88ca](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/92a88ca4c3b775ef582c7f0435816b973a9408cd))
* adapt the creation of multiple members ([e6c69f2](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/e6c69f26c04dde7aadbafa77376f799f8f876ae7))
* Add button to generate demo ([e1fe553](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/e1fe553dd98cac3df1ebca4934a1906bbc7ebd50))
* add Dockerfile for client image ([f739530](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/f739530c27dc2b21be989087963831a050ca98b0))
* add keyup enter to study and assay delete ([274c765](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/274c7654a74a8a896f2869c6ce037a46f2bf48d3))
* add two template for the chips ([f2a02cf](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/f2a02cfedb689d5e226364639d954d0c1cef1df4))
* click on username to add token to clipboard ([50583c3](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/50583c322f91e18056cfb90d5046083468b333ed))
* create CI config file ([37018af](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/37018af9305d1c3be53ad4d937e2a55ee3e702e1))
* create release config file ([3c89f62](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/3c89f62ae75e74960604315de61e9b963e3ce33d))
* diseable change member role for me ([c33ca52](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/c33ca52dbdb047856ef7bea33d1a63ffecc6f3f8))
* display dataobjects descriptions and links when creating a new datalink ([8d43646](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/8d4364605181b3375186e6dc183ecb21d3e4d4a2))
* handle errors in members ([d7fc71a](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/d7fc71a3194a71e8c2116b02fe3d5d1018915c94))
* handle members with the same role in chips ([91664ce](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/91664ce5657f89be509c3ab235d43f31f6bbfaf4))
* improve my member management in investigation ([922ac41](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/922ac41934ae8ee44a45ddac9bab64a40987d94b))
* include investigation id when creating a new tool ([b05f133](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/b05f13374f6d6637851d34017e953e0007b02216))
* keyup enter with latest vuetify version ([a8723c5](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/a8723c5ccf8de8ad3448741fb3f7688a70b88cbb))
* license changed to BS3 ([e6f074b](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/e6f074b2cd95ff95f3bd7142af14283794a5dc35))
* manage API error ([dfb63db](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/dfb63db3c1420e5df1d3def8952b627994c9f575))
* manage investigation members ([c401d06](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/c401d06312ea9859f6e89d6c155df86cdd97131c))
* name adapted to madbot for the ts files ([04839b7](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/04839b75ca15445a03b54b9145c03946603617bb))
* openlink renamed to madbot on client side ([c2f0dad](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/c2f0dad17c69a33d2710ade05b95d81eabc25567))
* prerelease ([5af3396](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/5af33968b834428574f2b4bf90c14580a1a811ce))
* refacfor connectors, tools and dataobject icon display through URLs or Font Awesome icons ([640f0a6](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/640f0a63fd4c2cb1652512832a6c567fcfcf1ac8))
* tool requests sent with the investigation id ([dedee34](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/dedee345ee65ad48e06c3a0184a2758dd96c0ca9))


### Bug Fixes

* add autofocus to assay deletion ([4342ac7](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/4342ac7bada40d94a316bc2f72328274160a14dc))
* delete the package-lock file ([128b030](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/128b030dce91ecfbb55338aa8cb70042005ae914))
* display selected dataobject info ([d45e4aa](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/d45e4aae399bd306b2f862018b02c5658c5c10de))
* ensure .env variables are available in production ([9cbc505](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/9cbc5052bbbada9f9e0f30e0eb46e415b7d856ec))
* handle error in members ([55d7506](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/55d7506806cc7344f31eba609f35752ed18428e4))
* remove tooltip  when the token is copied ([212aa7f](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/212aa7f7dc58c10901ab73b289e7fa4cbaa9a0e4))
* remove unused props ([adc0e6c](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/adc0e6cfab4248a035f04ad6caf72b72fc856e48))
* resolve dataobject infinte loading error ([b9ef1b4](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/commit/b9ef1b4949248d263468877a6c15629c5e29faf9))

// plugins/vuetify.js
/* eslint-disable import/no-namespace */
import { defineNuxtPlugin } from "#app";
import { createVuetify } from "vuetify";
import * as components from "vuetify/components";
import * as directives from "vuetify/directives";
import { fa } from "vuetify/iconsets/fa";
import { mdi } from "vuetify/iconsets/mdi";
import * as labs from "vuetify/labs/components";
/* eslint-enable */

const madbotThemeLight = {
  dark: false,
  colors: {
    "background": "#FFFFFF",
    "background-gray": "#fbfbfb",
    "surface": "#FFFFFF",
    "primary": "#336dff",
    "primary-darken-1": "#3700B3",
    "secondary": "#03DAC6",
    "secondary-darken-1": "#018786",
    "error": "#B00020",
    "info": "#2196F3",
    "success": "#4CAF50",
    "warning": "#FB8C00",
  },

};

const madbotThemeDark = {
  dark: true,
  colors: {
    "background": "#121212",
    "surface": "#212121",
    "background-gray": "#272727",
    "primary": "#2196F3",
    "primary-darken-1": "#277CC1",
    "secondary": "#54B6B2",
    "secondary-darken-1": "#48A9A6",
    "error": "#CF6679",
    "info": "#2196F3",
    "success": "#4CAF50",
    "warning": "#FB8C00",
  },
};

export default defineNuxtPlugin((nuxtApp) => {
  const vuetify = createVuetify({
    components: {
      ...components,
      ...labs,
    },
    directives,
    theme: {
      defaultTheme: "light",
      themes: {
        light: madbotThemeLight,
        dark: madbotThemeDark,
      },
      dark: true,
    },
    icons: {
      sets: {
        mdi,
        fa,
      },
    },
  });

  nuxtApp.vueApp.use(vuetify);
});

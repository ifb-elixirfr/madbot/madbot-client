import { createI18n } from "vue-i18n";

import dateTimeFormats from "../locales/dateTimeFormats.json";
import en from "../locales/en.json";
import fr from "../locales/fr.json";

export default defineNuxtPlugin(({ vueApp }) => {
  const i18n = createI18n({
    legacy: false,
    globalInjection: true,
    locale: "en",
    messages: {
      en,
      fr,
    },
    dateTimeFormats,
  });

  vueApp.use(i18n);
});

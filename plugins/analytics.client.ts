const cookieControl = useCookieControl();

if (cookieControl.cookiesEnabledIds.value.includes("google-analytics")) {
  initGoogleAnalytics(); // placeholder for your custom initialization
}

Before raising an issue to the GitLab issue tracker, please check that your issue has not already been proposed by another user:

- https://gitlab.com/ifb-elixirfr/madbot/madbot-client/-/issues

If you feel that your issue can be categorized as a reproducible bug or a feature proposal, please use one of the issue templates provided and include as much information as possible.

Thank you for helping to make Madbot a better product.

<!-- template inspired by Gitlab template https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/issue_templates/Default.md -->

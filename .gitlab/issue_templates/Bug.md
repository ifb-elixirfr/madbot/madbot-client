<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "regression" or "type::bug" label:

- https://gitlab.com/gitlab-org/gitlab/issues?label_name%5B%5D=regression
- https://gitlab.com/gitlab-org/gitlab/issues?label_name%5B%5D=type::bug

and verify the issue you're about to submit isn't a duplicate.
--->

### Summary

<!-- Summarize the bug encountered concisely. -->

### Environment

- Operating System: <!-- Specify OS -->
- Browser: <!-- Specify browser and version -->
- API version: <!-- Specify API version -->
- Client version: <!-- Specify client versions

### Steps to reproduce

<!-- Describe how one can reproduce the issue - this is very important. Please use an ordered list. -->

### What is the current _bug_ behavior?

<!-- Describe what actually happens. -->

### What is the expected _correct_ behavior?

<!-- Describe what you should see instead. -->

### Relevant logs and/or screenshots

<!-- Paste any relevant logs - please use code blocks (```) to format console output, logs, and code
 as it's tough to read otherwise. -->

### Possible fixes

<!-- If you can, link to the line of code that might be responsible for the problem. -->

/label ~"type::bug"

<!-- template inspired by Gitlab template https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/issue_templates/Bug.md -->

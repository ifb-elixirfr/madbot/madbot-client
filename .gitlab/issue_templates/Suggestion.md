<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "type::suggestion" label:

- https://gitlab.com/ifb-elixirfr/madbot/madbot-client/-/issues?label_name%5B%5D=type::suggestion

and verify the issue you're about to submit isn't a duplicate.
--->

### Problem to solve

<!-- What problem do we solve? Try to define the who/what/why of the opportunity as a user story. For example, "As a (who), I want (what), so I can (why/value)." -->

### Intended users

<!-- Who will use this feature? If known, include any of the following: types of users (e.g. Developer), personas, or specific company roles (e.g. Release Manager). It's okay to write "Unknown" and fill this field in later. -->

### User experience goal

<!-- What is the single user experience workflow this problem addresses?
For example, "The user should be able to use the UI/API/.gitlab-ci.yml with GitLab to <perform a specific task>"
https://about.gitlab.com/handbook/product/ux/ux-research-training/user-story-mapping/ -->

### Proposal

<!-- How are we going to solve the problem?  -->

### Further details

<!-- Include use cases, benefits, goals, or any other details that will help us understand the problem better. -->

### Idea for implementation

<!-- If you have any ideas or suggestions for implementation, please add your proposals here, specifying the files affected. -->

### What does success look like?

<!-- What are the indicators of successful implementation? -->

### Links / references

<!-- You can add any useful links for implementing the suggestion, such as to documentation, packages, stackoverflow, etc. -->

/label ~"type::suggestion"

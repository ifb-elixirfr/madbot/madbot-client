# Madbot Client

## Overview

### Description

Web client for Madbot, the Metadata And Data Brokering Online Tool

### Language

[![Made with VueJS](https://img.shields.io/badge/Made%20with-VueJS-blue)](https://vuejs.org/)
[![Made with Nuxt](https://img.shields.io/badge/Made%20with-Nuxt-blue)](https://nuxt.com/)
[![Made with Vuetify](https://img.shields.io/badge/Made%20with-Vuetify-blue)](https://vuetifyjs.com/en/)
[![Made with Pinia](https://img.shields.io/badge/Made%20with-Pinia-blue)](https://pinia.vuejs.org/)

**Continuous Integration**

![eslint](https://img.shields.io/badge/Eslint-passed-green)
![nuxi](https://img.shields.io/badge/Nuxi-passed-green)

**Questions**

[:speech_balloon: Ask a question/suggestions or report a bug](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/-/issues/new)
[:book: List of questions/suggestions and bugs](https://gitlab.com/ifb-elixirfr/madbot/madbot-client/-/issues)
[:book: General questions](mailto:ifb-madbot@groupes.france-bioinformatique.fr)

**Contribution**

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg)](code_of_conduct.md)

## Requirements

To run Madbot Client locally, a working Madbot API environment is required and the following tools have to be installed:

- [Miniconda3](https://docs.conda.io/en/main/miniconda.html)
- [yarn](https://yarnpkg.com/) (and [nodejs](https://nodejs.org/))
- [Git](https://git-scm.com/)
- [Docker](https://www.docker.com/)

## Installation procedure for developers

Follow the above steps to set up the development environment for Madbot.

**Clone the Repository:**

```bash
git clone git@gitlab.com:ifb-elixirfr/madbot/madbot-client.git
or
git clone https://gitlab.com/ifb-elixirfr/madbot/madbot-client.git

cd madbot-client
```

**Make sure to have the correct version of node:**

```bash
node -v
```

If you have a version higher than 18.2.0, you need to downgrade your installation:

```bash
sudo npm install -g n
sudo n 20.17.0
```

**Install the dependencies:**

```bash
npm install --global yarn
yarn install
```

**Create your development `.env` config file:**

```bash
ORIGIN=http://localhost:3000
NUXT_SECRET=ihAPtaDPqvrrGyyS8n_zTG6szYnJ7f-btRjeJAXbYNE
NUXT_OIDC_WELL_KNOWN=http://localhost:8000/o/.well-known/openid-configuration/
NUXT_PUBLIC_OIDC_CLIENT_ID=<APPLICATION ID>
NUXT_OIDC_CLIENT_SECRET=<SECRET>
NUXT_PUBLIC_MADBOT_API_URL=http://127.0.0.1:8000

```

Replace "APPLICATION ID" and "SECRET" with the application ID and secret obtained from gitlab.com

**Launch the server:**

```bash
yarn dev
```

## Code quality

Before every push in GitLab, it is recommended to test the following commands:

```bash
yarn run lint
```

Note: In order not to fail at the time of continuous integration, no errors should be detected.

## Developpement Best Practices

### Conventional Commit Messages

We follow the [Conventional Commits](https://www.conventionalcommits.org/) convention for our commit messages. Please adhere to this format for all commits in this project to maintain a coherent and readable code history.

**Format of the commit message:**

```
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

**Example of a commit message:**

```sh
git commit -m "feat: add new API endpoint"
```

**Allowed 'type' values:**

- **build**: Builds - changes that affect the build system or external dependencies
- **chores**: Chores - updating grunt tasks etc; no production code change
- **ci**: Continous Integration - chanhes to our CI configuration files and scripts
- **docs**: Documentation - changes to the documentation
- **feat**: Features - new feature for the user, not a new feature for build script
- **fix**: Bug fixes - bug fix for the user, not a fix to a build script
- **perf**: Performance Improvements - a code change that improves performance
- **refactor**: Code Refactoring - a code change that neither fixes a bug or adds a feature
- **reverts**: Reverts - reverts a previous commit
- **style**: Style - formatting, missing semi colons, etc; no production code change
- **test**: Tests - adding missing tests, refactoring tests; no production code change

**Example 'scope' values:**

- init
- runner
- watcher
- config
- web-server
- proxy
- etc.

The "scope" can be empty (e.g. if the change is a global or difficult to assign to a single component), in which case the parentheses are omitted. In smaller projects such as Karma plugins, the "scope" is empty.

**Message body:**

- Uses the imperative, present tense: “change” not “changed” nor “changes”
- Includes motivation for the change and contrasts with previous behavior

**Message footer:**

- **Referencing issues**: Closed issues should be listed on a separate line in the footer prefixed with "Closes" keyword like this:

```sh
Closes#234
```

or in case of multiple issues:

```sh
Closes#123, #245, #992
```

- **Breaking changes**: a commit that has a footer BREAKING CHANGE:, or appends a ! after the type/scope, introduces a breaking API change (correlating with MAJOR in Semantic Versioning). A BREAKING CHANGE can be part of commits of any type.

**Useful websites**

- <https://www.conventionalcommits.org/en/v1.0.0/>
- <http://karma-runner.github.io/1.0/dev/git-commit-msg.html>

## Contributors

- [Bouri Laurent](https://gitlab.com/lbouri1) <a itemprop="sameAs" content="https://orcid.org/0000-0002-2297-1559" href="https://orcid.org/0000-0002-2297-1559" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
- [Denecker Thomas](https://gitlab.com/thomasdenecker) <a itemprop="sameAs" content="https://orcid.org/0000-0003-1421-7641" href="https://orcid.org/0000-0003-1421-7641" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
- [Messak Imane](https://gitlab.com/imanemessak) <a itemprop="sameAs" content="https://orcid.org/0000-0002-1654-6652" href="https://orcid.org/0000-0002-1654-6652" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
- [Mohamed Anliat](https://gitlab.com/anliatm) <a itemprop="sameAs" content="https://orcid.org/0000-0002-1105-8262" href="https://orcid.org/0000-0002-1105-8262" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
- [Rousseau Baptiste](https://gitlab.com/nobabar) <a itemprop="sameAs" content="https://orcid.org/0009-0002-1723-2732" href="https://orcid.org/0009-0002-1723-2732" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
- [Seiler Julien](https://gitlab.com/julozi) <a itemprop="sameAs" content="https://orcid.org/0000-0002-4549-5188" href="https://orcid.org/0000-0002-4549-5188" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>

## Contribution

Please, see the [CONTRIBUTING](CONTRIBUTING.md) file.

## Contributor Code of conduct

Please note that this project is released with a [Contributor Code of Conduct](https://www.contributor-covenant.org/). By participating in this project you agree to abide by its terms. See [CODE_OF_CONDUCT](code_of_conduct.md) file.

## Licence

[![License](https://img.shields.io/badge/License-BSD_3--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

Madbot API is licensed under a [The 3-Clause BSD License License](https://opensource.org/license/bsd-3-clause/).

## Acknowledgement

- Nadia GOUE and Matéo HIRIART for their contribution to openLink;
- The working group for its advisory role;
- All application testers.

## Citation

If you use Matbot Client project, please cite us:

IFB-ElixirFr, Matbot Client, (2023), GitLab repository, <https://gitlab.com/ifb-elixirfr/madbot/madbot-client.git>

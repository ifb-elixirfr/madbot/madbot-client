import { NuxtAuthHandler } from "#auth";
import { createError, eventHandler } from "h3";

// Function to check backend health status
async function isBackendAvailable(): Promise<boolean> {
  const { public: { madbotApiURL } } = useRuntimeConfig();

  try {
    const response = await fetch(`${madbotApiURL}/health-check`);
    if (!response.ok) {
      console.warn(`Backend health check failed with status ${response.status}: ${response.statusText}`);
      return false;
    }
    return true;
  } catch (error) {
    if (error instanceof TypeError) {
      console.error("Network error while checking backend availability:", error);
    } else {
      console.error("Unexpected error during backend health check:", error);
    }
    return false;
  }
}

// Auth handler configuration with Nuxt Auth
const authHandler = NuxtAuthHandler({
  secret: useRuntimeConfig().secret,
  providers: [
    {
      id: "madbot",
      name: "Madbot",
      type: "oauth",
      wellKnown: useRuntimeConfig().oidcWellKnown,
      authorization: { params: { scope: "openid email profile read write" } },
      checks: ["pkce", "state"],
      idToken: true,
      clientId: useRuntimeConfig().public.oidcClientID,
      clientSecret: useRuntimeConfig().oidcClientSecret,
      profile(profile) {
        return {
          id: profile.sub,
          name: profile.name,
          email: profile.email,
        };
      },
    },
  ],
  callbacks: {
    async session({ session, token }) {
      session.token = token.token;
      session.user.id = token.sub;
      session.user.username = token.username;
      return session;
    },
    async jwt({ token, account, profile }) {
      if (account) {
        token.token = account.access_token;
        token.username = profile.preferred_username;
      }
      return token;
    },
  },
});

// Main event handler with backend availability check
export default eventHandler(async (event) => {
  const backendUp = await isBackendAvailable();

  if (!backendUp) {
    console.error("Backend is down. Redirecting user to fallback page.");
    throw createError({
      statusCode: 307,
      statusMessage: "Temporary Redirect",
      data: { url: "/server-unavailable" },
    });
  }

  try {
    return await authHandler(event);
  } catch (authError) {
    console.error("Authentication error:", authError);
    throw createError({
      statusCode: 500,
      statusMessage: "Authentication failed",
      data: { error: "Authentication processing error" },
    });
  }
});

import type { UseFetchOptions } from "#app";

import { useAPIFetch } from "./useAPIFetch";

/**
 * The function recursively fetches and concatenates results from an API until there are no
 * more next pages.
 * @param url - The `url` parameter is the URL of the API endpoint that you want to fetch
 * data from.
 * @returns The function `recFetch` is returning a promise that resolves to an array of
 * results.
 */

export async function recFetch(url: string, options?: UseFetchOptions<any>): Promise<any[]> {
  const { data: restResult, error } = await useAPIFetch(url, options);

  if (error.value) {
    throw error.value;
  }
  if (restResult.value.next) {
    return restResult.value.results.concat(await recFetch(restResult.value.next, options));
  } else {
    return restResult.value.results;
  }
}

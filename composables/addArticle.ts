export function addArticle(word) {
  const nextWord = word.toLowerCase(); // Convertir le mot suivant en minuscules pour la comparaison
  if (["a", "e", "i", "o", "u"].includes(nextWord[0])) {
    return `an ${word}`;
  } else {
    return `a ${word}`;
  }
}

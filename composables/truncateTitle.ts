export function truncateTitle(title: string, maxLength = 30): string {
  if (title.length > maxLength) {
    return `${title.substring(0, maxLength - 3)}...`;
  }
  return title;
}

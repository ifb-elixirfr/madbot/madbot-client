import type { UseFetchOptions } from "#app";
import type { Ref } from "vue";

export async function useAPIFetch(url: string | Request | Ref<string | Request> | (() => string | Request), options?: UseFetchOptions<any>) {
  const { data: session, signOut } = useAuth();
  if (options === undefined) {
    options = {};
  }
  options.headers = options.headers || {};
  if (session.value !== null) {
    options.headers.authorization = `Bearer ${session.value.token}`;
  }
  options.baseURL = useRuntimeConfig().public.madbotApiURL;
  options.onResponseError = async ({ response }) => {
    if (response.status === 401) {
      signOut({ callbackUrl: "/" });
    }
  };

  let data, error;

  try {
    data = await $fetch(url, options);
  } catch (e) {
    error = e;
    if (error.data === undefined || error.data.code === "unauthorized") {
      navigateTo("/server-unavailable");
    }
  }
  return { data: ref(data), error: ref(error) };
}

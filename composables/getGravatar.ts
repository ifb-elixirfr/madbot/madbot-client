import CryptoJS from "crypto-js";

export function getGravatar(email, size = 24) {
  if (typeof email === "undefined") {
    email = "";
  }
  return (
    `https://www.gravatar.com/avatar/${CryptoJS.MD5(email.toLowerCase().trim())}?size=${size}`
  );
}

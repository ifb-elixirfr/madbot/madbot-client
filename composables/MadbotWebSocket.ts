export function MadbotWebSocket(path) {
  const { data: session } = useAuth();

  let location = window.location.host;
  if (location === "localhost:3000") {
    location = "localhost:8000";
  }

  if (session.value !== null) {
    document.cookie = `AuthorizationToken=${session?.value?.token}; path=/;`;
  }

  const wsProtocol = window.location.protocol === "https:" ? "wss:" : "ws:";

  const websocket = new WebSocket(`${wsProtocol}//${location}/ws/${path}`);

  return websocket;
}

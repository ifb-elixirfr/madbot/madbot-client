import Ajv2019 from "ajv/dist/2019";
import ajvErrors from "ajv-errors";
import addFormats from "ajv-formats";

export function newAjv(
  allErrors: boolean = true,
  verbose: boolean = true,
  strict: boolean = false,
): Ajv2019 {
  const ajv = new Ajv2019({
    allErrors,
    verbose,
    strict,
  });
  ajvErrors(ajv);
  addFormats(ajv);

  return ajv;
}
